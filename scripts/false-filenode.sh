#!/bin/bash
#arg1 - path to file-node
#arg2 - number of blocks

seekval=`expr $2 - 1`
dd if=/dev/zero of=$1 bs=8K count=1 seek=$seekval
echo 'false file creation passed'