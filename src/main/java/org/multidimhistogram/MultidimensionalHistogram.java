package org.multidimhistogram;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.DBProperties;
import org.partitioning.AtomicPredicate;
import org.partitioning.AtomicPredicateUtils;
import org.partitioning.BitmapIndexHolder;
import org.partitioning.PostgresDBService;
import org.partitioning.SimpleTable;
import org.partitioning.TableMapping;
import org.roaringbitmap.FastAggregation;
import org.roaringbitmap.RoaringBitmap;

public class MultidimensionalHistogram implements Externalizable {
  private Map<String, String[]> distinctValues;
  private Map<AtomicPredicate, RoaringBitmap> predicateEntryBitmaps;

  private SimpleTable table;
  private int numberOfPredicates;
  private List<HistogramElement> entries;

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    MultidimensionalHistogram that = (MultidimensionalHistogram) o;

    if (numberOfPredicates != that.numberOfPredicates) return false;
    if (table != null ? !table.equals(that.table) : that.table != null) return false;
    if (entries != null ? !entries.equals(that.entries) : that.entries != null) return false;
    if (distinctValues != null ? !distinctValues.equals(that.distinctValues) : that.distinctValues != null) {
      return false;
    }
    return predicateEntryBitmaps != null ? predicateEntryBitmaps.equals(that.predicateEntryBitmaps)
        : that.predicateEntryBitmaps == null;
  }

  @Override public int hashCode() {
    int result = table != null ? table.hashCode() : 0;
    result = 31 * result + numberOfPredicates;
    result = 31 * result + (entries != null ? entries.hashCode() : 0);
    result = 31 * result + (distinctValues != null ? distinctValues.hashCode() : 0);
    result = 31 * result + (predicateEntryBitmaps != null ? predicateEntryBitmaps.hashCode() : 0);
    return result;
  }

  public MultidimensionalHistogram() {
  }

  public MultidimensionalHistogram(SimpleTable t, Collection<AtomicPredicate> predicatesForTable) {
    table = t;
    numberOfPredicates = predicatesForTable.size();
    entries = new ArrayList<>();
    initializeElements(predicatesForTable);
    predicateEntryBitmaps = new HashMap<>();
    initializeBitmaps(predicatesForTable);
    distinctValues = new HashMap<>();
  }

  public int size() {
    return entries.size();
  }

  public void fillEntries(DBProperties dbProperties) {
    for (HistogramElement e : entries)
      fillElement(e, dbProperties);
  }

  public void fillDistinctValues(DBProperties dbProperties) throws ClassNotFoundException, SQLException {
    HashSet<String> attributes = TableMapping.tableAttributes.get(table);
    for (String a : attributes)
      fillDistinctValues(a, dbProperties);
  }

  public List<Pair<String, Integer>> getOccurrences(String attribute, Collection<AtomicPredicate> partitionPredicates) {
    // find the attribute values that satisfy the predicates
    BitmapIndexHolder indexHolder = BitmapIndexHolder.getInstance();
    RoaringBitmap[] bitmaps = new RoaringBitmap[partitionPredicates.size()];
    int pos = 0;
    for (AtomicPredicate p : partitionPredicates) {
      bitmaps[pos++] = indexHolder.bitmapIndexes_Distinct.get(p).getBitmap(attribute);
    }
    RoaringBitmap satisfyingValues = FastAggregation.and(bitmaps);
    int numberOfValues = satisfyingValues.getCardinality();

    // find the entries in the histogram to be aggregated
    RoaringBitmap[] entryBitmaps = new RoaringBitmap[partitionPredicates.size()];
    pos = 0;
    for (AtomicPredicate p : partitionPredicates)
      entryBitmaps[pos++] = predicateEntryBitmaps.get(p);
    RoaringBitmap matchingEntries = FastAggregation.and(entryBitmaps);
    int numberOfEntries = matchingEntries.getCardinality();
    int[] matchingEntriesIDs = new int[numberOfEntries];
    for (int i = 0; i < numberOfEntries; i++) {
      matchingEntriesIDs[i] = matchingEntries.select(i);
    }

    // actual aggregation algorithm
    List<Pair<String, Integer>> result = new ArrayList<Pair<String, Integer>>(numberOfValues);
    int valueID, totalCount;
    for (int i = 0; i < numberOfValues; ++i) {
      valueID = satisfyingValues.select(i);
      totalCount = 0;
      for (int j = 0; j < numberOfEntries; ++j) {
        try {
          AVLTree avlTree = entries.get(matchingEntriesIDs[j]).data.get(attribute);
          if (avlTree != null) {
            totalCount += avlTree.lookup(valueID);
          }
        } catch (NullPointerException e) {
          e.printStackTrace();
        }
      }
      result.add(i, new Pair<>(distinctValues.get(attribute)[valueID], totalCount));
    }

    return result;
  }

  private void initializeElements(Collection<AtomicPredicate> predicatesForTable) {
    ArrayList<AtomicPredicate> predicateList = (ArrayList<AtomicPredicate>) predicatesForTable;
    // create all feasible combinations of all predicates
    HashMap<SimpleTable, ArrayList<HashSet<AtomicPredicate>>> combinations =
        AtomicPredicateUtils.generatePartitioningPredicates(predicateList);
    ArrayList<HashSet<AtomicPredicate>> comb = combinations.get(table);
    combinations_loop:
    for (HashSet<AtomicPredicate> s : comb) {
      HashMap<String, HashSet<AtomicPredicate>> predicatesByAttribute = new HashMap<>();
      ArrayList<AtomicPredicate> filteredPredicates = new ArrayList<>();
      for (AtomicPredicate p : s) {
        if (!predicatesByAttribute.containsKey(p.attribute)) {
          HashSet<AtomicPredicate> h = new HashSet<>();
          h.add(p);
          predicatesByAttribute.put(p.attribute, h);
        } else {
          predicatesByAttribute.get(p.attribute).add(p);
        }
      }
      for (String attribute : predicatesByAttribute.keySet()) {
        HashSet<AtomicPredicate> filteredSet = predicatesByAttribute.get(attribute);
        AtomicPredicateUtils.filter(filteredSet);
        if (!filteredSet.isEmpty()) {
          filteredPredicates.addAll(filteredSet);
        }
      }

      if (filteredPredicates.isEmpty()) {
        continue;
      }

      for (HistogramElement existingElement : entries) {
        // Hash sets don't take the order of the elements into account when performing equals(...)
        HashSet<AtomicPredicate> existingPredicates =
            new HashSet<AtomicPredicate>(Arrays.asList(existingElement.predicates));
        HashSet<AtomicPredicate> currentPredicates = new HashSet<AtomicPredicate>(filteredPredicates);
        if (existingPredicates.equals(currentPredicates)) {
          continue combinations_loop;
        }
      }
      AtomicPredicate[] predArr = filteredPredicates.toArray(new AtomicPredicate[filteredPredicates.size()]);
      entries.add(new HistogramElement(predArr));
    }
    System.out.println("Number of histogram entries: " + entries.size());
  }

  // the integer used to enumerate each string is its rank in the sorted ascending order
  // the actual order of the strings is preserved and reflected in the integers
  private void fillElement(HistogramElement e, DBProperties dbProperties) {
    if (e.predicates == null) {
      return;
    }
    HashSet<String> attributes = TableMapping.tableAttributes.get(table);
    String dataSQL = "select res2.row_number-1, res1.cnt "
        + "from (select %s, count(*) as cnt from %s where %s group by %s) res1 "
        + "inner join (select x.%s, row_number() over(order by x.%s) as row_number from (select distinct %s from %s) x) res2 on res1.%s = res2.%s";
    String predicateConjunction = AtomicPredicateUtils.generatePredicateConjunction(e.predicates);
    for (String attribute : attributes) {
      String query = String.format(
          dataSQL, attribute, table.toTextTable(), predicateConjunction, attribute,
          attribute, attribute, attribute, table.toTextTable(), attribute, attribute);
      try {
        Connection conn = PostgresDBService.getConnection(dbProperties);
        PreparedStatement pstmt = conn.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
          e.insertData(attribute, rs.getInt(1), rs.getInt(2));
        }
        rs.close();
      } catch (ClassNotFoundException | SQLException e1) {
        System.err.println("Extraction for attribute " + attribute + " failed for the following reason:");
        e1.printStackTrace();
      }
    }
  }

  private void initializeBitmaps(Collection<AtomicPredicate> predicatesForTable) {
    for (AtomicPredicate p : predicatesForTable) {
      predicateEntryBitmaps.put(p, new RoaringBitmap());
      predicateEntryBitmaps.put(AtomicPredicate.inverse(p), new RoaringBitmap());
    }

    int pos = 0;
    Set<AtomicPredicate> s = predicateEntryBitmaps.keySet();
    for (HistogramElement e : entries) {
      for (AtomicPredicate p : s) {
        for (int i = 0; i < e.predicates.length; ++i) {
          if (e.predicates[i].equals(p)) {
            predicateEntryBitmaps.get(p).add(pos);
          }
        }
      }
      pos++;
    }
  }

  private void fillDistinctValues(String attribute, DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {
    String[] data;
    String countQuery = String.format("select count (distinct %s) from %s", attribute, table.toTextTable());
    ResultSet rs0 = PostgresDBService.executeQuery(countQuery, dbProperties);
    rs0.next();
    int size = rs0.getInt(1);
    rs0.close();
    data = new String[size];
    String query = String.format("select distinct %s from %s order by %s",
        attribute, table.toTextTable(), attribute);
    Connection conn = PostgresDBService.getConnection(dbProperties);
    PreparedStatement pstmt = conn.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    ResultSet rs = pstmt.executeQuery();
    int pos = 0;
    while (rs.next()) {
      data[pos++] = rs.getString(1).trim();
    }
    rs.close();
    distinctValues.put(attribute, data);
  }

  @Override public void writeExternal(ObjectOutput objectOutput) throws IOException {
    objectOutput.writeObject(this.table);
    objectOutput.writeInt(this.numberOfPredicates);
    objectOutput.writeObject(this.entries);
    objectOutput.writeObject(this.distinctValues);
  }

  @Override public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
    this.table = (SimpleTable) objectInput.readObject();
    this.numberOfPredicates = objectInput.readInt();
    this.entries = (List<HistogramElement>) objectInput.readObject();
    this.distinctValues = (Map<String, String[]>) objectInput.readObject();
  }
}

class HistogramElement implements Externalizable {
  Map<String, AVLTree> data;  //maybe implement as two arrays???
  AtomicPredicate predicates[];

  public HistogramElement() {
    predicates = null;
    data = null;
  }

  HistogramElement(AtomicPredicate[] p) {
    predicates = Arrays.copyOf(p, p.length);
    data = new HashMap<>();
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    HistogramElement that = (HistogramElement) o;

    // Probably incorrect - comparing Object[] arrays with Arrays.equals
    if (!Arrays.equals(predicates, that.predicates)) return false;
    return data != null ? data.equals(that.data) : that.data == null;
  }

  @Override public int hashCode() {
    int result = Arrays.hashCode(predicates);
    result = 31 * result + (data != null ? data.hashCode() : 0);
    return result;
  }

  public void insertData(String attribute, int key, int value) {
    if (!data.containsKey(attribute)) {
      AVLTree tree = new AVLTree();
      tree.insert(key, value);
      data.put(attribute, tree);
    } else {
      data.get(attribute).insert(key, value);
    }
  }

  @Override public void writeExternal(ObjectOutput objectOutput) throws IOException {
    objectOutput.writeInt(this.predicates.length);
    for (AtomicPredicate predicate : this.predicates) {
      objectOutput.writeObject(predicate);
    }

    objectOutput.writeObject(this.data);
  }

  @Override public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
    int numPredicates = objectInput.readInt();
    this.predicates = new AtomicPredicate[numPredicates];
    for (int i = 0; i < numPredicates; i++) {
      this.predicates[i] = (AtomicPredicate) objectInput.readObject();
    }

    this.data = (Map<String, AVLTree>) objectInput.readObject();
  }
}
