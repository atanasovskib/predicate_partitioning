package org.multidimhistogram;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Iterator;
import java.util.Stack;

public class AVLTree implements Iterable<AVLNode>, Externalizable {
  private int keyNotFoundReturnValue;
  private AVLNode root;

  public AVLTree() {
    this.root = null;
    this.keyNotFoundReturnValue = 0;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AVLTree avlNodes = (AVLTree) o;

    if (keyNotFoundReturnValue != avlNodes.keyNotFoundReturnValue) return false;
    return root != null ? root.equals(avlNodes.root) : avlNodes.root == null;
  }

  @Override public int hashCode() {
    int result = root != null ? root.hashCode() : 0;
    result = 31 * result + keyNotFoundReturnValue;
    return result;
  }

  @Override public void writeExternal(ObjectOutput objectOutput) throws IOException {
    objectOutput.writeBoolean(this.root == null);
    if (this.root != null) {
      objectOutput.writeObject(this.root);
    }

    objectOutput.writeInt(this.keyNotFoundReturnValue);
  }

  @Override public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
    boolean isRootNull = objectInput.readBoolean();
    if (!isRootNull) {
      this.root = (AVLNode) objectInput.readObject();
    }

    this.keyNotFoundReturnValue = objectInput.readInt();
  }

  public void insert(int key, Integer value) {
    root = insert(key, value, root);
  }

  public void remove(int key) {
    root = remove(key, root);
  }

  public int lookup(int key) {
    return lookup(key, root);
  }

  public boolean contains(int key) {
    return contains(key, root);
  }

  public Integer findMin() {
    return findMin(root).value;
  }

  public Integer findMax() {
    return findMax(root).value;
  }

  public boolean isEmpty() {
    return root == null;
  }

  public AVLTreeIterator iterator() {
    return new AVLTreeIterator(root);
  }

  private int height(AVLNode t) {
    return t == null ? -1 : t.height;
  }

  private AVLNode insert(int key, Integer value, AVLNode t) {
    if (t == null) {
      // we have reached the destination for insertion
      return new AVLNode(key, value);
    }

    if (key < t.key) {
      t.left = insert(key, value, t.left);
    } else if (key > t.key) {
      t.right = insert(key, value, t.right);
    } else {
      // duplicate - do nothing
    }
    return balance(t);
  }

  private AVLNode balance(AVLNode t) {
    if (t == null) {
      return t;
    }

    if (height(t.left) - height(t.right) > 1) {
      if (height(t.left.left) >= height(t.left.right)) {
        t = singleRotationLeftChild(t);            // case 1
      } else {
        t = doubleRotationLeftChild(t);         // case 3
      }
    } else if (height(t.right) - height(t.left) > 1) {
      if (height(t.right.right) >= height(t.right.left)) {
        t = singleRotationRightChild(t);       // case 2
      } else {
        t = doubleRotationRightChild(t);           // case 4
      }
    }

    t.height = Math.max(height(t.left), height(t.right)) + 1;
    return t;
  }

  private AVLNode singleRotationLeftChild(AVLNode k2) {
    AVLNode k1 = k2.left;
    k2.left = k1.right;
    k1.right = k2;
    k2.height = Math.max(height(k2.left), height(k2.right)) + 1;  // required for k1.height
    k1.height = Math.max(height(k1.left), k2.height) + 1;
    return k1;
  }

  private AVLNode singleRotationRightChild(AVLNode k1) {
    AVLNode k2 = k1.right;
    k1.right = k2.left;
    k2.left = k1;
    k1.height = Math.max(height(k1.left), height(k1.right)) + 1;  // required for k2.height
    k2.height = Math.max(k1.height, height(k2.right)) + 1;
    return k2;
  }

  private AVLNode doubleRotationLeftChild(AVLNode k3) {
    k3.left = singleRotationRightChild(k3.left);
    return singleRotationLeftChild(k3);
  }

  private AVLNode doubleRotationRightChild(AVLNode k3) {
    k3.right = singleRotationLeftChild(k3.right);
    return singleRotationRightChild(k3);
  }

  private AVLNode remove(int key, AVLNode t) {
    if (t == null) {
      return null;  // key not found; do nothing
    }

    if (key < t.key) {
      t.left = remove(key, t.left);
    } else if (key > t.key) {
      t.right = remove(key, t.right);
    } else if (t.left != null && t.right != null) {  // two children
      AVLNode rightMin = findMin(t.right);
      t.key = rightMin.key;
      t.value = rightMin.value;
      t.right = remove(t.key, t.right);
    } else {
      t = t.left != null ? t.left : t.right;
    }

    return balance(t);
  }

  private int lookup(int key, AVLNode t) {
    while (t != null) {
      if (key < t.key) {
        t = t.left;
      } else if (key > t.key) {
        t = t.right;
      } else {
        return t.value;
      }
    }

    return keyNotFoundReturnValue;
  }

  private boolean contains(int key, AVLNode t) {
    while (t != null) {
      if (key < t.key) {
        t = t.left;
      } else if (key > t.key) {
        t = t.right;
      } else {
        return true;
      }
    }

    return false;
  }

  private AVLNode findMin(AVLNode t) {
    if (t == null) {
      return null;
    }

    while (t.left != null) {
      t = t.left;
    }

    return t;
  }

  private AVLNode findMax(AVLNode t) {
    if (t == null) {
      return t;
    }

    while (t.right != null) {
      t = t.right;
    }

    return t;
  }

  public class AVLTreeIterator implements Iterator<AVLNode> {

    Stack<AVLNode> stack;
    AVLNode toReturn;

    public AVLTreeIterator(AVLNode t) {
      this.stack = new Stack<>();
      toReturn = new AVLNode();
      while (t != null) {
        stack.push(t);
        t = t.left;
      }
    }

    @Override
    public boolean hasNext() {
      return !stack.isEmpty();
    }

    @Override
    public AVLNode next() {
      AVLNode t = stack.pop();
      toReturn.key = t.key;
      toReturn.value = t.value;
      t = t.right;
      while (t != null) {
        stack.push(t);
        t = t.left;
      }
      return toReturn;
    }

    @Override
    public void remove() {
      //not supported
      throw new UnsupportedOperationException("Remove is not supported.");
    }
  }
}

class AVLNode implements Externalizable {

  int key;
  int value;
  AVLNode left;
  AVLNode right;
  int height;

  public AVLNode() {
    this.key = -1;
    this.value = -1;
    this.left = null;
    this.right = null;
    this.height = 0;
  }

  AVLNode(int key, int value) {
    this.key = key;
    this.value = value;
    this.left = null;
    this.right = null;
    this.height = 0;
  }

  @Override public void writeExternal(ObjectOutput objectOutput) throws IOException {
    objectOutput.writeInt(this.key);
    objectOutput.writeInt(this.value);

    objectOutput.writeBoolean(this.left == null);
    if (this.left != null) {
      this.left.writeExternal(objectOutput);
    }

    objectOutput.writeBoolean(this.right == null);
    if (this.right != null) {
      this.right.writeExternal(objectOutput);
    }

    objectOutput.writeInt(this.height);
  }

  @Override public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
    this.key = objectInput.readInt();
    this.value = objectInput.readInt();

    boolean leftIsNull = objectInput.readBoolean();
    if (!leftIsNull) {
      this.left = new AVLNode();
      this.left.readExternal(objectInput);
    }

    boolean rightIsNull = objectInput.readBoolean();
    if (!rightIsNull) {
      this.right = new AVLNode();
      this.right.readExternal(objectInput);
    }

    this.height = objectInput.readInt();
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AVLNode avlNode = (AVLNode) o;

    if (key != avlNode.key) return false;
    if (value != avlNode.value) return false;
    if (height != avlNode.height) return false;
    if (left != null ? !left.equals(avlNode.left) : avlNode.left != null) return false;
    return right != null ? right.equals(avlNode.right) : avlNode.right == null;
  }

  @Override public int hashCode() {
    int result = key;
    result = 31 * result + value;
    result = 31 * result + (left != null ? left.hashCode() : 0);
    result = 31 * result + (right != null ? right.hashCode() : 0);
    result = 31 * result + height;
    return result;
  }
}