package org.jgap.optimization;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.DBProperties;
import org.jgap.FitnessFunction;
import org.jgap.Gene;
import org.jgap.IChromosome;
import org.jgap.impl.FixedBinaryGene;
import org.json.JSONException;
import org.partitioning.AtomicPredicate;
import org.partitioning.MultidimensionalHistogramHolder;
import org.partitioning.PartitioningUtils;
import org.partitioning.PostgresDBService;
import org.partitioning.SimpleTable;
import org.partitioning.SystemCatalogManager;
import org.partitioning.TableMetadata;
import org.partitioning.WorkloadPredicates;
import org.partitioning.WorkloadQuery;

@SuppressWarnings("serial")
public class CustomFitnessFunction extends FitnessFunction {

  private final double totalCostInNonPartitionedSchema;
  private final int maxNumberOfPartitions;
  private final DBProperties dbProperties;
  private final WorkloadPredicates workloadPredicates;
  private final TableMetadata tableMetadata;
  private final String[] m_tables = new String[] {"customer", "part", "supplier", "dates", "lineorder"};
  private final boolean preferLargerCost;
  private final String schemaName;
  private final String newSchemaName;
  private final MultidimensionalHistogramHolder multiHistograms;
  private Map<String, Double> chromosomesLookup;
  private int numberOfPartitions;

  public CustomFitnessFunction(
      double a_cost,
      int a_maxPart,
      boolean a_preferLargerCost,
      DBProperties dbProperties,
      String schemaName,
      String newSchemaName,
      MultidimensionalHistogramHolder multiHistograms,
      WorkloadPredicates workloadPredicates,
      TableMetadata tableMetadata) {
    totalCostInNonPartitionedSchema = a_cost;
    maxNumberOfPartitions = a_maxPart;
    this.dbProperties = dbProperties;
    this.workloadPredicates = workloadPredicates;
    this.tableMetadata = tableMetadata;
    numberOfPartitions = -1;
    preferLargerCost = a_preferLargerCost;
    chromosomesLookup = new TreeMap<>();
    this.schemaName = schemaName;
    this.newSchemaName = newSchemaName;
    this.multiHistograms = multiHistograms;
  }

  @Override
  protected double evaluate(IChromosome a_chromosome) {

    String chromStr = GeneticAlgorithmEvolution.chromosomeToString(a_chromosome);
    if (chromosomesLookup.containsKey(chromStr)) {
      double fitnessValue = chromosomesLookup.get(chromStr);
      System.out.println("CHROMOSOME COST = " + fitnessValue);
      return fitnessValue;
    }

    List<AtomicPredicate> selectedPredicates = new ArrayList<>();
    Gene[] genes = a_chromosome.getGenes();
    for (int i = 0; i < genes.length; i++) {
      FixedBinaryGene binGene = (FixedBinaryGene) genes[i];
      if (binGene.getBit(0)) {
        selectedPredicates.add(this.workloadPredicates.getPredicate(i));
      }
    }

    if (maxNumberOfPartitions == -1) {
      double cost = estimateExecutionCost(selectedPredicates, this.dbProperties, this.schemaName, this.newSchemaName,
          this.multiHistograms);
      if (preferLargerCost) {
        chromosomesLookup.put(chromStr, cost);
        return cost;
      } else {
        if (selectedPredicates.size() == 0 || cost == 0 || totalCostInNonPartitionedSchema - cost < 0) {
          chromosomesLookup.put(chromStr, .0);
          return 0;
        } else {
          chromosomesLookup.put(chromStr, totalCostInNonPartitionedSchema - cost);
          return totalCostInNonPartitionedSchema - cost;
        }
      }
    }
    return 0;
  }

  /* Helper functions */
  public double estimateExecutionCost(
      List<AtomicPredicate> selectedPredicates,
      DBProperties dbProperties,
      String schemaName,
      String newSchemaName,
      MultidimensionalHistogramHolder multiHistograms) {

    double totalCost = 0;

    Map<SimpleTable, Set<AtomicPredicate>> partitionedSchema = null;
    try {
      PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS partitioned CASCADE;", dbProperties);
      //PostgresDBService.executeUpdate("VACUUM;");
      partitionedSchema = createNewPartitionedSchema(
          schemaName,
          newSchemaName,
          m_tables,
          selectedPredicates,
          dbProperties,
          multiHistograms);
    } catch (FileNotFoundException | SQLException
        | ClassNotFoundException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException
        | NoSuchMethodException | SecurityException
        | InstantiationException | JSONException e) {
      System.err.println("The solution could not be evaluated. Stopping genetic algorithm.");
      e.printStackTrace();
      System.exit(1);
    }

    numberOfPartitions = partitionedSchema.keySet().size();

    String[] queries = org.partitioning.Constants.WORKLOAD.split(";");
    for (int i = 0; i < queries.length; i++) {
      WorkloadQuery q = new WorkloadQuery(queries[i]);

      try {
        q.extractPredicates();
        totalCost += q.calculateExecutionCost(partitionedSchema, true);
      } catch (JSONException | SQLException e) {
        e.printStackTrace();
        System.exit(1);
      }
    }
    System.out.println("CHROMOSOME COST = " + totalCost);
    return totalCost;
  }

  private Map<SimpleTable, Set<AtomicPredicate>> createNewPartitionedSchema(
      String schemaName, String newSchemaName, String[] oldTables,
      List<AtomicPredicate> selectedPredicates, DBProperties dbProperties,
      MultidimensionalHistogramHolder multiHistograms)
      throws ClassNotFoundException, FileNotFoundException, SQLException,
      JSONException, IllegalAccessException, IllegalArgumentException,
      InvocationTargetException, NoSuchMethodException,
      SecurityException, InstantiationException {

    Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates = null;
    SimpleTable[] starr = new SimpleTable[oldTables.length];
    for (int i = 0; i < oldTables.length; i++) {
      starr[i] = new SimpleTable(schemaName, oldTables[i]);
    }
    PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS "
        + newSchemaName + " CASCADE", dbProperties);
    // not really needed
    //PostgresDBService.executeUpdate("VACUUM");
    partitionsAndTheirPredicates =
        PartitioningUtils.createPartitionedSchema(newSchemaName, selectedPredicates, dbProperties);
    List<SimpleTable> desiredPartitions =
        populateCatalogEntries_allpartitions(starr, partitionsAndTheirPredicates, dbProperties, multiHistograms);
    return partitionsAndTheirPredicates;
  }

  public List<SimpleTable> populateCatalogEntries_allpartitions(
      SimpleTable[] oldTables,
      Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates,
      DBProperties dbProperties,
      MultidimensionalHistogramHolder multiHistograms
  )
      throws ClassNotFoundException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException,
      NoSuchMethodException, SecurityException, InstantiationException,
      SQLException {
    String newschema = null;
    boolean newschemaSet = false;
    List<SimpleTable> partitionsOfPredicates = new ArrayList<SimpleTable>();
    for (int i = 0; i < oldTables.length; i++) {
      SystemCatalogManager catalogManager = new SystemCatalogManager(
          oldTables[i].schemaname, oldTables[i].tablename, multiHistograms);
      catalogManager.retrieveMasterTableCatalogData(this.tableMetadata);
      for (SimpleTable partition : partitionsAndTheirPredicates.keySet()) {
        if (partition.tablename.startsWith(oldTables[i].tablename + "_")) {
          if (!newschemaSet) {
            newschema = partition.schemaname;
            newschemaSet = true;
          }
          if (partitionsAndTheirPredicates.get(partition) == null) {
            catalogManager.copyStatisticsForPseudoPartition(partition, dbProperties);
          } else {
            catalogManager.updateStatisticsForPartition(partition, partitionsAndTheirPredicates.get(partition),
                dbProperties);
            partitionsOfPredicates.add(partition);
          }
        }
      }
      if (newschema != null) {

        // Master tables should be empty, but they need to contain statistics
        // for their children
        // We do this by avoiding the pg_class update!

        SimpleTable master = new SimpleTable(newschema,
            oldTables[i].tablename + "_master");
        catalogManager.copyStatisticsForMasterTable(master, dbProperties);
      }
    }

    return partitionsOfPredicates;
  }
}
