package org.jgap.optimization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.DBProperties;
import org.ProjectConstants;
import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.FitnessFunction;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.FixedBinaryGene;
import org.jgap.impl.MutationOperator;
import org.json.JSONException;
import org.partitioning.AtomicPredicate;
import org.partitioning.BitmapIndexHolder;
import org.partitioning.Constants;
import org.partitioning.MultidimensionalHistogramHolder;
import org.partitioning.PartitioningUtils;
import org.partitioning.PostgresDBService;
import org.partitioning.SimpleTable;
import org.partitioning.TableMetadata;
import org.partitioning.WorkloadPredicates;
import org.partitioning.WorkloadQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneticAlgorithmEvolution {
  private static final String dbPropertiesFileName = "db.properties";
  private static final int POP_SIZE = 20;
  private static final int MAX_GENERATIONS = 100;
  static final int CONSTRAINT_GEN = 20;
  private static final int queries = 11;
  private static final String outFileName = "JGAP_Min_" + POP_SIZE + "_" + MAX_GENERATIONS + "_" + queries + ".debug";
  private static final double costInNonPartitionedSchema = 1.1e07;
  private static final int maxNumberOfPartitions = -1;
  static final String[] m_tables = new String[] {"customer", "part", "supplier", "dates", "lineorder"};
  private static final Logger log = LoggerFactory.getLogger(GeneticAlgorithmEvolution.class);
  private static final String schemaName = "public";
  private static final String newSchemaName = "partitioned";

  public static String chromosomeToString(IChromosome a_chromosome) {
    StringBuilder s = new StringBuilder();

    Gene[] genes = a_chromosome.getGenes();
    for (int i = 0; i < genes.length; i++) {
      FixedBinaryGene b = (FixedBinaryGene) genes[i];
      if (b.getBit(0)) {
        s.append("1");
      } else {
        s.append("0");
      }
    }
    return s.toString();
  }

  public static WorkloadPredicates setAllPredicatesArray() throws JSONException {
    log.debug("Set app predicates array called");
    if (Files.exists(Paths.get(ProjectConstants.serializedPredicatesFileName))) {
      try {
        return deserializePredicates();
      } catch (IOException | ClassNotFoundException e) {
        log.warn("error trying to deserialize WorkloadPredicates from {}",
            ProjectConstants.serializedPredicatesFileName);
      }
    }

    log.debug("extracting predicates from workload");
    String[] workload = Constants.WORKLOAD.split(";");
    Set<AtomicPredicate> uniquePredicates = new HashSet<>();
    for (String query : workload) {
      WorkloadQuery q = new WorkloadQuery(query);
      q.extractPredicates();
      List<AtomicPredicate> q_predicates = q.getPredicates();
      uniquePredicates.addAll(q_predicates);
    }

    log.debug("predicates extracted and added to set");
    WorkloadPredicates workloadPredicates =
        new WorkloadPredicates(null, uniquePredicates.toArray(new AtomicPredicate[0]));

    log.debug("unique predicates added to WorkloadPredicates.predicates");
    try {
      serializePredicates(workloadPredicates);
    } catch (IOException e) {
      log.warn("trouble serializing predicates to file.");
      log.error(e.getMessage());
    }

    return workloadPredicates;
  }

  private static void serializePredicates(WorkloadPredicates workloadPredicates) throws IOException {
    log.debug("serializing extracted predicates to file: {}", ProjectConstants.serializedPredicatesFileName);
    ObjectOutput output = new ObjectOutputStream(new FileOutputStream(ProjectConstants.serializedPredicatesFileName));
    output.writeObject(workloadPredicates);
    output.close();
  }

  private static WorkloadPredicates deserializePredicates() throws IOException, ClassNotFoundException {
    log.debug("deserializing existing predicates from file:  {}", ProjectConstants.serializedPredicatesFileName);
    ObjectInput ois = new ObjectInputStream(new FileInputStream(ProjectConstants.serializedPredicatesFileName));
    WorkloadPredicates predicates = (WorkloadPredicates) ois.readObject();
    ois.close();
    return predicates;
  }

  public static double[] getRealExecutionCostAndTime(List<AtomicPredicate> selectedPredicates,
      DBProperties dbProperties)
      throws ClassNotFoundException {

    double[] res =
        new double[3]; // res[0] = explain(cost), res[1] = expain analyze(cost), res[2] = explain analyze (time)
    res[0] = 0.0;
    res[1] = 0.0;
    res[2] = 0.0;
    String[] queries = org.partitioning.Constants.WORKLOAD.split(";");
    Map<SimpleTable, Set<AtomicPredicate>> partitionedSchema = null;
    try {
      PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS partitioned CASCADE;", dbProperties);
      PostgresDBService.executeUpdate("VACUUM;", dbProperties);
      partitionedSchema =
          createEmptyPartitionedSchema(m_tables, selectedPredicates, dbProperties);
      List<SimpleTable> desiredPartitions =
          insertDataIntoPartitionedSchema(partitionedSchema, dbProperties);
      //PostgresDBService.executeUpdate("ANALYZE;");
    } catch (SQLException | ClassNotFoundException | FileNotFoundException e) {
      e.printStackTrace();
    }

    for (String query : queries) {
      WorkloadQuery q = new WorkloadQuery(query);
      try {
        q.extractPredicates();
        res[0] += q.calculateExecutionCost(partitionedSchema, false);
        double[] costTime = q.calculateExecutionCostAndTime(partitionedSchema, false, dbProperties);
        res[1] += costTime[0];
        res[2] += costTime[1];
      } catch (JSONException | SQLException e) {
        System.out.println("Error in actual data evaluation!");
        e.printStackTrace();
        System.exit(1);
      }
    }

    return res;
  }

  public static Map<SimpleTable, Set<AtomicPredicate>> createEmptyPartitionedSchema(
      String[] oldTables,
      List<AtomicPredicate> selectedPredicates,
      DBProperties dbProperties) throws SQLException, FileNotFoundException, ClassNotFoundException {

    // Create the new empty partitions
    Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates = null;
    SimpleTable[] starr = new SimpleTable[oldTables.length];
    for (int i = 0; i < oldTables.length; i++) {
      starr[i] = new SimpleTable("public", oldTables[i]);
    }
    PostgresDBService.executeUpdate("DROP SCHEMA IF EXISTS " + "partitioned" + " CASCADE;", dbProperties);
    //not really needed
    //PostgresDBService.executeUpdate("VACUUM;");
    partitionsAndTheirPredicates =
        PartitioningUtils.createPartitionedSchema("partitioned", selectedPredicates, dbProperties);
    return partitionsAndTheirPredicates;
  }

  public static List<SimpleTable> insertDataIntoPartitionedSchema(
      Map<SimpleTable, Set<AtomicPredicate>> partitionsAndTheirPredicates,
      DBProperties dbProperties) throws SQLException, ClassNotFoundException {

    // PartitionsAndTheirPredicates contains pairs (partition, predicates that generate(constrain) 'partition')
    List<SimpleTable> populatedPartitions = new ArrayList<SimpleTable>();

    for (SimpleTable st : partitionsAndTheirPredicates.keySet()) {
      if (partitionsAndTheirPredicates.get(st) != null) {
        if (!st.tablename.contains("_master")) {
          String st_name = st.tablename;
          int j = st_name.length() - 1;
          Set<AtomicPredicate> originalTablePredicates = new HashSet<>();

          while (st_name.charAt(j--) != '_') ;
          st_name = st_name.substring(0, j + 1);

          for (AtomicPredicate ap : partitionsAndTheirPredicates.get(st)) {
            AtomicPredicate ap_orig =
                new AtomicPredicate("public", st_name, ap.attribute, ap.attributeType, ap.operator, ap.value);
            originalTablePredicates.add(ap_orig);
          }

          StringBuilder sb = new StringBuilder();
          for (AtomicPredicate ap : originalTablePredicates) {
            sb.append(ap.toTextPredicateWithoutSchema());
            sb.append(" AND ");
          }
          String predicateConjunction = sb.toString();

          String insertSQL = "INSERT INTO " + st.toTextTable()
              + " (SELECT * FROM " + "public" + "." + st_name
              + " WHERE " + predicateConjunction.substring(0, predicateConjunction.length() - 5) + ");";
          populatedPartitions.add(st);
          PostgresDBService.executeUpdate(insertSQL, dbProperties);
          //PostgresDBService.executeUpdate("analyze " + st);

        }
      } else {
        // no predicates for this table, so we have a single partiton -- just copy the data
        // we need a more intelligent way of getting the original table name as it may contain an undescore ( _ )
        if (!st.tablename.contains("_master")) {
          String insertCopySQL = "INSERT INTO "
              + st.toTextTable()
              + " (SELECT * FROM "
              + "public"
              + "."
              + st.tablename.substring(0, st.tablename.indexOf('_'))
              + ");";
          populatedPartitions.add(st);
          PostgresDBService.executeUpdate(insertCopySQL, dbProperties);

          //dodadeno
          //PostgresDBService.executeUpdate("analyze " + st);
        }
      }
    }
    PostgresDBService.executeUpdate("analyze;", dbProperties);
    return populatedPartitions;
  }

  public static List<AtomicPredicate> getSelectedPredicates(WorkloadPredicates predicates, IChromosome a_chromosome) {
    Gene[] genes = a_chromosome.getGenes();
    List<AtomicPredicate> selectedPredicates = new ArrayList<AtomicPredicate>();
    for (int j = 0; j < genes.length; j++) {
      FixedBinaryGene fb = (FixedBinaryGene) genes[j];
      if (fb.getBit(0)) {
        selectedPredicates.add(predicates.getPredicate(j));
      }
    }
    return selectedPredicates;
  }

  public static void main(String[] args)
      throws IOException, InvalidConfigurationException, ClassNotFoundException, SQLException, JSONException {
    log.debug("starting algorithm");
    DBProperties dbProperties = DBProperties.load(GeneticAlgorithmEvolution.dbPropertiesFileName);

    File outFile = new File(outFileName);
    log.info("Output file is: {}", outFile.getAbsolutePath());
    try (PrintWriter pw = new PrintWriter(new FileWriter(outFile, false))) {
      pw.println(
          "EvolutionCycle,BestFitness(MinimalExecutionCost),EstimatedExecutionCost(DATA),RealExecutionCost,RealExecutionTime,FittestChromosome");
      pw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

		/* Initializations */
    WorkloadPredicates workloadPredicates = setAllPredicatesArray();
    PostgresDBService.getConnection(dbProperties);

    // initialize bitmap indexes
    TableMetadata tableMetadata = TableMetadata.countRows(dbProperties);
    BitmapIndexHolder bitmapIndexes = BitmapIndexHolder.getInstance();
    bitmapIndexes.generateBitmapIndexesByDistinct(workloadPredicates.getPredicates(), dbProperties);
    System.out.println("Distinct bit arrays generated.");
    bitmapIndexes.generateBitmapIndexesByRows(workloadPredicates.getPredicates(), dbProperties);
    System.out.println("Regular bit arrays generated.");

    // initialize multidimensional histograms
    MultidimensionalHistogramHolder multiHistograms = MultidimensionalHistogramHolder.generateHistograms(
        workloadPredicates.getPredicates(),
        dbProperties
    );

    System.out.println("Histograms generated.");
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectOutput out = new ObjectOutputStream(baos);
    out.writeObject(multiHistograms);
    out.close();
    byte[] pipi = baos.toByteArray();
    baos.close();
    ByteArrayInputStream bais = new ByteArrayInputStream(pipi);
    ObjectInput in = new ObjectInputStream(bais);
    MultidimensionalHistogramHolder hoo = (MultidimensionalHistogramHolder) in.readObject();
    System.out.println("seri ok: " + hoo.equals(multiHistograms));
    System.exit(-1);
    /* Genetic Algorithm Setup 1 (Minimization) */
    Configuration conf = new DefaultConfiguration();
    conf.setKeepPopulationSizeConstant(false);
    conf.setPreservFittestIndividual(true);
    MutationOperator m = new MutationOperator(conf, POP_SIZE / 2);  // 1/10

    final boolean doNotPreferLargerCost = false;

    FitnessFunction myFunc = new CustomFitnessFunction(
        costInNonPartitionedSchema,
        maxNumberOfPartitions,
        doNotPreferLargerCost,
        dbProperties,
        schemaName,
        newSchemaName,
        multiHistograms,
        workloadPredicates,
        tableMetadata);
    conf.setFitnessFunction(myFunc);

    FixedBinaryGene[] sampleGenes = new FixedBinaryGene[workloadPredicates.getPredicates().length];
    for (int i = 0; i < sampleGenes.length; i++) {
      sampleGenes[i] = new FixedBinaryGene(conf, 1);
    }

    Chromosome sampleChromosome = new Chromosome(conf, sampleGenes);
    conf.setSampleChromosome(sampleChromosome);
    conf.setPopulationSize(POP_SIZE);

    Genotype population1 = Genotype.randomInitialGenotype(conf);

    IChromosome bestSolutionSoFar;
    for (int i = 0; i < MAX_GENERATIONS; i++) {

      population1.evolve();

      bestSolutionSoFar = population1.getFittestChromosome();
      double fittestCost = costInNonPartitionedSchema - bestSolutionSoFar.getFitnessValue();

      List<AtomicPredicate> selectedPredicates = getSelectedPredicates(workloadPredicates, bestSolutionSoFar);
      double[] realCostTime = getRealExecutionCostAndTime(selectedPredicates, dbProperties);
      double explainCost = realCostTime[0], analyzeCost = realCostTime[1], analyzeTime = realCostTime[2];
      try (PrintWriter pw = new PrintWriter(new FileWriter(outFile, true))) {
        pw.println((i + 1)
            + ","
            + fittestCost
            + ","
            + explainCost
            + ","
            + analyzeCost
            + ","
            + analyzeTime
            + ","
            + chromosomeToString(bestSolutionSoFar));
        pw.close();
      }
    }

    PostgresDBService.closeConnection();
  }
}
