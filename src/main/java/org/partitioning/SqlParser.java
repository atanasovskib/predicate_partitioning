package org.partitioning;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// DA SE DODADE FUNKCIJA KOJA STO KE PRIMA FAJL ILI CEL STRING OD WORKLOAD I KE
// NAPRAVI MERGE NA LISTITE SO PREDICATE, T.E KE GI EKSTRAHIRA SITE MOZNI PREDICATES
// OD DADEN WORKLOAD

public class SqlParser {
  private static final Logger log = LoggerFactory.getLogger(SqlParser.class);
  // Assumption. A table can only exist in single schema
  // NOTE: TableMapping MUST BE INITIALIZED

  public static ArrayList<AtomicPredicate> extractPredicates(String sql)
      throws JSONException {
    ArrayList<AtomicPredicate> predicates = new ArrayList<AtomicPredicate>();
    String parsetree = getParstreeAsString(sql);
    // String parsetree =
    // "[{\"SELECT\"=>{\"distinctClause\"=>nil, \"intoClause\"=>nil, \"targetList\"=>[{\"RESTARGET\"=>{\"name\"=>nil, \"indirection\"=>nil, \"val\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"x\", \"id\"], \"location\"=>7}}, \"location\"=>7}}], \"fromClause\"=>[{\"RANGEVAR\"=>{\"schemaname\"=>nil, \"relname\"=>\"x\", \"inhOpt\"=>2, \"relpersistence\"=>\"p\", \"alias\"=>nil, \"location\"=>17}}, {\"RANGEVAR\"=>{\"schemaname\"=>nil, \"relname\"=>\"y\", \"inhOpt\"=>2, \"relpersistence\"=>\"p\", \"alias\"=>nil, \"location\"=>19}}], \"whereClause\"=>{\"AEXPR AND\"=>{\"lexpr\"=>{\"AEXPR AND\"=>{\"lexpr\"=>{\"AEXPR AND\"=>{\"lexpr\"=>{\"AEXPR AND\"=>{\"lexpr\"=>{\"AEXPR AND\"=>{\"lexpr\"=>{\"AEXPR AND\"=>{\"lexpr\"=>{\"AEXPR\"=>{\"name\"=>[\">=\"], \"lexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"x\", \"id\"], \"location\"=>27}}, \"rexpr\"=>{\"A_CONST\"=>{\"val\"=>15, \"location\"=>40}}, \"location\"=>32}}, \"rexpr\"=>{\"AEXPR\"=>{\"name\"=>[\"<=\"], \"lexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"x\", \"id\"], \"location\"=>27}}, \"rexpr\"=>{\"A_CONST\"=>{\"val\"=>20, \"location\"=>47}}, \"location\"=>32}}, \"location\"=>32}}, \"rexpr\"=>{\"AEXPR\"=>{\"name\"=>[\"=\"], \"lexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"x\", \"id\"], \"location\"=>54}}, \"rexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"y\", \"id\"], \"location\"=>61}}, \"location\"=>59}}, \"location\"=>50}}, \"rexpr\"=>{\"AEXPR IN\"=>{\"name\"=>[\"=\"], \"lexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"x\", \"id\"], \"location\"=>70}}, \"rexpr\"=>[{\"A_CONST\"=>{\"val\"=>15, \"location\"=>79}}, {\"A_CONST\"=>{\"val\"=>16, \"location\"=>82}}, {\"A_CONST\"=>{\"val\"=>17, \"location\"=>85}}], \"location\"=>75}}, \"location\"=>66}}, \"rexpr\"=>{\"AEXPR\"=>{\"name\"=>[\"<\"], \"lexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"y\", \"name\"], \"location\"=>93}}, \"rexpr\"=>{\"A_CONST\"=>{\"val\"=>\"testname\", \"location\"=>102}}, \"location\"=>100}}, \"location\"=>89}}, \"rexpr\"=>{\"AEXPR\"=>{\"name\"=>[\">\"], \"lexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"y\", \"lastname\"], \"location\"=>117}}, \"rexpr\"=>{\"A_CONST\"=>{\"val\"=>\"abcd\", \"location\"=>130}}, \"location\"=>128}}, \"location\"=>113}}, \"rexpr\"=>{\"SUBLINK\"=>{\"subLinkType\"=>2, \"testexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"y\", \"data\"], \"location\"=>141}}, \"operName\"=>[\"=\"], \"subselect\"=>{\"SELECT\"=>{\"distinctClause\"=>nil, \"intoClause\"=>nil, \"targetList\"=>[{\"RESTARGET\"=>{\"name\"=>nil, \"indirection\"=>nil, \"val\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"y\", \"data\"], \"location\"=>159}}, \"location\"=>159}}], \"fromClause\"=>[{\"RANGEVAR\"=>{\"schemaname\"=>nil, \"relname\"=>\"y\", \"inhOpt\"=>2, \"relpersistence\"=>\"p\", \"alias\"=>nil, \"location\"=>171}}], \"whereClause\"=>{\"AEXPR OR\"=>{\"lexpr\"=>{\"AEXPR\"=>{\"name\"=>[\"=\"], \"lexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"y\", \"data\"], \"location\"=>179}}, \"rexpr\"=>{\"A_CONST\"=>{\"val\"=>\"petko\", \"location\"=>186}}, \"location\"=>185}}, \"rexpr\"=>{\"AEXPR\"=>{\"name\"=>[\"=\"], \"lexpr\"=>{\"COLUMNREF\"=>{\"fields\"=>[\"y\", \"data\"], \"location\"=>197}}, \"rexpr\"=>{\"A_CONST\"=>{\"val\"=>\"stanko\", \"location\"=>204}}, \"location\"=>203}}, \"location\"=>194}}, \"groupClause\"=>nil, \"havingClause\"=>nil, \"windowClause\"=>nil, \"valuesLists\"=>nil, \"sortClause\"=>nil, \"limitOffset\"=>nil, \"limitCount\"=>nil, \"lockingClause\"=>nil, \"withClause\"=>nil, \"op\"=>0, \"all\"=>false, \"larg\"=>nil, \"rarg\"=>nil}}, \"location\"=>148}}, \"location\"=>137}}, \"groupClause\"=>nil, \"havingClause\"=>nil, \"windowClause\"=>nil, \"valuesLists\"=>nil, \"sortClause\"=>nil, \"limitOffset\"=>nil, \"limitCount\"=>nil, \"lockingClause\"=>nil, \"withClause\"=>nil, \"op\"=>0, \"all\"=>false, \"larg\"=>nil, \"rarg\"=>nil}}]";
    String parsetreeJSON = convertParsetreeToValidJSON(parsetree);
    //System.out.println(parsetreeJSON);
    JSONArray jsonArray = null;
    if (parsetreeJSON.isEmpty()) return predicates;
    if (parsetreeJSON.charAt(0) == '[') {
      jsonArray = new JSONArray(parsetreeJSON);
    } else if (parsetreeJSON.charAt(0) == '{') {
      JSONObject jsonObject = new JSONObject(parsetreeJSON);
      jsonArray = new JSONArray();
      jsonArray.put(jsonObject);
    }
    JSONObject selectObj = jsonArray.getJSONObject(0).getJSONObject(Constants.PARSER_SELECT);
    JSONObject whereClause = selectObj.getJSONObject(Constants.PARSER_WHERE_CLAUSE);
    JSONArray fromClause = selectObj.getJSONArray(Constants.PARSER_FROM_CLAUSE);
    String[] relname = new String[fromClause.length()];
    String[] alias = new String[fromClause.length()];
    for (int i = 0; i < fromClause.length(); i++) {
      JSONObject tableInfo = fromClause.getJSONObject(i).getJSONObject(Constants.PARSER_RANGEVAR);
      relname[i] = tableInfo.getString(Constants.PARSER_RELNAME);
      if (tableInfo.has(Constants.PARSER_ALIAS_LC)) {
        if (tableInfo.get("alias") instanceof String &&
            tableInfo.get("alias").equals("$nil")) {
          alias[i] = null;
        } else {
          alias[i] = tableInfo.getJSONObject(Constants.PARSER_ALIAS_LC)
              .getJSONObject(Constants.PARSER_ALIAS_UC).getString(Constants.PARSER_ALISANAME);
        }
      } else {
        alias[i] = null;
      }
    }
    getWhereClauses(whereClause, relname, alias, predicates);
    return predicates;
  }

  public static void getWhereClauses(JSONObject jsonObj,
      String[] relname, String[] alias,
      ArrayList<AtomicPredicate> predicates) throws JSONException {
    if (jsonObj.has(Constants.PARSER_AEXPR)) {
      String schemaT = null, table = null, attribute = null;
      Operator operator = null;
      AttributeType attributeType = null;
      Object value = null;
      JSONObject aexpr = jsonObj.getJSONObject(Constants.PARSER_AEXPR);
      String oper = aexpr.getJSONArray(Constants.PARSER_NAME).getString(0);
      if (oper.equals("<")) {
        operator = Operator.LT;
      } else if (oper.equals("<=")) {
        operator = Operator.LTE;
      } else if (oper.equals(">")) {
        operator = Operator.GT;
      } else if (oper.equals(">=")) {
        operator = Operator.GTE;
      } else if (oper.equals("=")) {
        operator = Operator.EQ;
      } else {
        operator = Operator.NEQ;
      }
      JSONObject lexpr, rexpr;
      if (!((aexpr.getJSONObject(Constants.PARSER_LEXPR).has(Constants.PARSER_COLUMNREF) && aexpr
          .getJSONObject(Constants.PARSER_REXPR).has(Constants.PARSER_A_CONST)) || (aexpr
          .getJSONObject(Constants.PARSER_REXPR).has(Constants.PARSER_COLUMNREF) && aexpr
          .getJSONObject(Constants.PARSER_LEXPR).has(Constants.PARSER_A_CONST)))) {
        return;
      }
      if (aexpr.getJSONObject(Constants.PARSER_LEXPR).has(Constants.PARSER_COLUMNREF)) {
        lexpr = aexpr.getJSONObject(Constants.PARSER_LEXPR);
        rexpr = aexpr.getJSONObject(Constants.PARSER_REXPR);
      } else {
        lexpr = aexpr.getJSONObject(Constants.PARSER_REXPR);
        rexpr = aexpr.getJSONObject(Constants.PARSER_LEXPR);
      }
      JSONArray fields = lexpr.getJSONObject(Constants.PARSER_COLUMNREF).getJSONArray(
          "fields");
      if (fields.length() == 1) {
        attribute = fields.getString(0);
        // fields.length() == 1 means that only one table from the
        // "fromClause" contains this attribute
        main_loop:
        for (int i = 0; i < relname.length; i++) {
          for (SimpleTable st : TableMapping.tableAttributes.keySet()) {
            if (st.tablename.equals(relname[i])) {
              if (TableMapping.tableAttributes.get(st).contains(
                  attribute)) {
                schemaT = st.schemaname;
                table = st.tablename;
                break main_loop;
              }
            }
          }
        }
      } else if (fields.length() == 2) {
        boolean isAlias = true;
        for (int i = 0; i < relname.length; i++)
          if (relname[i].equals(fields.getString(0))) {
            isAlias = true;
            break;
          }
        if (isAlias) {
          for (int i = 0; i < relname.length; i++) {
            if (alias[i].equals(fields.getString(0))) {
              table = relname[i];
              break;
            }
          }
        } else {
          table = fields.getString(0);
        }
        attribute = fields.getString(1);
        for (SimpleTable st : TableMapping.tableAttributes.keySet()) {
          if (st.tablename.equals(table)
              && TableMapping.tableAttributes.get(st).contains(
              attribute)) {
            schemaT = st.schemaname;
            break;
          }
        }
      } else {
        schemaT = fields.getString(0);
        table = fields.getString(1);
        attribute = fields.getString(2);
      }
      value = rexpr.getJSONObject(Constants.PARSER_A_CONST).get(Constants.PARSER_VAL);
      AtomicPredicate ap = new AtomicPredicate(schemaT, table, attribute,
          attributeType, operator, value);
      predicates.add(ap);
      return;
    }
    if (jsonObj.has(Constants.PARSER_AEXPR_IN)) {
      String schemaT = null, table = null, attribute = null;
      Operator operator = null;
      AttributeType attributeType = null;
      Object value = null;
      JSONObject aexpr = jsonObj.getJSONObject(Constants.PARSER_AEXPR_IN);
      String oper = aexpr.getJSONArray(Constants.PARSER_NAME).getString(0);
      if (oper.equals("<")) {
        operator = Operator.LT;
      } else if (oper.equals("<=")) {
        operator = Operator.LTE;
      } else if (oper.equals(">")) {
        operator = Operator.GT;
      } else if (oper.equals(">=")) {
        operator = Operator.GTE;
      } else if (oper.equals("=")) {
        operator = Operator.EQ;
      } else {
        operator = Operator.NEQ;
      }
      JSONObject lexpr;
      Object rexpr;
      if (aexpr.getJSONObject(Constants.PARSER_LEXPR).has(Constants.PARSER_COLUMNREF)) {
        lexpr = aexpr.getJSONObject(Constants.PARSER_LEXPR);
        rexpr = aexpr.get(Constants.PARSER_REXPR);
      } else {
        lexpr = aexpr.getJSONObject(Constants.PARSER_REXPR);
        rexpr = aexpr.get(Constants.PARSER_LEXPR);
      }
      JSONArray fields = lexpr.getJSONObject(Constants.PARSER_COLUMNREF).getJSONArray(
          "fields");
      if (fields.length() == 1) {
        attribute = fields.getString(0);
        main_loop:
        for (int i = 0; i < relname.length; i++) {
          for (SimpleTable st : TableMapping.tableAttributes.keySet()) {
            if (st.tablename.equals(relname[i])) {
              if (TableMapping.tableAttributes.get(st).contains(
                  attribute)) {
                schemaT = st.schemaname;
                table = st.tablename;
                break main_loop;
              }
            }
          }
        }
      } else if (fields.length() == 2) {
        boolean isAlias = true;
        for (int i = 0; i < relname.length; i++)
          if (relname[i].equals(fields.getString(0))) {
            isAlias = true;
            break;
          }
        if (isAlias) {
          for (int i = 0; i < relname.length; i++) {
            if (alias[i].equals(fields.getString(0))) {
              table = relname[i];
              break;
            }
          }
        } else {
          table = fields.getString(0);
        }
        attribute = fields.getString(1);
        for (SimpleTable st : TableMapping.tableAttributes.keySet()) {
          if (st.tablename.equals(table)
              && TableMapping.tableAttributes.get(st).contains(
              attribute)) {
            schemaT = st.schemaname;
            break;
          }
        }
      } else {
        schemaT = fields.getString(0);
        table = fields.getString(1);
        attribute = fields.getString(2);
      }

      if (rexpr instanceof JSONArray) {
        JSONArray rexprarr = (JSONArray) rexpr;
        for (int i = 0; i < rexprarr.length(); i++) {
          if (rexprarr.getJSONObject(i).has(Constants.PARSER_A_CONST)) {
            Object val = rexprarr.getJSONObject(i)
                .getJSONObject(Constants.PARSER_A_CONST).get(Constants.PARSER_VAL);
            predicates.add(new AtomicPredicate(schemaT, table,
                attribute, attributeType, operator, val));
          }
        }
      } else {
        JSONObject rexprobj = (JSONObject) rexpr;
        if (rexprobj.has(Constants.PARSER_A_CONST)) {
          value = rexprobj.getJSONObject(Constants.PARSER_A_CONST).get(Constants.PARSER_VAL);
          if (value instanceof String) {
            if (!TableMapping.tableAttributes.get(table).contains(((String) value).toLowerCase())) {
              predicates.add(new AtomicPredicate(schemaT, table,
                  attribute, attributeType, operator, value));
            }
          } else {
            predicates.add(new AtomicPredicate(schemaT, table,
                attribute, attributeType, operator, value));
          }
        }
      }

      // if(schemaT.equals("$nil")) schemaT = null;
      AtomicPredicate ap = new AtomicPredicate(schemaT, table, attribute,
          attributeType, operator, value);
      predicates.add(ap);
      return;
    }

    // DA se sredi ako rexpr e lista togas site konstanti se zemaat i se
    // gradat povekje predikati, a ako
    // e objekt togas ostanuva isto
    @SuppressWarnings("unchecked")
    Iterator<String> iter = jsonObj.keys();
    while (iter.hasNext()) {
      String key = iter.next();
      Object j = jsonObj.get(key);
      if (j instanceof JSONObject) {
        getWhereClauses(jsonObj.getJSONObject(key), relname,
            alias, predicates);
      } else if (j instanceof JSONArray) {
        JSONArray array = (JSONArray) j;
        for (int index = 0; index < array.length(); index++) {
          Object arrayItem = array.get(index);
          if (arrayItem instanceof JSONObject) {
            getWhereClauses(array.getJSONObject(index), relname, alias, predicates);
          }
        }
      }
    }
  }

  public static String getParstreeAsString(String sql) {
    return getScriptResult(sql);
  }

  public static String convertParsetreeToValidJSON(String parsetree) {
    String[] toReplace = new String[] {"=>", "nil"};
    String[] replacement = new String[] {":", "\"$nil\""};
    String resultJSON = parsetree;
    for (int i = 0; i < toReplace.length; i++) {
      resultJSON = resultJSON.replace(toReplace[i], replacement[i]);
    }
    return resultJSON;
  }

  private static String getScriptResult(String sql) {
    String rubyScriptPath = createRubyParserScript(sql);
    return runScript(rubyScriptPath);
  }

  private static String createRubyParserScript(String sql) {
    log.debug("Creating ruby parser script.");
    log.debug("SQL: {}", sql);
    File rubyScript = null;
    PrintWriter pw = null;
    try {
      rubyScript = new File(sql.hashCode() + ".rb");
      rubyScript.createNewFile();
      pw = new PrintWriter(rubyScript);
      pw.println("require 'pg_query'");
      pw.println("parseResults=PgQuery.parse(\"" + sql + "\")");
      pw.println("print parseResults.parsetree");
    } catch (Exception ex) {
      if (rubyScript.exists()) {
        rubyScript.delete();
      }
      pw.close();
      return null;
    } finally {
      pw.close();
    }
    rubyScript.setExecutable(true);
    return rubyScript.getAbsolutePath();
  }

  private static String runScript(String scriptPath) {
    try {
      String cmd;
      cmd = "ruby " + scriptPath;

      Runtime rt = Runtime.getRuntime();
      Process proc = rt.exec(cmd);

      // any error message?
      StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");
      // any output?
      StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");
      // kick them off
      errorGobbler.start();
      outputGobbler.start();
      proc.waitFor();
      String scriptOutput = outputGobbler.getOutput();
      String errorOutput = errorGobbler.getOutput();
      // any error???
      if (!errorOutput.isEmpty()) {
        log.error("ruby script error output: {}", errorOutput);
      }

      if (!errorOutput.isEmpty()) {
        throw new RuntimeException("Error returned from ruby : " + errorOutput);
      } else if (scriptOutput.isEmpty()) {

        log.error("Result from ruby script was an empty string.");
        throw new RuntimeException("Result from ruby script is an empty string.");
      }

      //delete the .rb file
      cmd = "rm -f " + scriptPath;
      Process procDel = rt.exec(cmd);
      StreamGobbler errorGobblerDel = new StreamGobbler(proc.getErrorStream(), "ERROR");
      StreamGobbler outputGobblerDel = new StreamGobbler(proc.getInputStream(), "OUTPUT");
      errorGobblerDel.start();
      outputGobblerDel.start();
      procDel.waitFor();

      return scriptOutput;
    } catch (Throwable t) {
      log.error("Error executing ruby script for parsing");
      log.error(t.getMessage());
      throw new RuntimeException("Error executing ruby script. ", t);
    }
  }

  public static void main(String[] args) throws JSONException, IOException {

    ArrayList<AtomicPredicate> p =
        extractPredicates("SELECT id FROM tbl t1, tbl2 t2 WHERE t1.id > 15 AND t1.id < 20 AND t2.name = 'petko'");

    System.out.println("Creating maps for table=>predicate...");
    HashMap<SimpleTable, HashSet<AtomicPredicate>> h = AtomicPredicateUtils
        .createPredicateSetsForTables(p);
    System.out.println(h);
  }
}
