package org.partitioning;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;
import org.DBProperties;
import org.ProjectConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateStarSchema {
  private static final Logger log = LoggerFactory.getLogger(CreateStarSchema.class);
  private static final String DDL_DELIMITER = ";";
  private static final String DATA_DELIMITER = "|";
  private static final String PATH_TO_DDL_SCRIPT = "ssb.ddl";
  private static final String[] PATHS_TO_DATA_2 =
      new String[] {"/part.tbl", "/supplier.tbl", "/customer.tbl", "/date.tbl", "/lineorder.tbl"
      };
  private static final String[] TABLE_NAMES = new String[] {
      "part",
      "supplier",
      "customer",
      "dates",
      "lineorder",
  };

  private static String[] getPathsToData(DBProperties dbProperties) {
    String[] toReturn = new String[PATHS_TO_DATA_2.length];
    for (int i = 0; i < PATHS_TO_DATA_2.length; i++) {
      toReturn[i] = dbProperties.getStarShemaTBLFolder() + PATHS_TO_DATA_2[i];
    }

    return toReturn;
  }

  private static void loadTable(String table, String path, DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {
    log.debug("Loading table {}. From file {} into db.", table, path);
    String statement = "COPY " + table + " FROM '" + path + "' (DELIMITER '" + DATA_DELIMITER + "');";
    PostgresDBService.executeUpdate(
        statement, dbProperties);
  }

  public static void createDatabase(DBProperties dbProperties)
      throws ClassNotFoundException, SQLException, IOException {
    PostgresDBService.getConnection(dbProperties);
    Scanner s = null;
    String script;
    boolean isDatabaseDefault = dbProperties.getDatabaseName().toLowerCase().equals("postgres");
    try {
      s = new Scanner(new File(PATH_TO_DDL_SCRIPT));
      script = s.useDelimiter("\\Z").next();
    } finally {
      s.close();
    }

    if (!script.isEmpty()) {
      try {
        if (!isDatabaseDefault) {
          PostgresDBService.executeUpdate(String.format(
              "CREATE DATABASE \"%s\" WITH OWNER = %s", dbProperties.getDatabaseName(), dbProperties.getUsername()),
              dbProperties);
        }
      } catch (Exception e) {
        PostgresDBService.closeStatement();
        PostgresDBService.closeConnection();
        System.err.println("Database " + dbProperties + "could not be created!");
        System.exit(1);
      } finally {
        if (!isDatabaseDefault) {
          PostgresDBService.closeStatement();
          PostgresDBService.closeConnection();
        }
      }

      for (String table : TABLE_NAMES) {
        PostgresDBService.executeUpdate("DROP TABLE IF EXISTS " + table + " CASCADE", dbProperties);
      }

      if (!isDatabaseDefault) {
        PostgresDBService.getConnection(dbProperties);
      }

      PostgresDBService.executeSqlScript(script, DDL_DELIMITER, dbProperties);

      String[] pathsToData = getPathsToData(dbProperties);
      for (int i = 0; i < TABLE_NAMES.length; i++) {
        try {
          loadTable(TABLE_NAMES[i], pathsToData[i], dbProperties);
        } catch (Exception e) {
          log.error(e.toString());
          log.error(
              String.format("Data could not be loaded to table %s from file %s!", TABLE_NAMES[i], pathsToData[i]));
        }
      }
    }
  }

  public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {

    String dbPropertiesFileName;
    if (args.length > 0) {
      dbPropertiesFileName = args[0];
    } else {
      dbPropertiesFileName = ProjectConstants.defaultDbPropertiesFileName;
    }

    DBProperties dbProperties = DBProperties.load(dbPropertiesFileName);
    if (dbProperties == null) {
      log.error("Could not initialize database properties; Exiting");
      System.exit(-1);
    }

    createDatabase(dbProperties);
    System.out.println("Database created.");
  }
}
