package org.partitioning;

import java.io.Externalizable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.DBProperties;
import org.ProjectConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class TableMetadata implements Externalizable {
  private Map<SimpleTable, Long> rows;
  private static Logger log = LoggerFactory.getLogger(TableMetadata.class);

  public TableMetadata() {
    this.rows = new HashMap<>();
  }

  private TableMetadata(Map<SimpleTable, Long> rows) {
    this.rows = rows;
  }

  public static TableMetadata countRows(DBProperties dbProperties) throws ClassNotFoundException, SQLException {
    log.debug("counting rows");
    if (serializedValuesExist()) {
      log.debug("serialized values exist");
      try {
        return deserialize();
      } catch (IOException e) {
        log.error("could not deserialize from file, error: {}", e.getMessage());
      }
    }

    log.debug("no serialized file exists, loading from db");
    Map<SimpleTable, Long> rows = new HashMap<>();
    for (SimpleTable t : TableMapping.tableAttributes.keySet()) {
      log.debug("for table: {}", t);
      String sql = "SELECT COUNT(*) FROM " + t;
      ResultSet rs = PostgresDBService.executeQuery(sql, dbProperties);
      rs.next();
      long rowCount = Long.valueOf(rs.getString(1));
      rs.close();
      rows.put(t, rowCount);
    }

    TableMetadata tableMetadata = new TableMetadata(rows);
    try {
      serialize(tableMetadata);
    } catch (IOException e) {
      log.error("could not serialize to file. error: {}", e.getMessage());
    }

    return tableMetadata;
  }

  private static void serialize(TableMetadata tableMetadata) throws IOException {
    ObjectOutput output =
        new ObjectOutputStream(new FileOutputStream(ProjectConstants.serializedTableMetaDataFileName));
    output.writeObject(tableMetadata);
    output.close();
  }

  private static TableMetadata deserialize() throws IOException, ClassNotFoundException {
    ObjectInput objectInput =
        new ObjectInputStream(new FileInputStream(ProjectConstants.serializedTableMetaDataFileName));
    TableMetadata meta = (TableMetadata) objectInput.readObject();
    objectInput.close();
    return meta;
  }

  public Map<SimpleTable, Long> getRows() {
    return this.rows;
  }

  private static boolean serializedValuesExist() {
    return Files.exists(Paths.get(ProjectConstants.serializedTableMetaDataFileName));
  }

  @Override
  public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException {
    this.rows = (Map<SimpleTable, Long>) input.readObject();
  }

  @Override
  public void writeExternal(ObjectOutput output) throws IOException {
    output.writeObject(this.rows);
  }
}
