package org.partitioning;

import java.util.Arrays;


public class PgStatisticData {

	public String attributename;
	public String attributetype;
	
	public short staattnum;
	public boolean stainherit;
	public double stanullfrac;
	public int stawidth;
	public double stadistinct;
	public double correlation;
	public short[] stakind;
	public int[] staop;
	
	public String[] histogram;
	public String[] mostCommonValues;
	public Double[] mostCommonFreqs;
	//public HashMap<Integer, Integer> statisticalDataFields;
	
	public PgStatisticData() {
		this.stakind = new short[5];
		this.staop = new int[5];
		this.histogram = null;
		this.mostCommonValues = null;
		this.mostCommonFreqs = null;
	}
	
	public PgStatisticData(PgStatisticData rhs) {
		
		this.attributename = rhs.attributename;
		this.attributetype = rhs.attributetype;
		this.staattnum = rhs.staattnum;
		this.stainherit = rhs.stainherit;
		this.stanullfrac = rhs.stanullfrac;
		this.stawidth = rhs.stawidth;
		this.stadistinct = rhs.stadistinct;
		this.correlation = rhs.correlation;
		this.stakind = Arrays.copyOf(rhs.stakind, rhs.stakind.length);
		this.staop = Arrays.copyOf(rhs.staop, rhs.staop.length);
		if(rhs.histogram!=null)
		this.histogram = Arrays.copyOf(rhs.histogram, rhs.histogram.length);
		if(rhs.mostCommonValues != null)
		this.mostCommonValues = Arrays.copyOf(rhs.mostCommonValues, rhs.mostCommonValues.length);
		if(rhs.mostCommonFreqs != null)
		this.mostCommonFreqs = Arrays.copyOf(rhs.mostCommonFreqs, rhs.mostCommonFreqs.length);
	}
	
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		String newline = "\n";
		sb.append("Attributename: ");
		sb.append(this.attributename);
		sb.append(newline);
		sb.append("Attributetype: ");
		sb.append(this.attributetype);
		sb.append(newline);
		sb.append("staattnum: ");
		sb.append(this.staattnum);
		sb.append(newline);
		sb.append("stainherit: ");
		sb.append(this.stainherit);
		sb.append(newline);
		sb.append("stainullfrac: ");
		sb.append(this.stanullfrac);
		sb.append(newline);
		sb.append("stawidth: ");
		sb.append(this.stawidth);
		sb.append(newline);
		sb.append("stainherit: ");
		sb.append(this.stainherit);
		sb.append(newline);
		sb.append("stadistinct: ");
		sb.append(this.stadistinct);
		sb.append(newline);
		sb.append("correlation: ");
		sb.append(this.correlation);
		sb.append(newline);
		sb.append("stakind: ");
		for(Short s : this.stakind)
			sb.append(s + ",");
		sb.append(newline);
		sb.append("staop: ");
		for(Integer s : this.staop)
			sb.append(s + ",");
		sb.append(newline);
		if(this.histogram != null) {
			sb.append("histogram_bounds: ");
			for(String s : this.histogram)
				sb.append(s + ",");
			sb.append(newline);
		}
		if(this.mostCommonValues != null) {
			for(String s : this.mostCommonValues)
				sb.append(s + ",");
			sb.append(newline);
		}
		if(this.mostCommonFreqs != null) {
			for(Double s : this.mostCommonFreqs)
				sb.append(s + ",");
		}
		return sb.toString();
	}
	
}
