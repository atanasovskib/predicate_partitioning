package org.partitioning;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.DBProperties;
import org.bitmapindex.ColumnarBitmapIndex;
import org.bitmapindex.ColumnarBitmapIndex.ColumnarBitmapIndexIterator;
import org.bitmapindex.ColumnarBitmapIndex.ColumnarIndexElement;
import org.multidimhistogram.Pair;
import org.roaringbitmap.RoaringBitmap;

public final class PredicateBitmapsDistinct {

  //public static final Map<AtomicPredicate, ArrayList<RoaringBitmap>> predicateBitmaps = new HashMap<AtomicPredicate, ArrayList<RoaringBitmap>>();
  public static final Map<AtomicPredicate, ColumnarBitmapIndex> bitmapIndexes =
      new HashMap<AtomicPredicate, ColumnarBitmapIndex>();

  // predicates: all predicates extracted from the workload
  public static void generateBitArrays(AtomicPredicate[] predicates, DBProperties dbProperties)
      throws ClassNotFoundException, SQLException {
    String sql = "select res2.row_number-1 "
        + "from (select distinct %s from %s where %s) res1 "
        + "inner join (select x.%s, row_number() over(order by x.%s) as row_number from (select distinct %s from %s) x) res2 on res1.%s = res2.%s "
        + "order by res2.row_number";
    for (AtomicPredicate p : predicates) {
      SimpleTable t = p.extractTable();
      HashSet<String> attributes = TableMapping.tableAttributes.get(t);
      AtomicPredicate pInverse = AtomicPredicate.inverse(p);
      ColumnarBitmapIndex bitmapIndex = new ColumnarBitmapIndex(t);
      ColumnarBitmapIndex bitmapIndexInverse = new ColumnarBitmapIndex(t);
      for (String a : attributes) {
        String query = String.format(sql,
            a, t.toTextTable(), p.toTextPredicateRaw(),
            a, a, a, t.toTextTable(), a, a);
        RoaringBitmap r = new RoaringBitmap();
        ResultSet rs = PostgresDBService.executeQuery(query, dbProperties);
        while (rs.next()) {
          r.add(rs.getInt(1));
        }
        rs.close();
        bitmapIndex.addElement(a, r);

        String query2 = String.format(sql,
            a, t.toTextTable(), pInverse.toTextPredicateRaw(),
            a, a, a, t.toTextTable(), a, a);
        RoaringBitmap rInverse = new RoaringBitmap();
        ResultSet rsInverse = PostgresDBService.executeQuery(query2, dbProperties);
        while (rsInverse.next()) {
          rInverse.add(rsInverse.getInt(1));
        }
        rsInverse.close();
        bitmapIndexInverse.addElement(a, rInverse);
      }
      System.out.println("Predicate done.");
      bitmapIndexes.put(p, bitmapIndex);
      bitmapIndexes.put(pInverse, bitmapIndexInverse);
    }

		/*for(AtomicPredicate p : predicates) {
			ArrayList<RoaringBitmap> bitmaps = new ArrayList<RoaringBitmap>();
			ArrayList<RoaringBitmap> bitmapsInverse = new ArrayList<RoaringBitmap>();
			SimpleTable predicateTable = new SimpleTable(p.schema, p.table);
			AtomicPredicate pInverse = AtomicPredicate.inverse(p);
			
			for(SimpleTable t : TableMapping.tableAttributes.keySet()) {
				if(t.tablename.equals(p.table) && TableMapping.tableAttributes.get(t).contains(p.attribute)) {
					predicateTable = t;
					break;
				}
			}
			
			TreeSet<String> attSetSorted = new TreeSet<String>();	// required to preserve order
			attSetSorted.addAll(TableMapping.tableAttributes.get(predicateTable));
			
			for(String attribute : attSetSorted) {
				String sql = "select res2.row_number-1 "
						+ "from (select distinct %s from %s where %s) res1 "
						+ "inner join (select x.%s, row_number() over(order by x.%s) as row_number from (select distinct %s from %s) x) res2 on res1.%s = res2.%s "
						+ "order by res2.row_number";
				
				String query = String.format(sql,
						attribute,
						predicateTable.toTextTable(),
						p.toTextPredicateRaw(),
						attribute,
						attribute,
						attribute,
						predicateTable.toTextTable(),
						attribute,
						attribute);
				ResultSet rs = PostgresDBService.executeQuery(query);
				RoaringBitmap bitmap = new RoaringBitmap();
				while(rs.next()) {
					bitmap.add(Integer.valueOf(rs.getString(1)));
				}
				rs.close();
				bitmaps.add(bitmap);
				
				String queryInverse = String.format(sql,
						attribute,
						predicateTable.toTextTable(),
						pInverse.toTextPredicateRaw(),
						attribute,
						attribute,
						attribute,
						predicateTable.toTextTable(),
						attribute,
						attribute);
				ResultSet rsInverse = PostgresDBService.executeQuery(queryInverse);
				RoaringBitmap bitmapInverse = new RoaringBitmap();
				while(rsInverse.next()) {
					bitmapInverse.add(Integer.valueOf(rsInverse.getString(1)));
				}
				rsInverse.close();
				bitmapsInverse.add(bitmapInverse);
			}
			
			predicateBitmaps.put(p, bitmaps);
			predicateBitmaps.put(pInverse, bitmapsInverse);
		}*/
  }

  // predicates: a list of predicates over a table
  public static Map<String, Integer> selectCountDistinctAllAttributes(List<AtomicPredicate> predicates) {

    ColumnarBitmapIndex[] indexes = new ColumnarBitmapIndex[predicates.size()];
    int pos = 0;
    for (AtomicPredicate p : predicates)
      indexes[pos++] = bitmapIndexes.get(p);

    ColumnarBitmapIndex intersection = ColumnarBitmapIndex.and(indexes);
    Map<String, Integer> countDistinct = new TreeMap<String, Integer>();
    ColumnarBitmapIndexIterator it = intersection.iterator();
    while (it.hasNext()) {
      Pair<String, ColumnarIndexElement> p = it.next();
      countDistinct.put(p.key, p.value.getCardinality());
    }

    return countDistinct;
		
		/*Map<String, Integer> countDistinct = new TreeMap<String, Integer>();
		TreeSet<String> attSetSorted = new TreeSet<String>();	// we need this to preserve order of indexing (i-th attribute must match)
		SimpleTable t;
		if(predicates.get(0).schema != null)
			t = new SimpleTable(predicates.get(0).schema, predicates.get(0).table);
		else
			t=  new SimpleTable("public", predicates.get(0).table);
		
		attSetSorted.addAll(TableMapping.tableAttributes.get(t));

		int listPos = 0;
		for(String attribute : attSetSorted) {
			int pos = 0;
			RoaringBitmap[] bitmaps = new RoaringBitmap[predicates.size()];
			for(AtomicPredicate p : predicates) {
				bitmaps[pos++] = predicateBitmaps.get(p).get(listPos);
			}
			RoaringBitmap intersection = FastAggregation.and(bitmaps);
			countDistinct.put(attribute, intersection.getCardinality());;
			listPos++;
		}
		return countDistinct;*/
  }
}
