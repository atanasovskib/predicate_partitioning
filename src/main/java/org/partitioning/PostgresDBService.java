package org.partitioning;

import ch.qos.logback.core.db.dialect.PostgreSQLDialect;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.DBProperties;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostgresDBService {
  private static Logger log = LoggerFactory.getLogger(PostgreSQLDialect.class);
  private static Connection connection = null;
  private static ResultSet resultSet = null;
  private static Statement statement = null;

  public static boolean isConnectionActive() throws SQLException {
    return (connection != null && !connection.isClosed());
  }

  public static Connection getConnection(DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {
    if (connection != null && (!connection.isClosed())) {
      return connection;
    }

    Class.forName("org.postgresql.Driver");
    String connectionString =
        getConnectionString(dbProperties.getServer(), dbProperties.getPort(), dbProperties.getDatabaseName());
    connection = DriverManager.getConnection(connectionString, dbProperties.getUsername(), dbProperties.getPassword());
    connection.setAutoCommit(true);
    return connection;
  }

  public static void closeStatement() throws SQLException {
    try {
    } finally {
      if (statement != null) statement.close();
    }
  }

  public static void closeConnection() throws SQLException {
    try {
    } finally {
      if (connection != null) connection.close();
    }
  }

  public static void executeUpdate(String stmt, DBProperties dbProperties) throws SQLException, ClassNotFoundException {

    if (statement == null && connection != null && !connection.isClosed()) {
      log.debug("statement is null && connection is not closed");
      statement = connection.createStatement();
    }

    try {
      statement.executeUpdate(stmt);
    } catch (PSQLException pe) {
      log.error("PSQ Exception: {}", pe.getServerErrorMessage());
      pe.printStackTrace();
      getConnection(dbProperties);
      connection.createStatement();
      statement.executeUpdate(stmt);
    }
  }

  private static String getConnectionString(String server, String port, String dbName) {
    return String.format("jdbc:postgresql://%s:%s/%s", server, port, dbName);
  }

  public static ResultSet executeQuery(String stmt, DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {
    //log.debug("execute query statement: {}", stmt);
    if (statement == null && connection != null && !connection.isClosed()) {
      statement = connection.createStatement();
    }
    try {
      resultSet = statement.executeQuery(stmt);
    } catch (PSQLException pe) {
      log.error("PSQL Exception: {}", pe.getServerErrorMessage());
      pe.printStackTrace();
      getConnection(dbProperties);
      connection.createStatement();
      resultSet = statement.executeQuery(stmt);
    }

    return resultSet;
  }

  public static PreparedStatement getPreparedStatement(String stmt, Object... args) throws SQLException {
    PreparedStatement ps = connection.prepareStatement(stmt);
    int i = 1;
    for (Object arg : args) {
      if (arg instanceof Byte) {
        ps.setByte(i++, (Byte) arg); //used to represent a bit
      } else if (arg instanceof Short) {
        ps.setShort(i++, (Short) arg);
      } else if (arg instanceof Integer) {
        ps.setInt(i++, (Integer) arg);
      } else if (arg instanceof Long) {
        ps.setLong(i++, (Long) arg);
      } else if (arg instanceof BigDecimal) {
        ps.setBigDecimal(i++, (BigDecimal) arg);
      } else if (arg instanceof String) {
        ps.setString(i++, (String) arg);
      } else if (arg instanceof Float) {
        ps.setFloat(i++, (Float) arg);
      } else if (arg instanceof Double) {
        ps.setDouble(i++, (Double) arg);
      } else if (arg instanceof byte[]) {
        ps.setBytes(i++, (byte[]) arg);
      } else if (arg instanceof Boolean) {
        ps.setBoolean(i++, (boolean) arg);
      } else if (arg instanceof java.sql.Date) {
        ps.setDate(i++, (java.sql.Date) arg);
      } else if (arg instanceof java.sql.Time) {
        ps.setTime(i++, (java.sql.Time) arg);
      } else if (arg instanceof java.sql.Timestamp) {
        ps.setTimestamp(i++, (java.sql.Timestamp) arg);
      } else if (arg instanceof java.sql.Blob) {
        ps.setBlob(i++, (java.sql.Blob) arg);
      } else if (arg instanceof java.sql.Clob) {
        ps.setClob(i++, (java.sql.Clob) arg);
      } else if (arg instanceof java.sql.Array) {
        ps.setArray(i++, (java.sql.Array) arg);
      } else if (arg instanceof java.sql.Ref) {
        ps.setRef(i++, (java.sql.Ref) arg);
      }
    }

    return ps;
  }

  public static void executeUpdateParameterized(PreparedStatement ps) throws SQLException {
    ps.executeUpdate();
    resultSet = null;
  }

  public static ResultSet executeQueryParameterized(PreparedStatement ps) throws SQLException {
    resultSet = ps.executeQuery();
    return resultSet;
  }

  public static List<String> getStatementsFromSqlScript(String script, String statementDelimiter) {
    String[] lines = script.split("\\r?\\n");
    List<String> stmts = new ArrayList<String>();
    StringBuilder sb = new StringBuilder();
    for (String line : lines) {
      String trimmedLine = line.trim();
      if (!trimmedLine.isEmpty()) {
        if (!trimmedLine.startsWith("--") && !trimmedLine.startsWith("/") && !trimmedLine.startsWith("*")) {
          sb.append(trimmedLine);
          sb.append("\n");
          if (trimmedLine.endsWith(statementDelimiter)) {
            stmts.add(sb.toString());
            sb = new StringBuilder();
          }
        }
      }
    }
    return stmts;
  }

  public static ArrayList<ResultSet> executeSqlScript(String script, String statementDelimiter, DBProperties properties)
      throws SQLException, ClassNotFoundException {
    ArrayList<ResultSet> results = new ArrayList<ResultSet>();
    List<String> statements = getStatementsFromSqlScript(script, statementDelimiter);
    for (String statement : statements) {
      if (statement.toLowerCase().contains("select") || statement.toLowerCase().contains("explain")) {
        ResultSet rs = executeQuery(statement, properties);
        results.add(rs);
      } else {
        executeUpdate(statement, properties);
        results.add(null);
      }
    }
    return results;
  }

  public static void vacuum(DBProperties properties) throws SQLException, ClassNotFoundException {
    String sql = "VACUUM FULL ANALYZE;";
    executeUpdate(sql, properties);
  }

  public static ExplainResult parseExplainResults(ResultSet results, PreparedStatement ps)
      throws SQLException, JSONException {
    ExplainResult er = null;
    if (results.next()) {
      String res = results.getString(1);
      JSONArray arr = new JSONArray(res);
      JSONObject rootPlan = arr.getJSONObject(0).getJSONObject("Plan");
      double executionCost = rootPlan.getDouble("Total Cost");
      int rowsEstimation = rootPlan.getInt("Plan Rows");
      er = new ExplainResult(executionCost, rowsEstimation);
    }
    ps.close();
    return er;
  }

  public static double getCost(String stmt, boolean simulated, Object... args) throws SQLException, JSONException {
    if (simulated) {
      stmt = "EXPLAIN (FORMAT JSON) " + stmt;
      PreparedStatement ps = getPreparedStatement(stmt, args);
      ExplainResult res = parseExplainResults(executeQueryParameterized(ps), ps);
      return res.executionCost;
    } else {
      stmt = "EXPLAIN (FORMAT JSON) " + stmt;
      PreparedStatement ps = getPreparedStatement(stmt, args);
      ExplainResult res = parseAnalyzeResults(executeQueryParameterized(ps), ps);
      return res.executionCost;
    }
  }

  public static double[] getRealCostAndTime(String stmt, boolean simulated, DBProperties dbProperties, Object... args)
      throws SQLException, JSONException, ClassNotFoundException {
    double[] res = new double[2];
    if (simulated) {
      res[0] = res[1] = -1;
    } else {
      String explainAnalyze = "EXPLAIN ANALYZE " + stmt;
      String firstLine = "", lastLine = "";
      ResultSet rs = executeQuery(explainAnalyze, dbProperties);
      if (rs.next()) {
        firstLine = rs.getString(1);
      }
      while (rs.next()) {
        lastLine = rs.getString(1);
      }
      rs.close();
      double totalCost =
          Double.parseDouble(firstLine.substring(firstLine.indexOf("..") + 2, firstLine.indexOf("rows") - 1));
      double totalRuntime =
          Double.parseDouble(lastLine.substring(lastLine.indexOf(":") + 2, lastLine.indexOf("ms") - 1));
      res[0] = totalCost;
      res[1] = totalRuntime;
    }
    return res;
  }

  public static ExplainResult parseAnalyzeResults(ResultSet results, PreparedStatement ps)
      throws SQLException, JSONException {
    ExplainResult er = null;
    if (results.next()) {
      //System.out.println("ACTUAL EXPLAIN:");

      String res = results.getString(1);
      //System.out.println("ACTUAL EXECUTION PLAN: ");
      //System.out.println(res);
      //System.out.println(res);
      /*int j = res.indexOf("..");
      int k = res.indexOf(' ', j);
			String costString = res.substring(j + 2, k);
			double cost = Double.valueOf(costString);
			er = new ExplainResult(cost, 0);*/
      JSONArray arr = new JSONArray(res);
      JSONObject rootPlan = arr.getJSONObject(0).getJSONObject("Plan");
      double executionCost = rootPlan.getDouble("Total Cost");
      int rowsEstimation = rootPlan.getInt("Plan Rows");
      er = new ExplainResult(executionCost, rowsEstimation);
    }
    results.close();
    ps.close();
    return er;
  }

  public ResultSet explainQuery(String stmt, Object... args) throws SQLException {
    stmt = "EXPLAIN (FORMAT JSON) " + stmt;
    PreparedStatement ps = getPreparedStatement(stmt, args);
    return executeQueryParameterized(ps);
  }
}
