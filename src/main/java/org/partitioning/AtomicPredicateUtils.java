package org.partitioning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

public class AtomicPredicateUtils {

	public static HashMap<SimpleTable, ArrayList<HashSet<AtomicPredicate>>> predicatePermutations;

	public static HashMap<SimpleTable, ArrayList<HashSet<AtomicPredicate>>> generatePartitioningPredicates(
			List<AtomicPredicate> l) {
		predicatePermutations = new HashMap<SimpleTable, ArrayList<HashSet<AtomicPredicate>>>();
		HashMap<SimpleTable, HashSet<AtomicPredicate>> hm = createPredicateSetsForTables(l);
		createPredicatePermutations(hm);
		return predicatePermutations;
	}

	public static HashMap<SimpleTable, HashSet<AtomicPredicate>> createPredicateSetsForTables(
			List<AtomicPredicate> predicates) {
		HashMap<SimpleTable, HashSet<AtomicPredicate>> m = new HashMap<SimpleTable, HashSet<AtomicPredicate>>();
		SimpleTable stDummy = new SimpleTable();
		for (AtomicPredicate ap : predicates) {
			stDummy.schemaname = ap.schema;
			stDummy.tablename = ap.table;
			// is the table valid ?
			if (TableMapping.tableAttributes.containsKey(stDummy)) {
				// is the attribute valid ?
				if (TableMapping.tableAttributes.get(stDummy).contains(
						ap.attribute)) {
					if (!m.containsKey(stDummy)) {
						HashSet<AtomicPredicate> hs = new HashSet<>();
						hs.add(ap);
						m.put(new SimpleTable(ap.schema, ap.table), hs);
					} else {
						m.get(stDummy).add(ap);
					}
				}
			}
		}
		return m;
	}

	private static void createPredicatePermutations(
			HashMap<SimpleTable, HashSet<AtomicPredicate>> m) {
		for (SimpleTable st : m.keySet()) {
			AtomicPredicate[] predicates = new AtomicPredicate[m.get(st).size()];
			int i = 0;
			for (AtomicPredicate ap : m.get(st)) {
				predicates[i] = ap;
				++i;
			}
			enumeratePermutations(0, new int[predicates.length], st, predicates);
		}

	}

	private static void processPermutation(int[] perm, SimpleTable st,
			AtomicPredicate[] predicates) {
		HashSet<AtomicPredicate> hs = new HashSet<AtomicPredicate>();
		for (int i = 0; i < perm.length; i++) {
			if (perm[i] == Constants.PREDICATE_OP_REGULAR)
				hs.add(predicates[i]);
			else if (perm[i] == Constants.PREDICATE_OP_INVERSE)
				hs.add(AtomicPredicate.inverse(predicates[i]));
		}
		if (predicatePermutations.containsKey(st)) {
			predicatePermutations.get(st).add(hs);
		} else {
			ArrayList<HashSet<AtomicPredicate>> l = new ArrayList<HashSet<AtomicPredicate>>();
			l.add(hs);
			predicatePermutations.put(st, l);
		}
	}

	private static void enumeratePermutations(int k, int[] perm,
			SimpleTable st, AtomicPredicate[] predicates) {
		if (k == perm.length) {
			processPermutation(perm, st, predicates);
			return;
		}
		enumeratePermutations(k + 1, perm, st, predicates);
		perm[k] = Constants.PREDICATE_OP_INVERSE;
		enumeratePermutations(k + 1, perm, st, predicates);
		perm[k] = Constants.PREDICATE_OP_REGULAR;
	}

	/*
	 * Filtering se pravi otkoga ke se izvrtat kombinaciite predikati kako bit
	 * strings
	 */

	public static void filter(Set<AtomicPredicate> predicates) {
		
		Map<Object, HashSet<AtomicPredicate>> predsByValue = new HashMap<>();

		NavigableMap<Interval, Interval> sortedIntervals = new TreeMap<Interval, Interval>();
		for (AtomicPredicate p : predicates) {
			if (!predsByValue.containsKey(p.value)) {
				HashSet<AtomicPredicate> s = new HashSet<>();
				s.add(p);
				predsByValue.put(p.value, s);
			} else {
				predsByValue.get(p.value).add(p);
			}
			Interval intvl = new Interval();
			if (p.operator == Operator.EQ) {
				intvl.a = intvl.b = p.value;
				intvl.aClosed = intvl.bClosed = true;
				sortedIntervals.put(intvl, null);
			} else if (p.operator == Operator.NEQ) {
				intvl.a = Double.NEGATIVE_INFINITY;
				intvl.b = p.value;
				intvl.aClosed = false;
				intvl.bClosed = false;
				Interval intvl2 = new Interval();
				intvl2.a = p.value;
				intvl2.b = Double.POSITIVE_INFINITY;
				intvl2.aClosed = false;
				intvl2.bClosed = false;
				sortedIntervals.put(intvl, intvl2);
			} else if (p.operator == Operator.LT || p.operator == Operator.LTE) {
				intvl.a = Double.NEGATIVE_INFINITY;
				intvl.aClosed = false;
				intvl.b = p.value;
				if (p.operator == Operator.LT)
					intvl.bClosed = false;
				else
					intvl.bClosed = true;
				sortedIntervals.put(intvl, null);
			} else if (p.operator == Operator.GT || p.operator == Operator.GTE) {
				intvl.a = p.value;
				intvl.b = Double.POSITIVE_INFINITY;
				intvl.bClosed = false;
				if (p.operator == Operator.GT)
					intvl.aClosed = false;
				else
					intvl.aClosed = true;
				sortedIntervals.put(intvl, null);
			}
		}

		List<Interval> intersection = new ArrayList<>();
		intersection.add(new Interval(sortedIntervals.firstKey()));
		if (sortedIntervals.get(sortedIntervals.firstKey()) != null) {
			intersection.add(new Interval(sortedIntervals.get(sortedIntervals
					.firstKey())));
		}

		for (Interval intvl : sortedIntervals.keySet()) {
			List<Interval> newIntersection = new ArrayList<>();
			if (sortedIntervals.get(intvl) == null) {
				for (int i = 0; i < intersection.size(); i++) {
					Interval intvli = intersection.get(i);
					Interval intrsct = intvl.intersect(intvli);
					if (intrsct != null) {
						newIntersection.add(intrsct);
					}
				}
			} else {
				Interval scndIntvl = sortedIntervals.get(intvl);
				for (int i = 0; i < intersection.size(); i++) {
					Interval curr = new Interval(intersection.get(i));
					Interval intrsct1 = intvl.intersect(curr);
					if (intrsct1 != null)
						newIntersection.add(intrsct1);

					Interval intrsct2 = scndIntvl.intersect(curr);
					if (intrsct2 != null)
						newIntersection.add(intrsct2);
				}
			}
			intersection = new ArrayList<>();
			intersection.addAll(newIntersection);
		}
		if (intersection.isEmpty()) {
			predicates.clear();
			return;
		} else {
			boolean isAInf;
			//System.out.println("Intersection: " + intersection);
			HashSet<AtomicPredicate> newPredicates = new HashSet<>();
			for (int i = 0; i < intersection.size(); i++) {
				isAInf = false;
				Interval in = intersection.get(i);
				if (in.a instanceof Double) {
					Double aVal = (Double) in.a;
					if (aVal == Double.NEGATIVE_INFINITY)
						isAInf = true;
				}
				if (!isAInf) {
					HashSet<AtomicPredicate> s = predsByValue.get(in.a);
					if (s.size() == 1)
						newPredicates.add(s.iterator().next());
					else {
						if (in.aClosed) {
							boolean change = false;
							for (AtomicPredicate p : s) {
								if (p.operator == Operator.EQ) {
									newPredicates.add(p);
									change = true;
								}
							}
							if (!change) {
								for (AtomicPredicate p : s) {
									if (p.operator == Operator.GTE) {
										newPredicates.add(p);
									}
								}
							}
						} else {
							boolean change = false;
							for (AtomicPredicate p : s) {
								if (p.operator == Operator.GT) {
									newPredicates.add(p);
									change = true;
								}
							}
							if (!change) {
								for (AtomicPredicate p : s) {
									if (p.operator == Operator.NEQ) {
										newPredicates.add(p);
									}
								}
							}
						}
					}
				}
				if (in.b instanceof Double) {
					Double bVal = (Double) in.b;
					if (bVal == Double.POSITIVE_INFINITY)
						continue;
				}

				HashSet<AtomicPredicate> sb = predsByValue.get(in.b);
				if (sb.size() == 1)
					newPredicates.add(sb.iterator().next());
				else {
					if (in.bClosed) {
						boolean change = false;
						for (AtomicPredicate p : sb) {
							if (p.operator == Operator.EQ) {
								newPredicates.add(p);
								change = true;
							}
						}
						if (!change) {
							for (AtomicPredicate p : sb) {
								if (p.operator == Operator.LTE) {
									newPredicates.add(p);
								}
							}
						}
					} else {
						boolean change = false;
						for (AtomicPredicate p : sb) {
							if (p.operator == Operator.LT) {
								newPredicates.add(p);
								change = true;
							}
						}
						if (!change) {
							for (AtomicPredicate p : sb) {
								if (p.operator == Operator.NEQ) {
									newPredicates.add(p);
								}
							}
						}
					}

				}
			}
			predicates.clear();
			predicates.addAll(newPredicates);
		}
	}

	public static ArrayList<Interval> getIntersection(
			Set<AtomicPredicate> predicates) {
		// HashSet<AtomicPredicate> res = new HashSet<>();

		Map<Object, HashSet<AtomicPredicate>> predsByValue = new HashMap<>();

		NavigableMap<Interval, Interval> sortedIntervals = new TreeMap<Interval, Interval>();
		for (AtomicPredicate p : predicates) {
			if (!predsByValue.containsKey(p.value)) {
				HashSet<AtomicPredicate> s = new HashSet<>();
				s.add(p);
				predsByValue.put(p.value, s);
			} else {
				predsByValue.get(p.value).add(p);
			}
			Interval intvl = new Interval();
			if (p.operator == Operator.EQ) {
				intvl.a = intvl.b = p.value;
				intvl.aClosed = intvl.bClosed = true;
				sortedIntervals.put(intvl, null);
			} else if (p.operator == Operator.NEQ) {
				intvl.a = Double.NEGATIVE_INFINITY;
				intvl.b = p.value;
				intvl.aClosed = false;
				intvl.bClosed = false;
				Interval intvl2 = new Interval();
				intvl2.a = p.value;
				intvl2.b = Double.POSITIVE_INFINITY;
				intvl2.aClosed = false;
				intvl2.bClosed = false;
				sortedIntervals.put(intvl, intvl2);
			} else if (p.operator == Operator.LT || p.operator == Operator.LTE) {
				intvl.a = Double.NEGATIVE_INFINITY;
				intvl.aClosed = false;
				intvl.b = p.value;
				if (p.operator == Operator.LT)
					intvl.bClosed = false;
				else
					intvl.bClosed = true;
				sortedIntervals.put(intvl, null);
			} else if (p.operator == Operator.GT || p.operator == Operator.GTE) {
				intvl.a = p.value;
				intvl.b = Double.POSITIVE_INFINITY;
				intvl.bClosed = false;
				if (p.operator == Operator.GT)
					intvl.aClosed = false;
				else
					intvl.aClosed = true;
				sortedIntervals.put(intvl, null);
			}
		}

		List<Interval> intersection = new ArrayList<>();
		intersection.add(new Interval(sortedIntervals.firstKey()));
		if (sortedIntervals.get(sortedIntervals.firstKey()) != null) {
			intersection.add(new Interval(sortedIntervals.get(sortedIntervals
					.firstKey())));
		}

		for (Interval intvl : sortedIntervals.keySet()) {
			List<Interval> newIntersection = new ArrayList<>();
			if (sortedIntervals.get(intvl) == null) {
				for (int i = 0; i < intersection.size(); i++) {
					Interval intvli = intersection.get(i);
					Interval intrsct = intvl.intersect(intvli);
					if (intrsct != null) {
						newIntersection.add(intrsct);
					}
				}
			} else {
				Interval scndIntvl = sortedIntervals.get(intvl);
				for (int i = 0; i < intersection.size(); i++) {
					Interval curr = new Interval(intersection.get(i));
					Interval intrsct1 = intvl.intersect(curr);
					if (intrsct1 != null)
						newIntersection.add(intrsct1);

					Interval intrsct2 = scndIntvl.intersect(curr);
					if (intrsct2 != null)
						newIntersection.add(intrsct2);
				}
			}
			intersection = new ArrayList<>();
			intersection.addAll(newIntersection);
		}
		return (ArrayList<Interval>) intersection;
	}

	// Returns "P1 and P2 and ... and Pk"
	public static String generatePredicateConjunction(Collection<AtomicPredicate> predicates) {
		StringBuilder sb = new StringBuilder();
		for(AtomicPredicate p : predicates) {
			sb.append(p.toTextPredicateRaw());
			sb.append(" AND ");
		}
		String predicateConjunction = sb.toString();
		return predicateConjunction.substring(0, predicateConjunction.length() - 5);
	}
	
	public static String generatePredicateConjunction(AtomicPredicate[] predicates) {
		StringBuilder sb = new StringBuilder();
		for(AtomicPredicate p : predicates) {
			sb.append(p.toTextPredicateRaw());
			sb.append(" AND ");
		}
		String predicateConjunction = sb.toString();
		return predicateConjunction.substring(0, predicateConjunction.length() - 5);
	}
	

	public static void main(String[] args) {
		/*List<AtomicPredicate> l = new ArrayList<AtomicPredicate>();
		l.add(new AtomicPredicate("public", "lineorder", "lo_orderdate", AttributeType.NUMERIC, Operator.GT, 2));
		l.add(new AtomicPredicate("public", "lineorder", "lo_orderdate", AttributeType.NUMERIC, Operator.GT, 3));
		l.add(new AtomicPredicate("public", "lineorder", "lo_orderdate", AttributeType.NUMERIC, Operator.LT, 5));
		HashMap<SimpleTable, ArrayList<HashSet<AtomicPredicate>>> res = generatePartitioningPredicates(l);
		System.out.println(res.size());
		for(SimpleTable t : res.keySet()) {
			System.out.println(t);
			for(HashSet<AtomicPredicate> s : res.get(t)){
				filter(s);
				if(!s.isEmpty())
				System.out.println(s);
			}
			System.out.println();
		}*/
	}
}