package org.partitioning;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.DBProperties;
import org.json.JSONException;

public class WorkloadQuery {

  private String sql;
  private List<AtomicPredicate> predicates;
  private double cost;
  private double[] realCostTime;

  public WorkloadQuery() {
    this.sql = "";
    this.predicates = new ArrayList<AtomicPredicate>();
    this.cost = 0.0;
  }

  public WorkloadQuery(String sql) {
    this.sql = sql;
    this.predicates = new ArrayList<AtomicPredicate>();
    this.cost = 0.0;
  }

  public String getSql() {
    return this.sql;
  }

  public void setSql(String sql) {
    this.sql = sql;
  }

  public List<AtomicPredicate> getPredicates() {
    return this.predicates;
  }

  public void setPredicates(List<AtomicPredicate> predicates) {
    this.predicates = predicates;
  }

  public AtomicPredicate getPredicateAt(int index) {
    if (index < this.predicates.size()) {
      return this.predicates.get(index);
    }

    return null;
  }

  public void extractPredicates() throws JSONException {
    List<AtomicPredicate> p = SqlParser.extractPredicates(this.sql);
    for (AtomicPredicate ap : p)
      this.predicates.add(ap);
  }

  @SuppressWarnings("unused")
  public double calculateExecutionCost(Map<SimpleTable, Set<AtomicPredicate>> partSchemaPreds, boolean simulated)
      throws JSONException, SQLException {

    this.cost = 0.0;

    if (predicates.isEmpty()) {
      extractPredicates();
    }
    ArrayList<SimpleTable> partitionsMatchForQuery = new ArrayList<>();
    HashMap<SimpleTable, HashSet<AtomicPredicate>> predicatesFromQueryByTable =
        AtomicPredicateUtils.createPredicateSetsForTables(predicates);
    Set<SimpleTable> tablesFromQuery = new HashSet<>();
    for (AtomicPredicate p : this.predicates) {
      SimpleTable t = new SimpleTable(p.schema, p.table);
      if (t.tablename != null) {
        tablesFromQuery.add(t);
      }
    }

    // Add the tables from the FROM clause
    String tablesFrom =
        this.sql.substring((this.sql.toLowerCase().indexOf("from") + 4), this.sql.toLowerCase().indexOf("where"));
    String[] tablesFrom_arr = tablesFrom.split(",");
    for (String tablename : tablesFrom_arr) {
      tablename = tablename.trim();
      SimpleTable stt = new SimpleTable("public", tablename);
      tablesFromQuery.add(stt);
    }

    for (SimpleTable t : tablesFromQuery) {

      HashMap<String, HashSet<AtomicPredicate>> predicatesForTableByAttr = new HashMap<>();
      HashSet<AtomicPredicate> predicatesForTable = predicatesFromQueryByTable.get(t);
      //System.out.println("KEY TABLE: " + t);
      //System.out.println("KEY SET: " + predicatesFromQueryByTable.keySet());
      //System.out.println("RESULT: " + predicatesForTable);

      if (predicatesForTable != null) {  // find the corresponding partitions where the query should be executed
        // one should only consider partitions of the tables included in the predicates of the where clause

        for (AtomicPredicate p : predicatesForTable) {
          if (!predicatesForTableByAttr.containsKey(p.attribute)) {
            HashSet<AtomicPredicate> s = new HashSet<AtomicPredicate>();
            s.add(p);
            predicatesForTableByAttr.put(p.attribute, s);
          } else {
            predicatesForTableByAttr.get(p.attribute).add(p);
          }
        }
      }

      for (SimpleTable tPart : partSchemaPreds.keySet()) {
        if (tPart.tablename.contains(t.tablename) && partSchemaPreds.get(tPart) != null) {
          HashMap<String, HashSet<AtomicPredicate>> predicatesForPartitionByAttr = new HashMap<>();
          HashSet<AtomicPredicate> predicatesForPartition = (HashSet<AtomicPredicate>) partSchemaPreds.get(tPart);
          for (AtomicPredicate p : predicatesForPartition) {
            if (!predicatesForPartitionByAttr.containsKey(p.attribute)) {
              HashSet<AtomicPredicate> s = new HashSet<>();
              s.add(p);
              predicatesForPartitionByAttr.put(p.attribute, s);
            } else {
              predicatesForPartitionByAttr.get(p.attribute).add(p);
            }
          }

          for (String tableAttr : predicatesForTableByAttr.keySet()) {

            if (predicatesForPartitionByAttr.containsKey(tableAttr)) {
              HashSet<AtomicPredicate> union = new HashSet<>();
              union.addAll(predicatesForTableByAttr.get(tableAttr));
              union.addAll(predicatesForPartitionByAttr.get(tableAttr));
              if (!AtomicPredicateUtils.getIntersection(union).isEmpty()) {
                partitionsMatchForQuery.add(tPart);
              }
            } else {
              partitionsMatchForQuery.add(tPart);
            }
          }
        } else if (partSchemaPreds.get(tPart) == null) {
          partitionsMatchForQuery.add(tPart);
        }
      }
    }

    // remove duplicates
    Set<SimpleTable> partitionsMatchForQueryDistinct = new HashSet<SimpleTable>();
    partitionsMatchForQueryDistinct.addAll(partitionsMatchForQuery);
    partitionsMatchForQuery = new ArrayList<SimpleTable>();
    partitionsMatchForQuery.addAll(partitionsMatchForQueryDistinct);

    // sega znaeme vo koi particii moze da se pusta query-to
    if (tablesFromQuery.size() == 1) {  // the query does not contain a join
      for (SimpleTable part : partitionsMatchForQuery) {
        String newSql = sql.replace(tablesFromQuery.iterator().next().tablename,
            String.format("%s.%s", part.schemaname, part.tablename));

        // Explicitly add the partitioning predicates in the conditions list of the "where" clause
        // This will allow the planner to make more accurate execution cost estimations
        ArrayList<AtomicPredicate> partitioningPredicates = new ArrayList<AtomicPredicate>();
        //find the key equal to part
        SimpleTable partCounterpart = null;
        for (SimpleTable key : partSchemaPreds.keySet()) {
          if (key.equals(part)) {
            partCounterpart = key;
            break;
          }
        }
        Set<AtomicPredicate> partitioningPredicatesSet = partSchemaPreds.get(part);
        // if this partition does indeed have predicates (it is not a full pseudo-partition, [the whole original table as a single partition])
        if (partitioningPredicatesSet != null && !partitioningPredicatesSet.isEmpty()) {
          partitioningPredicates.addAll(partitioningPredicatesSet);

          // Now, add these predicates to the beginning of the WHERE clause
          String sqlBeforeWhere = newSql.substring(0, newSql.toLowerCase().indexOf("where") + 5);
          String sqlAfterWhere = newSql.substring(newSql.toLowerCase().indexOf("where") + 6, newSql.length());
          StringBuilder partitioningPredicatesConjunction = new StringBuilder();
          AtomicPredicate firstPartitioningPredicate = partitioningPredicates.get(0);
          partitioningPredicatesConjunction.append(firstPartitioningPredicate.toTextPredicateWithoutSchema());
          for (int p_cnt = 1; p_cnt < partitioningPredicates.size(); p_cnt++) {
            AtomicPredicate partitioningPredicate = partitioningPredicates.get(p_cnt);
            partitioningPredicatesConjunction.append(" AND " + partitioningPredicate.toTextPredicateWithoutSchema());
          }
          newSql = sqlBeforeWhere + " " + partitioningPredicatesConjunction.toString() + " AND " + sqlAfterWhere;
        }
        double cost_newsql = PostgresDBService.getCost(newSql, simulated);
        this.cost += cost_newsql;
      }
    } else {
      ArrayList<SimpleTable[]> partitionsByTable = new ArrayList<>();
      for (SimpleTable t : tablesFromQuery) {
        ArrayList<SimpleTable> parts = new ArrayList<SimpleTable>();
        for (SimpleTable p : partitionsMatchForQuery) {
          if (p.tablename.contains(t.tablename)) {
            parts.add(p);
          }
        }
        SimpleTable[] partsarr = new SimpleTable[parts.size()];
        partitionsByTable.add(parts.toArray(partsarr));
      }

      calculateCrossProductCost(partitionsByTable, partSchemaPreds, simulated);
    }

    return this.cost;
    /*
		 * 1. Sovpagjanje na intervali za izbor na particii
		 * 2. Ako ima join, kako da se implementira (dali site kombinacii na particii)?
		 * go ima podole ama dali e toa toa?
		 */

  }

  @SuppressWarnings("unused")
  public double[] calculateExecutionCostAndTime(Map<SimpleTable, Set<AtomicPredicate>> partSchemaPreds,
      boolean simulated, DBProperties dbProperties) throws JSONException, SQLException, ClassNotFoundException {

    this.cost = 0.0;
    this.realCostTime = new double[2];
    this.realCostTime[0] = 0;
    this.realCostTime[1] = 0;

    if (predicates.isEmpty()) {
      extractPredicates();
    }
    ArrayList<SimpleTable> partitionsMatchForQuery = new ArrayList<>();
    HashMap<SimpleTable, HashSet<AtomicPredicate>> predicatesFromQueryByTable =
        AtomicPredicateUtils.createPredicateSetsForTables(predicates);
    Set<SimpleTable> tablesFromQuery = new HashSet<>();
    for (AtomicPredicate p : this.predicates) {
      SimpleTable t = new SimpleTable(p.schema, p.table);
      if (t.tablename != null) {
        tablesFromQuery.add(t);
      }
    }

    // Add the tables from the FROM clause
    String tablesFrom =
        this.sql.substring((this.sql.toLowerCase().indexOf("from") + 4), this.sql.toLowerCase().indexOf("where"));
    String[] tablesFrom_arr = tablesFrom.split(",");
    for (String tablename : tablesFrom_arr) {
      tablename = tablename.trim();
      SimpleTable stt = new SimpleTable("public", tablename);
      tablesFromQuery.add(stt);
    }

    for (SimpleTable t : tablesFromQuery) {

      HashMap<String, HashSet<AtomicPredicate>> predicatesForTableByAttr = new HashMap<>();
      HashSet<AtomicPredicate> predicatesForTable = predicatesFromQueryByTable.get(t);

      if (predicatesForTable != null) {  // find the corresponding partitions where the query should be executed
        // one should only consider partitions of the tables included in the predicates of the where clause

        for (AtomicPredicate p : predicatesForTable) {
          if (!predicatesForTableByAttr.containsKey(p.attribute)) {
            HashSet<AtomicPredicate> s = new HashSet<AtomicPredicate>();
            s.add(p);
            predicatesForTableByAttr.put(p.attribute, s);
          } else {
            predicatesForTableByAttr.get(p.attribute).add(p);
          }
        }
      }

      for (SimpleTable tPart : partSchemaPreds.keySet()) {
        if (tPart.tablename.contains(t.tablename) && partSchemaPreds.get(tPart) != null) {
          HashMap<String, HashSet<AtomicPredicate>> predicatesForPartitionByAttr = new HashMap<>();
          HashSet<AtomicPredicate> predicatesForPartition = (HashSet<AtomicPredicate>) partSchemaPreds.get(tPart);
          for (AtomicPredicate p : predicatesForPartition) {
            if (!predicatesForPartitionByAttr.containsKey(p.attribute)) {
              HashSet<AtomicPredicate> s = new HashSet<>();
              s.add(p);
              predicatesForPartitionByAttr.put(p.attribute, s);
            } else {
              predicatesForPartitionByAttr.get(p.attribute).add(p);
            }
          }

          for (String tableAttr : predicatesForTableByAttr.keySet()) {

            if (predicatesForPartitionByAttr.containsKey(tableAttr)) {
              HashSet<AtomicPredicate> union = new HashSet<>();
              union.addAll(predicatesForTableByAttr.get(tableAttr));
              union.addAll(predicatesForPartitionByAttr.get(tableAttr));
              if (!AtomicPredicateUtils.getIntersection(union).isEmpty()) {
                partitionsMatchForQuery.add(tPart);
              }
            } else {
              partitionsMatchForQuery.add(tPart);
            }
          }
        } else if (partSchemaPreds.get(tPart) == null) {
          partitionsMatchForQuery.add(tPart);
        }
      }
    }

    // remove duplicates
    Set<SimpleTable> partitionsMatchForQueryDistinct = new HashSet<SimpleTable>();
    partitionsMatchForQueryDistinct.addAll(partitionsMatchForQuery);
    partitionsMatchForQuery = new ArrayList<SimpleTable>();
    partitionsMatchForQuery.addAll(partitionsMatchForQueryDistinct);

    // sega znaeme vo koi particii moze da se pusta query-to
    if (tablesFromQuery.size() == 1) {  // the query does not contain a join
      for (SimpleTable part : partitionsMatchForQuery) {
        String newSql = sql.replace(tablesFromQuery.iterator().next().tablename,
            String.format("%s.%s", part.schemaname, part.tablename));

        // Explicitly add the partitioning predicates in the conditions list of the "where" clause
        // This will allow the planner to make more accurate execution cost estimations
        ArrayList<AtomicPredicate> partitioningPredicates = new ArrayList<AtomicPredicate>();
        //find the key equal to part
        SimpleTable partCounterpart = null;
        for (SimpleTable key : partSchemaPreds.keySet()) {
          if (key.equals(part)) {
            partCounterpart = key;
            break;
          }
        }
        Set<AtomicPredicate> partitioningPredicatesSet = partSchemaPreds.get(part);
        // if this partition does indeed have predicates (it is not a full pseudo-partition, [the whole original table as a single partition])
        if (partitioningPredicatesSet != null && !partitioningPredicatesSet.isEmpty()) {
          partitioningPredicates.addAll(partitioningPredicatesSet);

          // Now, add these predicates to the beginning of the WHERE clause
          String sqlBeforeWhere = newSql.substring(0, newSql.toLowerCase().indexOf("where") + 5);
          String sqlAfterWhere = newSql.substring(newSql.toLowerCase().indexOf("where") + 6, newSql.length());
          StringBuilder partitioningPredicatesConjunction = new StringBuilder();
          AtomicPredicate firstPartitioningPredicate = partitioningPredicates.get(0);
          partitioningPredicatesConjunction.append(firstPartitioningPredicate.toTextPredicateWithoutSchema());
          for (int p_cnt = 1; p_cnt < partitioningPredicates.size(); p_cnt++) {
            AtomicPredicate partitioningPredicate = partitioningPredicates.get(p_cnt);
            partitioningPredicatesConjunction.append(" AND " + partitioningPredicate.toTextPredicateWithoutSchema());
          }
          newSql = sqlBeforeWhere + " " + partitioningPredicatesConjunction.toString() + " AND " + sqlAfterWhere;
        }
        double[] analyzeCostAndTime = PostgresDBService.getRealCostAndTime(newSql, simulated, dbProperties);
        this.realCostTime[0] += analyzeCostAndTime[0];
        this.realCostTime[1] += analyzeCostAndTime[1];
      }
    } else {
      ArrayList<SimpleTable[]> partitionsByTable = new ArrayList<>();
      for (SimpleTable t : tablesFromQuery) {
        ArrayList<SimpleTable> parts = new ArrayList<SimpleTable>();
        for (SimpleTable p : partitionsMatchForQuery) {
          if (p.tablename.contains(t.tablename)) {
            parts.add(p);
          }
        }
        SimpleTable[] partsarr = new SimpleTable[parts.size()];
        partitionsByTable.add(parts.toArray(partsarr));
      }

      calculateCrossProductCostAndTime(partitionsByTable, partSchemaPreds, simulated, dbProperties);
    }

    return this.realCostTime;
		/*
		 * 1. Sovpagjanje na intervali za izbor na particii
		 * 2. Ako ima join, kako da se implementira (dali site kombinacii na particii)?
		 * go ima podole ama dali e toa toa?
		 */

  }

  private void calculateCrossProductCost(ArrayList<SimpleTable[]> tabs,
      Map<SimpleTable, Set<AtomicPredicate>> partSchemaPreds, boolean simulated) throws SQLException, JSONException {
    combine(0, tabs, partSchemaPreds, new SimpleTable[tabs.size()], simulated);
  }

  private void combine(int k, ArrayList<SimpleTable[]> tabs, Map<SimpleTable, Set<AtomicPredicate>> partSchemaPreds,
      SimpleTable[] container, boolean simulated) throws SQLException, JSONException {
    if (k == tabs.size()) {

      String newSql = this.sql;
      Set<String> replacedStrings = new HashSet<>();
      for (int i = 0; i < container.length; i++) {
        if (replacedStrings.contains(container[i].tablename)) {
          continue;
        }
        String toReplace = container[i].tablename.substring(0, container[i].tablename.indexOf('_'));
        String replacement = String.format("%s.%s", container[i].schemaname, container[i].tablename);
        for (int j = 0; j < newSql.length() - toReplace.length() - 1; j++) {
          boolean match = true;
          for (int l = j; l < j + toReplace.length(); l++) {
            if (newSql.charAt(l) != toReplace.charAt(l - j)) {
              match = false;
              break;
            }
          }
          if (j > 0) {
            if (Character.isLetter(newSql.charAt(j - 1))) {
              match = false;
            }
          }
          if (Character.isLetter(newSql.charAt(j + toReplace.length()))) {
            match = false;
          }
          if (match) {
            newSql = newSql.substring(0, j) + replacement + newSql.substring((j + toReplace.length()));
            j = j + replacement.length();
            replacedStrings.add(container[i].tablename);
          }
        }

        // find the key in partSchemaPreds that is equal to container[i]
        SimpleTable containerCounterpart = null;
        for (SimpleTable key : partSchemaPreds.keySet()) {
          if (key.equals(container[i])) {
            containerCounterpart = key;
            break;
          }
        }

        // get the partitioning predicates for this partition
        ArrayList<AtomicPredicate> partitioningPredicatesForPartition = new ArrayList<AtomicPredicate>();
        Set<AtomicPredicate> partitioningPredicatesSet = partSchemaPreds.get(containerCounterpart);
        if (partitioningPredicatesSet != null && !partitioningPredicatesSet.isEmpty()) {
          partitioningPredicatesForPartition.addAll(partitioningPredicatesSet);

          // get the sql before and after the where clause
          String sqlBeforeWhere = newSql.substring(0, newSql.toLowerCase().indexOf("where") + 5);
          String sqlAfterWhere = newSql.substring(newSql.toLowerCase().indexOf("where") + 6, newSql.length());

          // construct a conjunction of the partitioning predicates for this partition
          StringBuilder partitioningPredicatesConjunction = new StringBuilder();
          AtomicPredicate first = partitioningPredicatesForPartition.get(0);
          partitioningPredicatesConjunction.append(first.toTextPredicateWithoutSchema());
          for (int p_cnt = 1; p_cnt < partitioningPredicatesForPartition.size(); p_cnt++) {
            partitioningPredicatesConjunction.append(
                " AND " + partitioningPredicatesForPartition.get(p_cnt).toTextPredicateWithoutSchema());
          }

          newSql = sqlBeforeWhere + " " + partitioningPredicatesConjunction.toString() + " AND " + sqlAfterWhere;
        }
      }

      double cost_newsql = PostgresDBService.getCost(newSql, simulated);
      this.cost += cost_newsql;
    } else {
      for (int i = 0; i < tabs.get(k).length; i++) {
        container[k] = tabs.get(k)[i];
        combine(k + 1, tabs, partSchemaPreds, container, simulated);
      }
    }
  }

  private void calculateCrossProductCostAndTime(ArrayList<SimpleTable[]> tabs,
      Map<SimpleTable, Set<AtomicPredicate>> partSchemaPreds, boolean simulated, DBProperties dbProperties)
      throws SQLException, JSONException, ClassNotFoundException {
    combineCostAndTime(0, tabs, partSchemaPreds, new SimpleTable[tabs.size()], simulated, dbProperties);
  }

  private void combineCostAndTime(int k, ArrayList<SimpleTable[]> tabs,
      Map<SimpleTable, Set<AtomicPredicate>> partSchemaPreds, SimpleTable[] container, boolean simulated,
      DBProperties dbProperties)
      throws SQLException, JSONException, ClassNotFoundException {
    if (k == tabs.size()) {

      String newSql = this.sql;
      Set<String> replacedStrings = new HashSet<>();
      for (int i = 0; i < container.length; i++) {
        if (replacedStrings.contains(container[i].tablename)) {
          continue;
        }
        String toReplace = container[i].tablename.substring(0, container[i].tablename.indexOf('_'));
        String replacement = String.format("%s.%s", container[i].schemaname, container[i].tablename);
        for (int j = 0; j < newSql.length() - toReplace.length() - 1; j++) {
          boolean match = true;
          for (int l = j; l < j + toReplace.length(); l++) {
            if (newSql.charAt(l) != toReplace.charAt(l - j)) {
              match = false;
              break;
            }
          }
          if (j > 0) {
            if (Character.isLetter(newSql.charAt(j - 1))) {
              match = false;
            }
          }
          if (Character.isLetter(newSql.charAt(j + toReplace.length()))) {
            match = false;
          }
          if (match) {
            newSql = newSql.substring(0, j) + replacement + newSql.substring((j + toReplace.length()));
            j = j + replacement.length();
            replacedStrings.add(container[i].tablename);
          }
        }

        // find the key in partSchemaPreds that is equal to container[i]
        SimpleTable containerCounterpart = null;
        for (SimpleTable key : partSchemaPreds.keySet()) {
          if (key.equals(container[i])) {
            containerCounterpart = key;
            break;
          }
        }

        // get the partitioning predicates for this partition
        ArrayList<AtomicPredicate> partitioningPredicatesForPartition = new ArrayList<AtomicPredicate>();
        Set<AtomicPredicate> partitioningPredicatesSet = partSchemaPreds.get(containerCounterpart);
        if (partitioningPredicatesSet != null && !partitioningPredicatesSet.isEmpty()) {
          partitioningPredicatesForPartition.addAll(partitioningPredicatesSet);

          // get the sql before and after the where clause
          String sqlBeforeWhere = newSql.substring(0, newSql.toLowerCase().indexOf("where") + 5);
          String sqlAfterWhere = newSql.substring(newSql.toLowerCase().indexOf("where") + 6, newSql.length());

          // construct a conjunction of the partitioning predicates for this partition
          StringBuilder partitioningPredicatesConjunction = new StringBuilder();
          AtomicPredicate first = partitioningPredicatesForPartition.get(0);
          partitioningPredicatesConjunction.append(first.toTextPredicateWithoutSchema());
          for (int p_cnt = 1; p_cnt < partitioningPredicatesForPartition.size(); p_cnt++) {
            partitioningPredicatesConjunction.append(
                " AND " + partitioningPredicatesForPartition.get(p_cnt).toTextPredicateWithoutSchema());
          }

          newSql = sqlBeforeWhere + " " + partitioningPredicatesConjunction.toString() + " AND " + sqlAfterWhere;
        }
      }

      double[] analyzeCostAndTime = PostgresDBService.getRealCostAndTime(newSql, simulated, dbProperties);
      this.realCostTime[0] += analyzeCostAndTime[0];
      this.realCostTime[1] += analyzeCostAndTime[1];
    } else {
      for (int i = 0; i < tabs.get(k).length; i++) {
        container[k] = tabs.get(k)[i];
        combineCostAndTime(k + 1, tabs, partSchemaPreds, container, simulated, dbProperties);
      }
    }
  }

  public void executeAsUpdate(DBProperties properties) throws ClassNotFoundException {
    try {
      PostgresDBService.executeUpdate(this.sql, properties);
    } catch (SQLException se) {
      se.printStackTrace();
    }
  }

  public ResultSet executeAsQuery(DBProperties dbProperties) throws ClassNotFoundException {
    ResultSet res = null;
    try {
      res = PostgresDBService.executeQuery(this.sql, dbProperties);
    } catch (SQLException se) {
      se.printStackTrace();
    }
    return res;
  }
}
