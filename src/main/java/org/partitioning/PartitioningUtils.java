package org.partitioning;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import org.DBProperties;
import org.json.JSONException;

public class PartitioningUtils {

  private static void createMasterTables(String schemaName, String pathToMasterTablesDDL, DBProperties dbProperties)
      throws SQLException, FileNotFoundException, ClassNotFoundException {
    Scanner s = null;
    String script = null;
    try {
      s = new Scanner(new File(pathToMasterTablesDDL));
      script = s.useDelimiter("\\Z").next();
    } finally {
      s.close();
    }
    script = script.replace("CREATE TABLE ", "CREATE TABLE " + schemaName + ".");
    script = script.replace("REFERENCES ", "REFERENCES " + schemaName + ".");
    PostgresDBService.executeSqlScript(script, ";", dbProperties);
  }

  public static Map<SimpleTable, Set<AtomicPredicate>> createPartitionedSchema(String newschema,
      List<AtomicPredicate> allPredicates, DBProperties dbProperties)
      throws FileNotFoundException, SQLException, ClassNotFoundException {

    Map<SimpleTable, Set<AtomicPredicate>> result = new HashMap<>();
    HashMap<SimpleTable, ArrayList<HashSet<AtomicPredicate>>> hm =
        AtomicPredicateUtils.generatePartitioningPredicates(allPredicates);

    List<String> partitionedSchemaDDL = new ArrayList<>();
    List<HashSet<AtomicPredicate>> allPartitionsPredicates = null;
    for (SimpleTable table : hm.keySet()) {

      SimpleTable masterTable = new SimpleTable(newschema, table.tablename + "_master");

      allPartitionsPredicates = hm.get(table);
      partition_loop:
      for (int i = 0; i < allPartitionsPredicates.size(); i++) {
        HashSet<AtomicPredicate> hs = allPartitionsPredicates.get(i);
        Set<AtomicPredicate> predicatesForPartition = new HashSet<>();
        for (AtomicPredicate p : hs) {
          predicatesForPartition.add(
              new AtomicPredicate(newschema, (table.tablename + "_" + i), p.attribute, p.attributeType, p.operator,
                  p.value));
        }
        //System.out.println(predicatesForPartition);
        HashMap<String, HashSet<AtomicPredicate>> predsByAttr = new HashMap<>();
        // podeli po atributi i filtriraj mnozestva
        // posle unija na filtriranite mnozestva
        for (AtomicPredicate p : predicatesForPartition) {
          if (!predsByAttr.containsKey(p.attribute)) {
            HashSet<AtomicPredicate> s = new HashSet<>();
            s.add(p);
            predsByAttr.put(p.attribute, s);
          } else {
            predsByAttr.get(p.attribute).add(p);
          }
        }

        // ako po barem eden atribut nema presek => kontradiktorno
        predicatesForPartition.clear();
        boolean contradictory = false;
        for (String attr : predsByAttr.keySet()) {
          HashSet<AtomicPredicate> predsForThisAttr = predsByAttr.get(attr);
          AtomicPredicateUtils.filter(predsForThisAttr);
          if (!predsForThisAttr.isEmpty()) {
            predicatesForPartition.addAll(predsForThisAttr);
          } else {
            contradictory = true;
            break;
          }
        }

        if (!predicatesForPartition.isEmpty() && !contradictory) {
          // This combination may be a duplicate due to reduction to eliminate contradictory predicates
          for (SimpleTable existingPartition : result.keySet()) {
            HashSet<AtomicPredicate> existingPartitionPredicates =
                (HashSet<AtomicPredicate>) result.get(existingPartition);
            if (existingPartitionPredicates.equals(predicatesForPartition)) {
              continue partition_loop;
            }
          }
          SimpleTable partition = new SimpleTable(newschema, (table.tablename + "_" + i));
          result.put(partition, predicatesForPartition);
          partitionedSchemaDDL.add(getCreatePartitionSQL(partition, masterTable, predicatesForPartition));
          partitionedSchemaDDL.add(getInsertOneRowSQL(partition, table, predicatesForPartition));
          partitionedSchemaDDL.add("ANALYZE " + partition.toTextTable());
          //partitionedSchemaDDL.add("analyze " + partition);
          //partitionedSchemaDDL.add("vacuum " + partition);
          //partitionedSchemaDDL.add(getAnalyzeSQL());
          //partitionedSchemaDDL.add(getClearTableSQL(partition));
          //partitionedSchemaDDL.add(getVacuumSQL());
        }
      }
    }

    for (SimpleTable table : TableMapping.tableAttributes.keySet()) {
      if (!hm.containsKey(table)) {
        SimpleTable partition = new SimpleTable(newschema, (table.tablename + "_0"));
        SimpleTable masterTable = new SimpleTable(newschema, (table.tablename + "_master"));
        result.put(partition, null);
        partitionedSchemaDDL.add(getCreatePseudoPartitionSQL(partition, masterTable));
        partitionedSchemaDDL.add(getInsertOneRowSQL(partition, table, null));
        partitionedSchemaDDL.add("ANALYZE " + partition.toTextTable());
        //partitionedSchemaDDL.add("analyze " + partition);
        //partitionedSchemaDDL.add("vacuum " + partition);
        //partitionedSchemaDDL.add(getAnalyzeSQL());
        //artitionedSchemaDDL.add(getClearTableSQL(partition));
        //partitionedSchemaDDL.add(getVacuumSQL());
      }
    }

    //partitionedSchemaDDL.add(getAnalyzeSQL());

    for (SimpleTable newlyCreatedPartition : result.keySet()) {
      partitionedSchemaDDL.add(getClearTableSQL(newlyCreatedPartition));
      //partitionedSchemaDDL.add("VACUUM " + newlyCreatedPartition.toTextTable());
    }

		/*for(SimpleTable table : TableMapping.tableAttributes.keySet()) {

			// promeneto, treba da go ima ovoj if uslov kako pogore (ima samo edna particija togas taa tabela ne se sodrzi vo hm - nema predikati za nea)
			if(!hm.containsKey(table)) {
				SimpleTable partition = new SimpleTable(newschema, (table.tablename + "_0"));
				partitionedSchemaDDL.add(getClearTableSQL(partition));
			}
		}*/

    //partitionedSchemaDDL.add(getVacuumSQL());

    PostgresDBService.executeUpdate("CREATE SCHEMA IF NOT EXISTS " + newschema + ";", dbProperties);
    createMasterTables(newschema, "ssb_master.ddl", dbProperties);
    for (String sqlStatement : partitionedSchemaDDL) {
      PostgresDBService.executeUpdate(sqlStatement, dbProperties);
    }
    return result;
  }

  public static Map<SimpleTable, Set<AtomicPredicate>> createPartitionedSchema(String nonPartitionedSchema,
      String newSchemaName,
      File workloadFile, DBProperties dbProperties)
      throws ClassNotFoundException, SQLException, FileNotFoundException, JSONException {

    String script = null;
    Map<SimpleTable, Set<AtomicPredicate>> result = new HashMap<>();
    if (workloadFile != null) {
      Scanner s = null;

      try {
        s = new Scanner(workloadFile);
        script = s.useDelimiter("\\Z").next();
      } finally {
        s.close();
      }
    } else {
      script =
          Constants.WORKLOAD;
    }

    if (script != null) {
      List<String> stmts = PostgresDBService.getStatementsFromSqlScript(script, ";");
      List<List<AtomicPredicate>> predicates = new ArrayList<>();
      for (String statement : stmts) {
        try {
          predicates.add(SqlParser.extractPredicates(statement));
        } catch (JSONException je) {
          System.out.println(String.format("Predicate extraction for \"%s\" failed!", statement));
          je.printStackTrace();
        }
      }

      List<AtomicPredicate> allPredicates = mergeLists(predicates);
      HashMap<SimpleTable, ArrayList<HashSet<AtomicPredicate>>> hm =
          AtomicPredicateUtils.generatePartitioningPredicates(allPredicates);

      List<String> partitionedSchemaDDL = new ArrayList<>();
      List<HashSet<AtomicPredicate>> allPartitionsPredicates = null;
      for (SimpleTable table : hm.keySet()) {

        SimpleTable masterTable = new SimpleTable(newSchemaName, table.tablename + "_master");

        allPartitionsPredicates = hm.get(table);
        partition_loop_file:
        for (int i = 0; i < allPartitionsPredicates.size(); i++) {
          HashSet<AtomicPredicate> hs = allPartitionsPredicates.get(i);
          Set<AtomicPredicate> predicatesForPartition = new HashSet<>();
          for (AtomicPredicate p : hs) {
            predicatesForPartition.add(
                new AtomicPredicate(newSchemaName, (table.tablename + "_" + i), p.attribute, p.attributeType,
                    p.operator, p.value));
          }
          //System.out.println(predicatesForPartition);
          HashMap<String, HashSet<AtomicPredicate>> predsByAttr = new HashMap<>();
          // podeli po atributi i filtriraj mnozestva
          // posle unija na filtriranite mnozestva
          for (AtomicPredicate p : predicatesForPartition) {
            if (!predsByAttr.containsKey(p.attribute)) {
              HashSet<AtomicPredicate> sd = new HashSet<>();
              sd.add(p);
              predsByAttr.put(p.attribute, sd);
            } else {
              predsByAttr.get(p.attribute).add(p);
            }
          }
          //System.out.println("BEFORE FILTER<><><><><><> " + predicatesForPartition);

          // ako po barem eden atribut nema presek => kontradiktorno
          predicatesForPartition.clear();
          boolean contradictory = false;
          for (String attr : predsByAttr.keySet()) {
            HashSet<AtomicPredicate> predsForThisAttr = predsByAttr.get(attr);
            AtomicPredicateUtils.filter(predsForThisAttr);
            if (!predsForThisAttr.isEmpty()) {
              predicatesForPartition.addAll(predsForThisAttr);
            } else {
              contradictory = true;
              break;
            }
          }
          //System.out.println("AFTER FILTER<><><><><><> " + predicatesForPartition);

          if (!predicatesForPartition.isEmpty() && !contradictory) {
            // This combination may be a duplicate due to reduction to eliminate contradictory predicates
            for (SimpleTable existingPartition : result.keySet()) {
              HashSet<AtomicPredicate> existingPartitionPredicates =
                  (HashSet<AtomicPredicate>) result.get(existingPartition);
              if (existingPartitionPredicates.equals(predicatesForPartition)) {
                continue partition_loop_file;
              }
            }
            SimpleTable partition = new SimpleTable(newSchemaName, (table.tablename + "_" + i));
            result.put(partition, predicatesForPartition);
            partitionedSchemaDDL.add(getCreatePartitionSQL(partition, masterTable, predicatesForPartition));
            partitionedSchemaDDL.add(getInsertOneRowSQL(partition, table, predicatesForPartition));
            partitionedSchemaDDL.add("ANALYZE " + partition.toTextTable());
            //partitionedSchemaDDL.add("ANALYZE " + partition);
            //partitionedSchemaDDL.add("vacuum " + partition);
            //partitionedSchemaDDL.add(getAnalyzeSQL());
            //partitionedSchemaDDL.add(getClearTableSQL(partition));
            //partitionedSchemaDDL.add(getVacuumSQL());
          }
        }
      }

      for (SimpleTable table : TableMapping.tableAttributes.keySet()) {
        if (!hm.containsKey(table)) {
          SimpleTable partition = new SimpleTable(newSchemaName, (table.tablename + "_0"));
          SimpleTable masterTable = new SimpleTable(newSchemaName, (table.tablename + "_master"));
          result.put(partition, null);
          partitionedSchemaDDL.add(getCreatePseudoPartitionSQL(partition, masterTable));
          partitionedSchemaDDL.add(getInsertOneRowSQL(partition, table, null));
          partitionedSchemaDDL.add("ANALYZE " + partition.toTextTable());
          //partitionedSchemaDDL.add("ANALYZE " + partition);
          //partitionedSchemaDDL.add("vacuum " + partition);
          //partitionedSchemaDDL.add(getAnalyzeSQL());
          //artitionedSchemaDDL.add(getClearTableSQL(partition));
          //partitionedSchemaDDL.add(getVacuumSQL());
        }
      }

      //partitionedSchemaDDL.add(getAnalyzeSQL());

      for (SimpleTable table : TableMapping.tableAttributes.keySet()) {
        SimpleTable partition = new SimpleTable(newSchemaName, (table.tablename + "_0"));
        partitionedSchemaDDL.add(getClearTableSQL(partition));
      }

      //partitionedSchemaDDL.add(getVacuumSQL());

      PostgresDBService.executeUpdate("CREATE SCHEMA IF NOT EXISTS " + newSchemaName + ";", dbProperties);
      createMasterTables(newSchemaName, "ssb_master.ddl", dbProperties);
      for (String sqlStatement : partitionedSchemaDDL) {
        //System.out.println(sqlStatement);
        PostgresDBService.executeUpdate(sqlStatement, dbProperties);
      }
    }

    return result;
  }

  private static String getVacuumSQL() {
    return "vacuum;";
  }

  private static String getAnalyzeSQL() {
    return "analyze;";
  }

  private static String getClearTableSQL(SimpleTable table) {
    return "DELETE FROM " + table.toTextTable() + ";";
  }

  private static String getCreatePartitionSQL(SimpleTable partition, SimpleTable parentTable,
      Set<AtomicPredicate> predicates) {
    return "CREATE TABLE " + partition.toTextTable() + " ( "
        + "CHECK (" + generatePredicateConjunctionSQL(predicates) + ")) "
        + "INHERITS(" + parentTable.toTextTable() + ");";
  }

  private static String getCreatePseudoPartitionSQL(SimpleTable partition, SimpleTable parentTable) {
    return "CREATE TABLE " + partition.toTextTable() + " () "
        + "INHERITS(" + parentTable.toTextTable() + ");";
  }

  private static String getInsertOneRowSQL(SimpleTable targetPartition, SimpleTable parentTable,
      Set<AtomicPredicate> predicates) {
    if (predicates == null) {
      return "INSERT INTO " + targetPartition.toTextTable()
          + " SELECT * FROM " + parentTable.toTextTable()
          + " LIMIT 1;";
    } else {
      return "INSERT INTO " + targetPartition.toTextTable()
          + " SELECT * FROM " + parentTable.toTextTable() + " WHERE "
          + generatePredicateConjunctionSQL(predicates)
          + " LIMIT 1;";
    }
  }

  private static String generatePredicateConjunctionSQL(Set<AtomicPredicate> predicates) {
    StringBuilder sb = new StringBuilder();
    for (AtomicPredicate p : predicates) {
      sb.append(p.toTextPredicateRaw()).append(" AND ");
    }
    String result = sb.toString().substring(0, sb.lastIndexOf(" AND "));
    return result;
  }

  //private static

  public static boolean isContradictory(Set<AtomicPredicate> predicates) {
    Map<String, List<AtomicPredicate>> predicatesByAttribute = new HashMap<>();
    for (AtomicPredicate p : predicates) {
      if (!predicatesByAttribute.containsKey(p.attribute)) {
        List<AtomicPredicate> l = new ArrayList<>();
        l.add(p);
        predicatesByAttribute.put(p.attribute, l);
      } else {
        predicatesByAttribute.get(p.attribute).add(p);
      }
    }
    for (String attr : predicatesByAttribute.keySet()) {
      ArrayList<AtomicPredicate> l = (ArrayList<AtomicPredicate>) predicatesByAttribute.get(attr);
      if (l.size() > 1) {
        for (int i = 0; i < l.size() - 1; i++) {
          for (int j = i + 1; j < l.size(); j++) {
            if (l.get(i).value instanceof String) {    //l[j] is also a string
              String si = (String) l.get(i).value;
              String sj = (String) l.get(j).value;
              Operator oi = l.get(i).operator;
              Operator oj = l.get(j).operator;
              String stmp;
              Operator otmp;
              if (si.compareTo(sj) > 0) {
                stmp = si;
                si = sj;
                sj = stmp;
                otmp = oi;
                oi = oj;
                oj = otmp;
              }
              if (si.equals(sj)) {
                if (oi == Operator.GT) {
                  if (oj == Operator.LT || oj == Operator.EQ || oj == Operator.LTE) return true;
                } else if (oi == Operator.LT) {
                  if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                } else if (oi == Operator.LTE) {
                  if (oj == Operator.GT) return true;
                } else if (oi == Operator.GTE) {
                  if (oj == Operator.LT) return true;
                } else if (oi == Operator.EQ) {
                  if (oj.equality == 0) return true;
                } else if (oi == Operator.NEQ) {
                  if (oj == Operator.EQ) return true;
                }
              } else {
                if (oi == Operator.LT || oi == Operator.LTE) {
                  if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                } else if (oi == Operator.EQ) {
                  if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                }
              }
            } else {
              if (l.get(i).value instanceof Double) {
                double ni = (Double) l.get(i).value;
                double nj = (Double) l.get(j).value;
                Operator oi = l.get(i).operator;
                Operator oj = l.get(j).operator;
                Operator otmp;
                double ntmp;
                if (ni > nj) {
                  ntmp = ni;
                  ni = nj;
                  nj = ntmp;
                  otmp = oi;
                  oi = oj;
                  oj = otmp;
                }
                if (ni == nj) {
                  if (oi == Operator.GT) {
                    if (oj == Operator.LT || oj == Operator.EQ || oj == Operator.LTE) return true;
                  } else if (oi == Operator.LT) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.LTE) {
                    if (oj == Operator.GT) return true;
                  } else if (oi == Operator.GTE) {
                    if (oj == Operator.LT) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj.equality == 0) return true;
                  } else if (oi == Operator.NEQ) {
                    if (oj == Operator.EQ) return true;
                  }
                } else {
                  if (oi == Operator.LT || oi == Operator.LTE) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  }
                }
              } else if (l.get(i).value instanceof Float) {
                float ni = (float) l.get(i).value;
                float nj = (float) l.get(j).value;
                Operator oi = l.get(i).operator;
                Operator oj = l.get(j).operator;
                Operator otmp;
                float ntmp;
                if (ni > nj) {
                  ntmp = ni;
                  ni = nj;
                  nj = ntmp;
                  otmp = oi;
                  oi = oj;
                  oj = otmp;
                }
                if (ni == nj) {
                  if (oi == Operator.GT) {
                    if (oj == Operator.LT || oj == Operator.EQ || oj == Operator.LTE) return true;
                  } else if (oi == Operator.LT) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.LTE) {
                    if (oj == Operator.GT) return true;
                  } else if (oi == Operator.GTE) {
                    if (oj == Operator.LT) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj.equality == 0) return true;
                  } else if (oi == Operator.NEQ) {
                    if (oj == Operator.EQ) return true;
                  }
                } else {
                  if (oi == Operator.LT || oi == Operator.LTE) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  }
                }
              } else if (l.get(i).value instanceof Long) {
                long ni = (long) l.get(i).value;
                long nj = (long) l.get(j).value;
                Operator oi = l.get(i).operator;
                Operator oj = l.get(j).operator;
                Operator otmp;
                long ntmp;
                if (ni > nj) {
                  ntmp = ni;
                  ni = nj;
                  nj = ntmp;
                  otmp = oi;
                  oi = oj;
                  oj = otmp;
                }
                if (ni == nj) {
                  if (oi == Operator.GT) {
                    if (oj == Operator.LT || oj == Operator.EQ || oj == Operator.LTE) return true;
                  } else if (oi == Operator.LT) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.LTE) {
                    if (oj == Operator.GT) return true;
                  } else if (oi == Operator.GTE) {
                    if (oj == Operator.LT) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj.equality == 0) return true;
                  } else if (oi == Operator.NEQ) {
                    if (oj == Operator.EQ) return true;
                  }
                } else {
                  if (oi == Operator.LT || oi == Operator.LTE) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  }
                }
              } else if (l.get(i).value instanceof Integer) {
                int ni = (int) l.get(i).value;
                int nj = (int) l.get(j).value;
                Operator oi = l.get(i).operator;
                Operator oj = l.get(j).operator;
                Operator otmp;
                int ntmp;
                if (ni > nj) {
                  ntmp = ni;
                  ni = nj;
                  nj = ntmp;
                  otmp = oi;
                  oi = oj;
                  oj = otmp;
                }
                if (ni == nj) {
                  if (oi == Operator.GT) {
                    if (oj == Operator.LT || oj == Operator.EQ || oj == Operator.LTE) return true;
                  } else if (oi == Operator.LT) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.LTE) {
                    if (oj == Operator.GT) return true;
                  } else if (oi == Operator.GTE) {
                    if (oj == Operator.LT) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj.equality == 0) return true;
                  } else if (oi == Operator.NEQ) {
                    if (oj == Operator.EQ) return true;
                  }
                } else {
                  if (oi == Operator.LT || oi == Operator.LTE) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  }
                }
              } else if (l.get(i).value instanceof Short) {
                short ni = (short) l.get(i).value;
                short nj = (short) l.get(j).value;
                Operator oi = l.get(i).operator;
                Operator oj = l.get(j).operator;
                Operator otmp;
                short ntmp;
                if (ni > nj) {
                  ntmp = ni;
                  ni = nj;
                  nj = ntmp;
                  otmp = oi;
                  oi = oj;
                  oj = otmp;
                }
                if (ni == nj) {
                  if (oi == Operator.GT) {
                    if (oj == Operator.LT || oj == Operator.EQ || oj == Operator.LTE) return true;
                  } else if (oi == Operator.LT) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.LTE) {
                    if (oj == Operator.GT) return true;
                  } else if (oi == Operator.GTE) {
                    if (oj == Operator.LT) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj.equality == 0) return true;
                  } else if (oi == Operator.NEQ) {
                    if (oj == Operator.EQ) return true;
                  }
                } else {
                  if (oi == Operator.LT || oi == Operator.LTE) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  }
                }
              } else if (l.get(i).value instanceof Byte) {
                byte ni = (byte) l.get(i).value;
                byte nj = (byte) l.get(j).value;
                Operator oi = l.get(i).operator;
                Operator oj = l.get(j).operator;
                Operator otmp;
                byte ntmp;
                if (ni > nj) {
                  ntmp = ni;
                  ni = nj;
                  nj = ntmp;
                  otmp = oi;
                  oi = oj;
                  oj = otmp;
                }
                if (ni == nj) {
                  if (oi == Operator.GT) {
                    if (oj == Operator.LT || oj == Operator.EQ || oj == Operator.LTE) return true;
                  } else if (oi == Operator.LT) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.LTE) {
                    if (oj == Operator.GT) return true;
                  } else if (oi == Operator.GTE) {
                    if (oj == Operator.LT) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj.equality == 0) return true;
                  } else if (oi == Operator.NEQ) {
                    if (oj == Operator.EQ) return true;
                  }
                } else {
                  if (oi == Operator.LT || oi == Operator.LTE) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  } else if (oi == Operator.EQ) {
                    if (oj == Operator.GT || oj == Operator.GTE || oj == Operator.EQ) return true;
                  }
                }
              }
            }
          }
        }
      }
    }
    return false;
  }

  private static List<AtomicPredicate> mergeLists(List<List<AtomicPredicate>> lists) {
    List<AtomicPredicate> list = new ArrayList<AtomicPredicate>();
    for (List<AtomicPredicate> l : lists) {
      list.addAll(l);
    }
    return list;
  }
	
	/*public static void main(String[] args) {
		Operator o1 = Operator.EQ;
		Operator o2 = Operator.NEQ;
		System.out.println(o1==o2);
	}*/
}
