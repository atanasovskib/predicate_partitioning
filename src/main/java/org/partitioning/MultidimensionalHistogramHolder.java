package org.partitioning;

import java.io.Externalizable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.DBProperties;
import org.ProjectConstants;
import org.multidimhistogram.MultidimensionalHistogram;
import org.multidimhistogram.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultidimensionalHistogramHolder implements Externalizable {
  private static final Logger log = LoggerFactory.getLogger(MultidimensionalHistogramHolder.class);
  private Map<SimpleTable, MultidimensionalHistogram> histograms;

  public MultidimensionalHistogramHolder() {
  }

  private MultidimensionalHistogramHolder(Map<SimpleTable, MultidimensionalHistogram> histograms) {
    this.histograms = histograms;
  }

  private static MultidimensionalHistogramHolder deserialize() throws IOException, ClassNotFoundException {
    log.debug("Deserializing MultidimensionalHistogramHolder from file {}",
        ProjectConstants.serializedMultiDimensionalHistogramsFileName);
    ObjectInput input =
        new ObjectInputStream(new FileInputStream(ProjectConstants.serializedMultiDimensionalHistogramsFileName));
    return (MultidimensionalHistogramHolder) input.readObject();
  }

  private static void serialize(MultidimensionalHistogramHolder holder) throws IOException {
    ObjectOutput output =
        new ObjectOutputStream(new FileOutputStream(ProjectConstants.serializedMultiDimensionalHistogramsFileName));
    output.writeObject(holder);
    output.close();
  }

  public static MultidimensionalHistogramHolder generateHistograms(AtomicPredicate[] predicates,
      DBProperties dbProperties)
      throws ClassNotFoundException, SQLException {

    if (serializedFileExists()) {
      try {
        return deserialize();
      } catch (IOException e) {
        log.error("could not deserialize from saved file, error: {}", e.getMessage());
      }
    }

    // regroup predicates by table
    Map<SimpleTable, List<AtomicPredicate>> predicatesByTable = new HashMap<>();
    for (AtomicPredicate p : predicates) {
      SimpleTable t = p.extractTable();
      if (!predicatesByTable.containsKey(t)) {
        List<AtomicPredicate> pList = new ArrayList<>();
        pList.add(p);
        predicatesByTable.put(t, pList);
      } else {
        predicatesByTable.get(t).add(p);
      }
    }

    Map<SimpleTable, MultidimensionalHistogram> histograms = new HashMap<>();
    for (SimpleTable t : predicatesByTable.keySet()) {
      MultidimensionalHistogram histogram = new MultidimensionalHistogram(t, predicatesByTable.get(t));
      histogram.fillDistinctValues(dbProperties);
      histogram.fillEntries(dbProperties);
      histograms.put(t, histogram);
    }

    MultidimensionalHistogramHolder holder = new MultidimensionalHistogramHolder(histograms);
    try {
      serialize(holder);
    } catch (IOException e) {
      log.debug("could not serialize to file, error: {}", e.getMessage());
    }

    return holder;
  }

  private static boolean serializedFileExists() {
    log.debug("Checking if serialized multidim histograms exist in file: {}",
        ProjectConstants.serializedMultiDimensionalHistogramsFileName);
    return Files.exists(Paths.get(ProjectConstants.serializedMultiDimensionalHistogramsFileName));
  }

  public List<Pair<String, Integer>> getOccurences(SimpleTable table, String attribute,
      Collection<AtomicPredicate> predicates) {
    return histograms.get(table).getOccurrences(attribute, predicates);
  }

  @Override public void writeExternal(ObjectOutput objectOutput) throws IOException {
    objectOutput.writeObject(this.histograms);
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    MultidimensionalHistogramHolder that = (MultidimensionalHistogramHolder) o;

    return histograms.equals(that.histograms);
  }

  @Override public int hashCode() {
    return histograms.hashCode();
  }

  @Override public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
    this.histograms = (Map<SimpleTable, MultidimensionalHistogram>) objectInput.readObject();
  }
}
