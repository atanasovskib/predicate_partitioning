package org.partitioning;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class WorkloadPredicates implements Externalizable {

  private SimpleTable[] nonPartitionedTables;
  private AtomicPredicate[] predicates;

  public WorkloadPredicates() {

  }

  public WorkloadPredicates(SimpleTable[] nonPartitionedTables, AtomicPredicate[] predicates) {
    this.nonPartitionedTables = nonPartitionedTables;
    this.predicates = predicates;
  }

  public void writeExternal(ObjectOutput objectOutput) throws IOException {
    objectOutput.writeBoolean(nonPartitionedTables == null);
    if (nonPartitionedTables != null) {
      objectOutput.writeInt(nonPartitionedTables.length);
      for (SimpleTable table : nonPartitionedTables) {
        objectOutput.writeObject(table);
      }
    }

    objectOutput.writeInt(predicates.length);
    for (AtomicPredicate predicate : predicates) {
      objectOutput.writeObject(predicate);
    }
  }

  public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
    boolean tablesAreNull = objectInput.readBoolean();
    if (!tablesAreNull) {
      int numTables = objectInput.readInt();
      nonPartitionedTables = new SimpleTable[numTables];
      for (int i = 0; i < numTables; i++) {
        nonPartitionedTables[i] = (SimpleTable) objectInput.readObject();
      }
    }

    int numPredicates = objectInput.readInt();
    predicates = new AtomicPredicate[numPredicates];
    for (int i = 0; i < numPredicates; i++) {
      predicates[i] = (AtomicPredicate) objectInput.readObject();
    }
  }

  public AtomicPredicate getPredicate(int index) {
    return this.predicates[index];
  }

  public AtomicPredicate[] getPredicates(){
    return this.predicates;
  }
}
