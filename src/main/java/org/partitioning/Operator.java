package org.partitioning;

public enum Operator {

	LT("<", 0, 0), GT(">", 1, 0), LTE("<=", 0, 1), GTE(">=", 1, 1), EQ("=", 2, 1), NEQ("<>", 3, 0);
	
	public final String code;
	public final int direction;	// 0: <,<= 1: >,>=
	public final int equality;
	
	Operator(String code, int direction, int equality) {
		this.code = code;
		this.direction = direction;
		this.equality = equality;
	}
}
