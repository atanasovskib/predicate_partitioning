package org.partitioning;

public final class DataType {

	public static final String[] NUMERIC = new String[] {
		"smallint",
		"integer",
		"bigint",
		"decimal",
		"numeric",
		"real",
		"double precision",
		"smallserial",
		"serial",
		"bigserial"
	};
	
	public static final String[] TEXT = new String[] {
		"character varying",
		"varchar",
		"character",
		"char",
		"text"
	};
}
