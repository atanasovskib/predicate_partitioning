package org.partitioning;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* An atomic predicate of the form (attribute, operator, value) */
public class AtomicPredicate implements Comparable<AtomicPredicate>, Externalizable {

  public String schema;
  public String table;
  public String attribute;
  public AttributeType attributeType;
  public Operator operator;
  public Object value;

  public AtomicPredicate(String schema, String table, String attribute, AttributeType attributeType, Operator operator,
      Object value) {
    if (schema != null) {
      if (schema.equals("$nil")) {
        this.schema = null;
      } else {
        this.schema = schema;
      }
    } else {
      this.schema = null;
    }
    this.table = table;
    this.attribute = attribute;
    this.attributeType = attributeType;
    this.operator = operator;
    this.value = value;
  }

  public AtomicPredicate() {
  }

  public AtomicPredicate(String table, String attribute, AttributeType attributeType, Operator operator, Object value) {
    this.schema = null;
    this.table = table;
    this.attribute = attribute;
    this.attributeType = attributeType;
    this.operator = operator;
    this.value = value;
  }

  public static AtomicPredicate inverse(AtomicPredicate p) {
    switch (p.operator) {
      case EQ:
        return new AtomicPredicate(p.schema, p.table, p.attribute, p.attributeType, Operator.NEQ, p.value);
      case NEQ:
        return new AtomicPredicate(p.schema, p.table, p.attribute, p.attributeType, Operator.EQ, p.value);
      case LT:
        return new AtomicPredicate(p.schema, p.table, p.attribute, p.attributeType, Operator.GTE, p.value);
      case GT:
        return new AtomicPredicate(p.schema, p.table, p.attribute, p.attributeType, Operator.LTE, p.value);
      case LTE:
        return new AtomicPredicate(p.schema, p.table, p.attribute, p.attributeType, Operator.GT, p.value);
      case GTE:
        return new AtomicPredicate(p.schema, p.table, p.attribute, p.attributeType, Operator.LT, p.value);
      default:
        throw new IllegalArgumentException(String.format(
            "Invalid operator %s. This operator is not supported yet.", p.operator.name()));
    }
  }

  @Override
  public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
    boolean isSchemaNull = objectInput.readBoolean();
    if (!isSchemaNull) {
      this.schema = objectInput.readUTF();
    }

    this.table = objectInput.readUTF();
    this.attribute = objectInput.readUTF();
    boolean isTypeNull = objectInput.readBoolean();
    if (!isTypeNull) {
      this.attributeType = AttributeType.values()[objectInput.readInt()];
    }

    this.operator = Operator.values()[objectInput.readInt()];
    this.value = objectInput.readObject();
  }

  public void writeExternal(ObjectOutput out) throws IOException {
    out.writeBoolean(this.schema == null);
    if (this.schema != null) {
      out.writeUTF(this.schema);
    }

    out.writeUTF(this.table);
    out.writeUTF(this.attribute);
    out.writeBoolean(this.attributeType == null);
    if (this.attributeType != null) {
      out.writeInt(attributeType.ordinal());
    }

    out.writeInt(this.operator.ordinal());
    out.writeObject(this.value);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result
        + ((attribute == null) ? 0 : attribute.hashCode());
    result = prime * result
        + ((attributeType == null) ? 0 : attributeType.hashCode());
    result = prime * result
        + ((operator == null) ? 0 : operator.hashCode());
    result = prime * result + ((schema == null) ? 0 : schema.hashCode());
    result = prime * result + ((table == null) ? 0 : table.hashCode());
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    AtomicPredicate other = (AtomicPredicate) obj;
    if (attribute == null) {
      if (other.attribute != null) {
        return false;
      }
    } else if (!attribute.equals(other.attribute)) {
      return false;
    }
    if (attributeType != other.attributeType) {
      return false;
    }
    if (operator != other.operator) {
      return false;
    }
    if (schema == null) {
      if (other.schema != null) {
        return false;
      }
    } else if (!schema.equals(other.schema)) {
      return false;
    }
    if (table == null) {
      if (other.table != null) {
        return false;
      }
    } else if (!table.equals(other.table)) {
      return false;
    }
    if (value == null) {
      if (other.value != null) {
        return false;
      }
    } else if (!value.equals(other.value)) {
      return false;
    }
    return true;
  }

  public SimpleTable extractTable() {
    if (schema != null && table != null) {
      return new SimpleTable(schema, table);
    } else if (schema == null && table != null) {
      return new SimpleTable(Constants.NONPARTITIONED_SCHEMA, table);
    } else {
      return null;
    }
  }

  @Override
  public String toString() {
    return this.toTextPredicate();
  }

  public String toTextPredicate() {
    if (this.schema != null) {
      return String.format("%s.%s.%s %s '%s'", this.schema, this.table, this.attribute, this.operator.code,
          String.valueOf(this.value));
    }
    return String.format("%s.%s %s '%s'", this.table, this.attribute, this.operator.code, String.valueOf(this.value));
  }

  public String toTextPredicateWithoutSchema() {
    return String.format("%s.%s %s '%s'", this.table, this.attribute, this.operator.code, String.valueOf(this.value));
  }

  public String toTextPredicateRaw() {
    return String.format("%s %s '%s'", this.attribute, this.operator.code, String.valueOf(this.value));
  }

  @Override
  public int compareTo(AtomicPredicate other) {
    return 0;
  }
}
