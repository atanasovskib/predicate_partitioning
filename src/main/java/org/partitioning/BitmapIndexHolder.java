package org.partitioning;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.DBProperties;
import org.ProjectConstants;
import org.bitmapindex.ColumnarBitmapIndex;
import org.bitmapindex.ColumnarBitmapIndex.ColumnarBitmapIndexIterator;
import org.bitmapindex.ColumnarBitmapIndex.ColumnarIndexElement;
import org.multidimhistogram.Pair;
import org.roaringbitmap.RoaringBitmap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BitmapIndexHolder {
  private static final Logger log = LoggerFactory.getLogger(BitmapIndexHolder.class);
  static private BitmapIndexHolder _instance = null;
  public Map<AtomicPredicate, ColumnarBitmapIndex> bitmapIndexes_Distinct =
      new HashMap<>();
  public Map<AtomicPredicate, ColumnarBitmapIndex> bitmapIndexes_Rows =
      new HashMap<>();
  private boolean isIndexByDistinctInitialized = false;
  private boolean isIndexByRowsInitialized = false;

  private BitmapIndexHolder() {
  }

  static public BitmapIndexHolder getInstance() {
    if (_instance == null) {
      _instance = new BitmapIndexHolder();
    }
    return _instance;
  }

  // predicates: all predicates extracted from the workload
  public void generateBitmapIndexesByDistinct(AtomicPredicate[] predicates, DBProperties dbProperties)
      throws ClassNotFoundException, SQLException {
    log.debug("Generating Bitmap indexes by distinct");
    if (isIndexByDistinctInitialized) {
      return;
    }

    if (serializedFileExists()) {
      try {
        deserialize();
        return;
      } catch (IOException e) {
        log.error("could not deseriaze from saved file, error: {}", e.getMessage());
      }
    }

    String sql = "select res2.row_number-1 "
        + "from (select distinct %s from %s where %s) res1 "
        + "inner join (select x.%s, row_number() over(order by x.%s) as row_number from (select distinct %s from %s) x) res2 on res1.%s = res2.%s "
        + "order by res2.row_number";
    log.debug("sql in generate bitmap: {}", sql);
    for (AtomicPredicate p : predicates) {
      log.debug("atomic predicate: {}", p);
      SimpleTable t = p.extractTable();
      HashSet<String> attributes = TableMapping.tableAttributes.get(t);
      AtomicPredicate pInverse = AtomicPredicate.inverse(p);
      ColumnarBitmapIndex bitmapIndex = new ColumnarBitmapIndex(t);
      ColumnarBitmapIndex bitmapIndexInverse = new ColumnarBitmapIndex(t);
      for (String a : attributes) {
        String query = String.format(sql, a, t.toTextTable(),
            p.toTextPredicateRaw(), a, a, a, t.toTextTable(), a, a);
        RoaringBitmap r = new RoaringBitmap();
        ResultSet rs = PostgresDBService.executeQuery(query, dbProperties);
        while (rs.next()) {
          r.add(rs.getInt(1));
        }
        rs.close();
        bitmapIndex.addElement(a, r);

        String query2 = String.format(sql, a, t.toTextTable(),
            pInverse.toTextPredicateRaw(), a, a, a,
            t.toTextTable(), a, a);
        RoaringBitmap rInverse = new RoaringBitmap();
        ResultSet rsInverse = PostgresDBService.executeQuery(query2, dbProperties);
        while (rsInverse.next()) {
          rInverse.add(rsInverse.getInt(1));
        }
        rsInverse.close();
        bitmapIndexInverse.addElement(a, rInverse);
      }
      log.debug("Predicate done.");
      bitmapIndexes_Distinct.put(p, bitmapIndex);
      bitmapIndexes_Distinct.put(pInverse, bitmapIndexInverse);
    }

    isIndexByDistinctInitialized = true;
    try {
      serialize();
    } catch (IOException e) {
      log.debug("could not serialize to file, error: {}", e.getMessage());
    }

		/*
     * for(AtomicPredicate p : predicates) { ArrayList<RoaringBitmap>
		 * bitmaps = new ArrayList<RoaringBitmap>(); ArrayList<RoaringBitmap>
		 * bitmapsInverse = new ArrayList<RoaringBitmap>(); SimpleTable
		 * predicateTable = new SimpleTable(p.schema, p.table); AtomicPredicate
		 * pInverse = AtomicPredicate.inverse(p);
		 * 
		 * for(SimpleTable t : TableMapping.tableAttributes.keySet()) {
		 * if(t.tablename.equals(p.table) &&
		 * TableMapping.tableAttributes.get(t).contains(p.attribute)) {
		 * predicateTable = t; break; } }
		 * 
		 * TreeSet<String> attSetSorted = new TreeSet<String>(); // required to
		 * preserve order
		 * attSetSorted.addAll(TableMapping.tableAttributes.get(predicateTable
		 * ));
		 * 
		 * for(String attribute : attSetSorted) { String sql =
		 * "select res2.row_number-1 " +
		 * "from (select distinct %s from %s where %s) res1 " +
		 * "inner join (select x.%s, row_number() over(order by x.%s) as row_number from (select distinct %s from %s) x) res2 on res1.%s = res2.%s "
		 * + "order by res2.row_number";
		 * 
		 * String query = String.format(sql, attribute,
		 * predicateTable.toTextTable(), p.toTextPredicateRaw(), attribute,
		 * attribute, attribute, predicateTable.toTextTable(), attribute,
		 * attribute); ResultSet rs = PostgresDBService.executeQuery(query);
		 * RoaringBitmap bitmap = new RoaringBitmap(); while(rs.next()) {
		 * bitmap.add(Integer.valueOf(rs.getString(1))); } rs.close();
		 * bitmaps.add(bitmap);
		 * 
		 * String queryInverse = String.format(sql, attribute,
		 * predicateTable.toTextTable(), pInverse.toTextPredicateRaw(),
		 * attribute, attribute, attribute, predicateTable.toTextTable(),
		 * attribute, attribute); ResultSet rsInverse =
		 * PostgresDBService.executeQuery(queryInverse); RoaringBitmap
		 * bitmapInverse = new RoaringBitmap(); while(rsInverse.next()) {
		 * bitmapInverse.add(Integer.valueOf(rsInverse.getString(1))); }
		 * rsInverse.close(); bitmapsInverse.add(bitmapInverse); }
		 * 
		 * predicateBitmaps.put(p, bitmaps); predicateBitmaps.put(pInverse,
		 * bitmapsInverse); }
		 */
  }

  private void serialize() throws IOException {
    try (ObjectOutput output = new ObjectOutputStream(
        new FileOutputStream(ProjectConstants.serializedBitmapIndexHolderFileName))) {
      output.writeObject(this.bitmapIndexes_Distinct);
      output.writeObject(this.bitmapIndexes_Rows);
      output.writeBoolean(isIndexByDistinctInitialized);
      output.writeBoolean(isIndexByRowsInitialized);
    }
  }

  private void deserialize() throws IOException, ClassNotFoundException {
    log.debug("Deserializing BitmapIndexHolder from file {}", ProjectConstants.serializedBitmapIndexHolderFileName);
    ObjectInputStream input =
        new ObjectInputStream(new FileInputStream(ProjectConstants.serializedBitmapIndexHolderFileName));
    readExternal(input);
    input.close();
  }

  private void readExternal(ObjectInputStream input) throws IOException, ClassNotFoundException {
    this.bitmapIndexes_Distinct = (Map<AtomicPredicate, ColumnarBitmapIndex>) input.readObject();
    this.bitmapIndexes_Rows = (Map<AtomicPredicate, ColumnarBitmapIndex>) input.readObject();

    isIndexByDistinctInitialized = input.readBoolean();
    isIndexByRowsInitialized = input.readBoolean();
  }
  private boolean serializedFileExists() {
    log.debug("Checking if serialized indexes exist in file: {}", ProjectConstants.serializedBitmapIndexHolderFileName);
    return Files.exists(Paths.get(ProjectConstants.serializedBitmapIndexHolderFileName));
  }

  public Map<String, Integer> getCountDistinct(Collection<AtomicPredicate> predicates) {

    ColumnarBitmapIndex[] indexes = new ColumnarBitmapIndex[predicates.size()];
    int pos = 0;
    for (AtomicPredicate p : predicates)
      indexes[pos++] = bitmapIndexes_Distinct.get(p);

    ColumnarBitmapIndex intersection = ColumnarBitmapIndex.and(indexes);
    Map<String, Integer> countDistinct = new TreeMap<String, Integer>();
    ColumnarBitmapIndexIterator it = intersection.iterator();
    while (it.hasNext()) {
      Pair<String, ColumnarIndexElement> p = it.next();
      countDistinct.put(p.key, p.value.getCardinality());
    }

    return countDistinct;

		/*Map<String, Integer> countDistinct = new TreeMap<String, Integer>();
    TreeSet<String> attSetSorted = new TreeSet<String>();	// we need this to preserve order of indexing (i-th attribute must match)
		SimpleTable t;
		if(predicates.get(0).schema != null)
			t = new SimpleTable(predicates.get(0).schema, predicates.get(0).table);
		else
			t=  new SimpleTable("public", predicates.get(0).table);
		
		attSetSorted.addAll(TableMapping.tableAttributes.get(t));

		int listPos = 0;
		for(String attribute : attSetSorted) {
			int pos = 0;
			RoaringBitmap[] bitmaps = new RoaringBitmap[predicates.size()];
			for(AtomicPredicate p : predicates) {
				bitmaps[pos++] = predicateBitmaps.get(p).get(listPos);
			}
			RoaringBitmap intersection = FastAggregation.and(bitmaps);
			countDistinct.put(attribute, intersection.getCardinality());;
			listPos++;
		}
		return countDistinct;*/
  }

  // predicates: all predicates that were extracted from the workload
  public void generateBitmapIndexesByRows(AtomicPredicate[] predicates, DBProperties dbProperties)
      throws ClassNotFoundException, SQLException {

    if (isIndexByRowsInitialized) {
      return;
    }

    if (isIndexByDistinctInitialized) {
      return;
    }

    if (serializedFileExists()) {
      try {
        deserialize();
        return;
      } catch (IOException e) {
        log.error("could not deseriaze from saved file, error: {}", e.getMessage());
      }
    }

    String sql = "select rownum - 1 from (select row_number() over(order by %s) as rownum, %s from %s) x where %s";

    for (AtomicPredicate p : predicates) {
      SimpleTable t = p.extractTable();
      AtomicPredicate pInverse = AtomicPredicate.inverse(p);
      ColumnarBitmapIndex index = new ColumnarBitmapIndex(t);
      ColumnarBitmapIndex indexInverse = new ColumnarBitmapIndex(t);
      RoaringBitmap r = new RoaringBitmap();
      RoaringBitmap rInverse = new RoaringBitmap();

      String query = String.format(sql, getPrimaryKeyAsString(t),
          p.attribute, t.toTextTable(), p.toTextPredicateRaw());
      ResultSet rs = PostgresDBService.executeQuery(query, dbProperties);
      while (rs.next()) {
        r.add(rs.getInt(1));
      }
      rs.close();

      query = String.format(sql, getPrimaryKeyAsString(t),
          pInverse.attribute, t.toTextTable(),
          pInverse.toTextPredicateRaw());
      ResultSet rsInverse = PostgresDBService.executeQuery(query, dbProperties);
      while (rsInverse.next()) {
        rInverse.add(rsInverse.getInt(1));
      }
      rsInverse.close();

      index.addElement(p.attribute, r);
      indexInverse.addElement(p.attribute, rInverse);

      bitmapIndexes_Rows.put(p, index);
      bitmapIndexes_Rows.put(pInverse, indexInverse);
    }

    isIndexByRowsInitialized = true;
    try {
      serialize();
    } catch (IOException e) {
      log.debug("could not serialize to file, error: {}", e.getMessage());
    }
    /*
     * for(AtomicPredicate p : predicates) { SimpleTable t = new
		 * SimpleTable("public", p.table); if(p.schema != null) {
		 * if(!p.schema.isEmpty()) t.schemaname = p.schema; } RoaringBitmap
		 * bitmap = new RoaringBitmap(); String sql =
		 * "select rownum - 1 from (select row_number() over(order by " +
		 * getPrimaryKeyAsString(t) + ") as rownum, " + p.attribute + " from " +
		 * t + ") x where " + p.toTextPredicateRaw(); ResultSet rs =
		 * PostgresDBService.executeQuery(sql); int bitIndex; while(rs.next()) {
		 * bitIndex = Integer.valueOf(rs.getString(1)); bitmap.add(bitIndex); }
		 * rs.close(); predicateBitmaps.put(p, bitmap);
		 * 
		 * // do the same for the inverted predicate AtomicPredicate pInverse =
		 * AtomicPredicate.inverse(p); RoaringBitmap bitmapInverse = new
		 * RoaringBitmap(); sql =
		 * "select rownum - 1 from (select row_number() over(order by " +
		 * getPrimaryKeyAsString(t) + ") as rownum, " + pInverse.attribute +
		 * " from " + t + ") x where " + pInverse.toTextPredicateRaw();
		 * ResultSet rsInverse = PostgresDBService.executeQuery(sql);
		 * while(rsInverse.next()) { bitIndex =
		 * Integer.valueOf(rsInverse.getString(1)); bitmapInverse.add(bitIndex);
		 * } rs.close(); predicateBitmaps.put(pInverse, bitmapInverse); }
		 */

  }

  // predicates: predicates over the same table
  public int selectCountAll(List<AtomicPredicate> predicates) {
    ColumnarBitmapIndex[] indexes = new ColumnarBitmapIndex[predicates
        .size()];
    int pos = 0;
    for (AtomicPredicate p : predicates)
      indexes[pos++] = bitmapIndexes_Rows.get(p);
    ColumnarBitmapIndex intersection = ColumnarBitmapIndex.and(indexes);
    ColumnarBitmapIndexIterator it = intersection.iterator();
    Pair<String, ColumnarIndexElement> pair = it.next();
    return pair.value.getCardinality();

		/*
     * SimpleTable t = new SimpleTable("public", predicates.get(0).table);
		 * if(predicates.get(0).schema != null) {
		 * if(!predicates.get(0).schema.isEmpty()) t.schemaname =
		 * predicates.get(0).schema; } if(predicates.size() == 1) { return
		 * predicateBitmaps.get(predicates.get(0)).getCardinality(); } else {
		 * RoaringBitmap[] bitmaps = new RoaringBitmap[predicates.size()]; int
		 * pos = 0; for(AtomicPredicate p : predicates) { bitmaps[pos++] =
		 * predicateBitmaps.get(p); } RoaringBitmap intersection =
		 * FastAggregation.and(bitmaps); return intersection.getCardinality(); }
		 */
  }

  private String getPrimaryKeyAsString(SimpleTable table) {
    return String.format(
        "(select array_to_string(array(select a.attname from pg_index i join pg_attribute a on a.attrelid = i.indrelid and a.attnum = any(i.indkey) "
            + "where i.indrelid = '%s'::regclass "
            + "and i.indisprimary), ','))", table.toTextTable());
  }
}