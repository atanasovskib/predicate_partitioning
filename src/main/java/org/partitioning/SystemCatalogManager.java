package org.partitioning;

// FREKVENCIITE OD MCV NA ORIGINALNATA TABELA NE MORA DA SE CUVAAT
// MCV MOZE VO TREESET NAMESTO TREEMAP
// MEMORISKA KOMPLEKSNOST OVDE E BR. NA RAZLICNI VREDNOSTI VO CELATA TABELA
// KAJ HISTOGRAMOT E OD BROJOT NA RAZLICNI VREDNOSTI DO CELATA TABELA MAX

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import org.DBProperties;
import org.multidimhistogram.Pair;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SystemCatalogManager {
  private static final Logger log = LoggerFactory.getLogger(SystemCatalogManager.class);
  private String schemaname;
  private String relname;
  private List<PgStatisticData> pgStatistic;
  private PgClassData pgClass;
  private MultidimensionalHistogramHolder histograms;

  public SystemCatalogManager(String schemaname, String relname, MultidimensionalHistogramHolder histograms) {
    this.schemaname = schemaname;
    this.relname = relname;
    this.pgStatistic = new ArrayList<>();
    this.pgClass = new PgClassData();
    this.histograms = Objects.requireNonNull(histograms);
  }

  public static int compare(Object lhs, Object rhs) {
    if (lhs instanceof Integer && rhs instanceof Integer) {
      return ((Integer) lhs).compareTo((Integer) rhs);
    } else if (lhs instanceof Long && rhs instanceof Long) {
      return ((Long) lhs).compareTo((Long) rhs);
    } else if (lhs instanceof Short && rhs instanceof Short) {
      return ((Short) lhs).compareTo((Short) rhs);
    } else if (lhs instanceof Double && rhs instanceof Double) {
      return ((Double) lhs).compareTo((Double) rhs);
    } else if (lhs instanceof String && rhs instanceof String) {
      return ((String) lhs).compareTo((String) rhs);
    }
    return -2;
  }

  public String getSchemaname() {
    return schemaname;
  }

  public void setSchemaname(String schemaname) {
    this.schemaname = schemaname;
  }

  public String getRelname() {
    return relname;
  }

  public void setRelname(String relname) {
    this.relname = relname;
  }

  public List<PgStatisticData> getPgStatistic() {
    return pgStatistic;
  }

  public void setPgStatistic(List<PgStatisticData> pgStatistic) {
    this.pgStatistic = pgStatistic;
  }

  public PgClassData getPgClass() {
    return pgClass;
  }

  public void setPgClass(PgClassData pgClass) {
    this.pgClass = pgClass;
  }

  public void addPgStatistic(PgStatisticData psd) {
    if (this.pgStatistic != null) {
      this.pgStatistic.add(psd);
    }
  }

  public void retrieveMasterTableCatalogData(TableMetadata tableMetadata) throws ClassNotFoundException,
      SQLException {
    // AKO TREBA DA SE VRATI RELTUPLES
    PreparedStatement pgClassPs = PostgresDBService
        .getPreparedStatement(
            "SELECT pc.relpages "
                + "FROM pg_class pc JOIN pg_tables pt ON pc.relname = pt.tablename "
                + "WHERE pt.schemaname = ? AND pt.tablename = ?;",
            this.schemaname, this.relname);
    ResultSet pgClassRes = PostgresDBService
        .executeQueryParameterized(pgClassPs);
    if (pgClassRes.next()) {
      //System.out.println("RELTUPLES RETRIEVED: " + pgClassRes.getLong(1));
      SimpleTable relation = new SimpleTable(this.schemaname, this.relname);
      pgClass.reltuples = tableMetadata.getRows().get(relation);
      pgClass.relpages = pgClassRes.getLong(1);
    }

    PreparedStatement pgStatisticPs = PostgresDBService
        .getPreparedStatement(
            "SELECT ps.attname,format_type(a.atttypid,NULL),s.stainherit,s.stanullfrac,s.stawidth,s.stadistinct,"
                + "s.stakind1,s.stakind2,s.stakind3,s.stakind4,s.stakind5,"
                + "s.staop1,s.staop2,s.staop3,s.staop4,s.staop5,"
                + "regexp_replace(regexp_replace(regexp_replace('{'||array_to_string(most_common_vals::anyarray, ',')||'}', '(,+|,*(, )+,*)', ',', 'g'), '{,', '{', 'g'), ',}','}', 'g')::text[],"
                + "regexp_replace(regexp_replace(regexp_replace('{'||array_to_string(histogram_bounds::anyarray, ',')||'}', '(,+|,*(, )+,*)', ',', 'g'), '{,', '{', 'g'), ',}','}', 'g')::text[],"
                + "most_common_freqs, correlation "
                + "FROM pg_statistic s JOIN pg_class c ON s.starelid=c.oid "
                + "JOIN pg_attribute a ON s.starelid=a.attrelid AND s.staattnum=a.attnum "
                + "JOIN pg_stats ps ON ps.attname=a.attname AND ps.tablename=c.relname "
                + "WHERE ps.schemaname = ? AND ps.tablename = ?",
            this.schemaname, this.relname);
    ResultSet pgStatisticRes = PostgresDBService
        .executeQueryParameterized(pgStatisticPs);

    pgStatistic = new ArrayList<PgStatisticData>();
    while (pgStatisticRes.next()) {
      PgStatisticData psd = new PgStatisticData();
      psd.attributename = pgStatisticRes.getString(1);
      psd.attributetype = pgStatisticRes.getString(2);
      psd.stainherit = pgStatisticRes.getBoolean(3);
      psd.stanullfrac = pgStatisticRes.getDouble(4);
      psd.stawidth = pgStatisticRes.getInt(5);
      psd.stadistinct = pgStatisticRes.getDouble(6);
      for (int i = 7; i <= 11; i++)
        psd.stakind[i - 7] = pgStatisticRes.getShort(i);
      for (int i = 12; i <= 16; i++)
        psd.staop[i - 12] = pgStatisticRes.getInt(i);
      psd.mostCommonValues = pgStatisticRes.getArray(17) == null ? null
          : (String[]) pgStatisticRes.getArray(17).getArray();
      psd.histogram = pgStatisticRes.getArray(18) == null ? null
          : (String[]) pgStatisticRes.getArray(18).getArray();
      Double[] freqValues = null;
      if (pgStatisticRes.getString(19) != null) {
        String[] freqArray = pgStatisticRes.getString(19)
            .replace("{", "").replace("}", "").replaceAll("\"", "").split(",");
        freqValues = new Double[freqArray.length];
        for (int i = 0; i < freqArray.length; i++) {
          log.debug("freqArray[{}]={}", i, freqArray[i]);
          BigDecimal bg = new BigDecimal(freqArray[i]);
          Formatter fmt = new Formatter();
          fmt.format("%." + bg.scale() + "f", bg);
          freqValues[i] = Double.valueOf(fmt.toString());
          fmt.close();
        }
      }
      psd.correlation = pgStatisticRes.getDouble(20);
      psd.mostCommonFreqs = freqValues;
      // filter out null values from the psd arrays
      // additionaly, trim all the strings due to automatic padding from PostgreSQL
      if (psd.histogram != null) {
        List<String> newHist = new ArrayList<String>();
        for (int i = 0; i < psd.histogram.length; i++) {
          if (psd.histogram[i] != null) {
            newHist.add(psd.histogram[i]);
          }
        }
        psd.histogram = newHist.toArray(new String[newHist.size()]);
      }
      if (psd.mostCommonValues != null && psd.mostCommonFreqs != null) {
        List<String> newMcv = new ArrayList<String>();
        List<Double> newMcf = new ArrayList<Double>();
        for (int i = 0; i < psd.mostCommonValues.length; i++) {
          if (psd.mostCommonValues[i] != null && psd.mostCommonFreqs[i] != null) {
            newMcv.add(psd.mostCommonValues[i]);
            newMcf.add(psd.mostCommonFreqs[i]);
          }
        }
        psd.mostCommonValues = newMcv.toArray(new String[newMcv.size()]);
        psd.mostCommonFreqs = newMcf.toArray(new Double[newMcf.size()]);
      }

      if (psd.mostCommonValues != null) {
        for (int i = 0; i < psd.mostCommonValues.length; i++) {
          if (psd.mostCommonValues[i].charAt(0) == '"'
              && psd.mostCommonValues[i].charAt(psd.mostCommonValues[i].length() - 1) == '"') {
            psd.mostCommonValues[i] = psd.mostCommonValues[i].substring(1, psd.mostCommonValues[i].length() - 1).trim();
          }
        }
      }
      if (psd.histogram != null) {
        for (int i = 0; i < psd.histogram.length; i++) {
          if (psd.histogram[i].charAt(0) == '"' && psd.histogram[i].charAt(psd.histogram[i].length() - 1) == '"') {
            psd.histogram[i] = psd.histogram[i].substring(1, psd.histogram[i].length() - 1).trim();
          }
        }
      }

      pgStatistic.add(psd);
    }
    pgClassPs.close();
    pgStatisticPs.close();
  }

  // This method is called only when predicates for a table generate
  // at least two partitions ==> it only refers to partitioned tables
  public void updateStatisticsForPartition(SimpleTable partition,
      Set<AtomicPredicate> predicatesForPartition, DBProperties dbProperties)
      throws ClassNotFoundException, SQLException, IllegalAccessException, IllegalArgumentException,
      InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
    if (!partition.tablename.substring(0, partition.tablename.indexOf('_'))
        .equals(relname)) {
      return;
    }
    Map<String, Set<AtomicPredicate>> predicatesByAttribute = new HashMap<>();
    for (AtomicPredicate p : predicatesForPartition) {
      if (!predicatesByAttribute.containsKey(p.attribute)) {
        Set<AtomicPredicate> set = new HashSet<>();
        set.add(p);
        predicatesByAttribute.put(p.attribute, set);
      } else {
        predicatesByAttribute.get(p.attribute).add(p);
      }
    }

    // Calculate countDistinct and countAll for the set of predicates
    // But, first convert each predicate to refer to the non-partitioned schema tables
    List<AtomicPredicate> predicatesForPartitionList = new ArrayList<AtomicPredicate>();
    for (AtomicPredicate p : predicatesForPartition) {
      StringBuffer sb = new StringBuffer(p.table);
      AtomicPredicate pOriginal =
          new AtomicPredicate(Constants.NONPARTITIONED_SCHEMA, sb.substring(0, sb.lastIndexOf("_")), p.attribute,
              p.attributeType, p.operator, p.value);
      predicatesForPartitionList.add(pOriginal);
    }

    BitmapIndexHolder bitmapIndexes = BitmapIndexHolder.getInstance();
    TreeMap<String, Integer> attributesDistinctValues =
        (TreeMap<String, Integer>) bitmapIndexes.getCountDistinct(predicatesForPartitionList);
    int partitionCardinality = bitmapIndexes.selectCountAll(predicatesForPartitionList);


		/*System.out.println("Partition: " + partition + ". Cardinality " + partitionCardinality);
    System.out.println("Predicates for partition: " + predicatesForPartitionList);
		System.out.println("CountDistinct for all attributes: " + attributesDistinctValues);
		*/
    // Pg class update calculation for this partition
    /*double predicateSetSelectivity = calculateTotalSelectivity(predicatesForPartition);
    long reltuples_partition = (long)(predicateSetSelectivity * this.pgClass.reltuples);
		int total_avg_attr_width = 0;
		*/
    //int totalAvarageAttributeWidth = 0;
    //for(PgStatisticData p_iter : this.pgStatistic)
    //	totalAvarageAttributeWidth += p_iter.stawidth;
    /*long relpages_partition = (long)Math.ceil(reltuples_partition / Math.floor(8168.0 / (total_avg_attr_width + 32.0)));
    if(predicateSetSelectivity == 1.0 / pgClass.reltuples){
			reltuples_partition = 1;
			relpages_partition = 1;
		}
		*/

    //relpages_partition = (long)Math.ceil(partitionCardinality / Math.floor(8168.0 / (totalAverageAttributeWidth + 32.0)));
    int partitionDiskPages = calculatePerfectlyAccurateRelpages(partition, partitionCardinality, dbProperties);

    updatePgClass(partition, partitionCardinality, partitionDiskPages, dbProperties);

    // update attribute statistics for which a predicate does not exist
    for (PgStatisticData psd : this.pgStatistic) {
      PgStatisticData newPsd = new PgStatisticData();
      //long start = System.currentTimeMillis();
      // this is also very fast - it uses the index on psd.attributenametable
      /*String sqlForAttr = "SELECT " + psd.attributename + ", COUNT(*)" + " FROM " + this.relname
          + " WHERE " + AtomicPredicateUtils.generatePredicateConjunction(predicatesForPartition)
					+ " GROUP BY " + psd.attributename
					+ " ORDER BY " + psd.attributename;

			*//** OD VAKVIOT PRASALNIK MOZE VSUSNOST DA SE IZGRADI KD TREE **//*
      Connection conn = PostgresDBService.getConnection("postgres", "postgres", "postgres");
			PreparedStatement pstmt = conn.prepareStatement(sqlForAttr, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = pstmt.executeQuery();
			*/

      Map<String, Double> sortedMcv = null;
      String val;
      int cnt_new;
      if (psd.mostCommonValues != null) {
        sortedMcv = new TreeMap<String, Double>();
        for (int i = 0; i < psd.mostCommonValues.length; i++) {
          sortedMcv.put(psd.mostCommonValues[i], psd.mostCommonFreqs[i]);
        }
      }

      //if (!predicatesByAttribute.containsKey(psd.attributename)) {

      // Attribute statistics are not merely copied to the destination
      // partition. Instead, they also have to be re-calculated to
      // satisfy the predicates used during the process.

      newPsd = new PgStatisticData(psd);
      List<String> newHistogram = new ArrayList<>();
      List<String> newMostCommonValues = new ArrayList<>();
      List<Double> newMostCommonFrequencies = new ArrayList<>();
      // in order to avoid hash collisions, a tree map is used to store (mcv,mcf) pairs

      SimpleTable table;
      if (this.schemaname == null) {
        table = new SimpleTable(Constants.NONPARTITIONED_SCHEMA, this.relname);
      } else {
        table = new SimpleTable(this.schemaname, this.relname);
      }

      List<Pair<String, Integer>> distinctValueOccurrences = histograms.getOccurences(
          table,
          psd.attributename,
          predicatesForPartitionList);

      for (Pair<String, Integer> pair : distinctValueOccurrences) {
        val = pair.key;
        cnt_new = pair.value;

        // if the histogram for this attribute exists
        if (psd.histogram != null) {
          // the histogram is pre-sorted, so we can perform a binary search
          int index = Arrays.binarySearch(psd.histogram, val);
          if (index != -1) {
            newHistogram.add(val);
          }
        }

        if (psd.mostCommonValues != null) {
          if (sortedMcv.containsKey(val)) {
            newMostCommonValues.add(val);
            double freqNew = (double) cnt_new / (double) partitionCardinality;
            newMostCommonFrequencies.add(freqNew);
          }
        }
      }

				/*while(rs.next()) {
          val = rs.getString(1);
					val = val.trim();
					cnt_new = Long.valueOf(rs.getString(2));

					// if the histogram for this attribute exists
					int index = -1;
					if(psd.histogram != null) {
						// the histogram is pre-sorted, so we can perform a binary search for "val"
						index = Arrays.binarySearch(psd.histogram, val);
					}

					if(index >= 0) {
						newHistogram.add(val);
					}

					if(psd.mostCommonValues != null){
						// "val" should exist in mcv
						if(sortedMcv.containsKey(val)) {
							newMostCommonValues.add(val);
							double freqNew = (double) cnt_new / (double) partitionCardinality;
							newMostCommonFrequencies.add(freqNew);
							*//**
       * DALI TREBA f_new = (total_selectivity * f_old * N_old) / N_new;
       * Ova e ako pretpostavime uniformna raspredelba na podatocite vo sekoja kolona
       *//*
            }
					}
				}
				pstmt.close();
				rs.close();*/

      //long end = System.currentTimeMillis();

      if (psd.histogram != null) {
        newPsd.histogram = newHistogram.toArray(new String[newHistogram.size()]);
      }
      if (psd.mostCommonValues != null) {
        newPsd.mostCommonValues = newMostCommonValues.toArray(new String[newMostCommonValues.size()]);
        newPsd.mostCommonFreqs = newMostCommonFrequencies.toArray(new Double[newMostCommonFrequencies.size()]);
      }

      long psdAttributeCountDistinct = attributesDistinctValues.get(psd.attributename);
      newPsd = calculateNewStadistinct(psd, newPsd, psdAttributeCountDistinct, partitionCardinality);

      //}
      /*else {

				newPsd = new PgStatisticData(psd);
				newPsd = calculateNewHistogram(psd, newPsd,
						predicatesByAttribute.get(psd.attributename));
				newPsd = calculateNewMostCommonValsFreqs(psd, newPsd,
						predicatesByAttribute.get(psd.attributename));
				newPsd = calculateNewStadistinct(psd, newPsd, predicatesForPartition, reltuples_local);

				if(newPsd.mostCommonValues != null) {
					Arrays.sort(newPsd.mostCommonValues);
					while(rs.next()) {
						val = rs.getString(1);
						val = val.trim();
						cnt_new = Long.valueOf(rs.getString(2));
						int index = Arrays.binarySearch(newPsd.mostCommonValues, val);
						if(index >= 0) {
							newPsd.mostCommonFreqs[index] = (double) cnt_new / (double) reltuples_local; // reltuples_partition
						}
					}
					pstmt.close();
					rs.close();
				}
			}*/
      updateBasicPgStatisticData(partition, newPsd, dbProperties);
      updatePgStatisticArrays(partition, newPsd, dbProperties);
    }
  }

  private void updateBasicPgStatisticData(SimpleTable dest,
      PgStatisticData psd, DBProperties properties) throws SQLException, ClassNotFoundException {

    String sql = String.format("UPDATE pg_statistic SET "
            + "stanullfrac = '%s'," + "stadistinct = '%s',"
            + "stawidth = %d,"
            + (psd.stakind[0] != 0 ? ("stakind1 = " + psd.stakind[0] + ",") : "")
            + (psd.stakind[1] != 0 ? ("stakind2 = " + psd.stakind[1] + ",") : "")
            + (psd.stakind[2] != 0 ? ("stakind3 = " + psd.stakind[2] + ",") : "")
            + (psd.stakind[3] != 0 ? ("stakind4 = " + psd.stakind[3] + ",") : "")
            + (psd.stakind[4] != 0 ? ("stakind5 = " + psd.stakind[4] + ",") : "")
            + "staop1 = %d," + "staop2 = %d,"
            + "staop3 = %d," + "staop4 = %d," + "staop5 = %d "
            + "WHERE "
            + "starelid = (SELECT '%s.%s'::regclass::oid) AND "
            + "staattnum = (SELECT attnum FROM pg_attribute WHERE "
            + "attrelid = (SELECT '%s.%s'::regclass::oid) AND "
            + "				 attname = '%s');",
        String.valueOf(psd.stanullfrac),
        String.valueOf(psd.stadistinct),
        psd.stawidth,
        psd.staop[0],
        psd.staop[1],
        psd.staop[2],
        psd.staop[3],
        psd.staop[4],
        dest.schemaname,
        dest.tablename,
        dest.schemaname,
        dest.tablename,
        psd.attributename);

    PostgresDBService.executeUpdate(sql, properties);
  }

  public void copyStatisticsForPseudoPartition(SimpleTable pseudopartition, DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {
    for (PgStatisticData psd : this.pgStatistic) {
      PgStatisticData psdNew = new PgStatisticData(psd);
      updateBasicPgStatisticData(pseudopartition, psdNew, dbProperties);
      updatePgStatisticArrays(pseudopartition, psdNew, dbProperties);
      updatePgClass(pseudopartition, this.pgClass.reltuples, this.pgClass.relpages, dbProperties);
    }
  }

  public void copyStatisticsForMasterTable(SimpleTable master, DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {
    for (PgStatisticData psd : this.pgStatistic) {
      PgStatisticData psdNew = new PgStatisticData(psd);
      updateBasicPgStatisticData(master, psdNew, dbProperties);
      updatePgStatisticArrays(master, psdNew, dbProperties);
    }
  }

  public String getActualAttributeType(String attributeName, DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {
    String attributeType = null;
    String getAttributeTypeSQL = "SELECT DISTINCT format_type(a.atttypid, a.atttypmod) "
        + "FROM pg_attribute a "
        + "WHERE a.attname = '" + attributeName + "'";
    ResultSet rs = PostgresDBService.executeQuery(getAttributeTypeSQL, dbProperties);
    rs.next();
    attributeType = rs.getString(1);
    rs.close();
    return attributeType;
  }

  // histogram_bounds e sekogas anyarray(stavalues) i stakind=2
  private void updatePgStatisticArrays(SimpleTable dest, PgStatisticData psd, DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {

    String actualAttributeTypeOfPsd = getActualAttributeType(psd.attributename, dbProperties);

    int correlationIdx = -1;
    for (int i = 0; i < psd.stakind.length; i++) {
      if (psd.stakind[i] == 3) {
        correlationIdx = i + 1;
        break;
      }
    }
    if (correlationIdx != -1) {
      String correlationColumn = "stanumbers" + correlationIdx;
      if (!String.format("'{%s}'::real[]", psd.correlation).equals("'{}'::real[]")) {
        PostgresDBService.executeUpdate("UPDATE pg_statistic SET "
            + correlationColumn
            + "="
            + String.format("'{%s}'::real[]", psd.correlation)
            + String.format(
            " WHERE "
                + "starelid = (SELECT '%s.%s'::regclass::oid) AND "
                + "staattnum = (SELECT attnum FROM pg_attribute WHERE "
                + "attrelid = (SELECT '%s.%s'::regclass::oid) "
                + "AND attname = '%s')",
            dest.schemaname,
            dest.tablename,
            dest.schemaname,
            dest.tablename,
            psd.attributename), dbProperties);
      }
    }

    int histIdx = -1, mcvIdx = -1, mcfIdx = -1;
    boolean existMcvMcf = false;
    for (int i = 0; i < psd.stakind.length; i++) {
      if (psd.stakind[i] == 2) {
        histIdx = i + 1;
        break;
      }
    }
    if (histIdx != -1) {

      String histColumn = "stavalues" + histIdx;
      String histStr = arrayToString(psd.histogram, ",");
      if (!histStr.equals("null")) {
        //histStr = histStr.substring(1, histStr.length() - 1); // remove []
        histStr = "array_in(anyarray_out('{" + histStr + "}'::"
            + actualAttributeTypeOfPsd + "[]), '" + psd.attributetype
            + "'::regtype,1)";
      }
      try {
        PostgresDBService
            .executeUpdate("UPDATE pg_statistic SET "
                + histColumn
                + "="
                + histStr
                + String.format(
                " WHERE "
                    + "starelid = (SELECT '%s.%s'::regclass::oid) AND "
                    + "staattnum = (SELECT attnum FROM pg_attribute WHERE "
                    + "attrelid = (SELECT '%s.%s'::regclass::oid) "
                    + "AND attname = '%s')",
                dest.schemaname,
                dest.tablename,
                dest.schemaname,
                dest.tablename,
                psd.attributename), dbProperties);
      } catch (PSQLException e) {
        System.out.println(psd.attributename);
        System.out.println(histColumn);
        System.out.println(histStr);
      }
    }

    for (int i = 0; i < psd.stakind.length; i++) {
      if (psd.stakind[i] == 1 || psd.stakind[i] == 4) {
        mcvIdx = i + 1;
        existMcvMcf = true;
        break;
      }
    }
    if (!existMcvMcf) {
      mcvIdx = -1;
    }
    if (mcvIdx != -1) {

      String mcv = arrayToString(psd.mostCommonValues, ",");

      if (!mcv.equals("null")) {
        //mcv = mcv.substring(1, mcv.length() - 1); // remove []
        mcv = "array_in(anyarray_out('{" + mcv + "}'::"
            + actualAttributeTypeOfPsd + "[]), '" + psd.attributetype
            + "'::regtype,1)";
      }
      String mcvColumn = "stavalues" + mcvIdx;
      try {
        PostgresDBService
            .executeUpdate("UPDATE pg_statistic SET "
                + mcvColumn
                + "="
                + mcv
                + String.format(
                " WHERE "
                    + "starelid = (SELECT '%s.%s'::regclass::oid) AND "
                    + "staattnum = (SELECT attnum FROM pg_attribute WHERE "
                    + "attrelid = (SELECT '%s.%s'::regclass::oid) AND "
                    + "attname = '%s')",
                dest.schemaname,
                dest.tablename,
                dest.schemaname,
                dest.tablename,
                psd.attributename), dbProperties);
      } catch (PSQLException e) {
        System.out.println(mcv);
        System.out.println(psd.mostCommonValues.length);
      }
      existMcvMcf = false;
      for (int i = 0; i < psd.stakind.length; i++) {
        if (psd.stakind[i] == 1 || psd.stakind[i] == 4) {
          mcfIdx = i + 1;
          existMcvMcf = true;
          break;
        }
      }
      if (!existMcvMcf) {
        mcfIdx = -1;
      }
      if (mcfIdx != -1) {

        String mcvFreq = Arrays.toString(psd.mostCommonFreqs);
        if (!mcvFreq.equals("null")) {
          mcvFreq = "'{" + mcvFreq.substring(1, mcvFreq.length() - 1)
              + "}'::real[]";
        }
        String mcvFreqColumn = "stanumbers" + mcfIdx;
        if (!mcvFreq.equals("'{}'::real[]")) {
          PostgresDBService
              .executeUpdate("UPDATE pg_statistic SET "
                  + mcvFreqColumn
                  + "="
                  + mcvFreq
                  + String.format(
                  " WHERE "
                      + "starelid = (SELECT '%s.%s'::regclass::oid) AND "
                      + "staattnum = (SELECT attnum FROM pg_attribute WHERE "
                      + "attrelid = (SELECT '%s.%s'::regclass::oid) AND "
                      + "attname = '%s')",
                  dest.schemaname,
                  dest.tablename,
                  dest.schemaname,
                  dest.tablename,
                  psd.attributename), dbProperties);
        }
      }
    }
  }

  private void updatePgClass(SimpleTable dest, long reltuples, long relpages, DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {
    ResultSet resFn = PostgresDBService.executeQuery(String.format(
        "SELECT relfilenode FROM pg_class WHERE oid=(SELECT '%s.%s'::regclass::oid);",
        dest.schemaname,
        dest.tablename), dbProperties);
    resFn.next();
    int fileNode = resFn.getInt(1);
    String fileNodePath = Constants.DB_DIR + String.valueOf(fileNode);
    createFalseFileNode(fileNodePath, relpages);
    String sql =
        String.format("UPDATE pg_class SET reltuples=%d, relpages=%d "
                + "WHERE oid=(SELECT '%s.%s'::regclass::oid);",
            reltuples,
            relpages,
            dest.schemaname,
            dest.tablename);
    PostgresDBService.executeUpdate(sql, dbProperties);
  }

  @SuppressWarnings("unused")
  private PgStatisticData calculateNewHistogram(PgStatisticData psd,
      PgStatisticData dest, Set<AtomicPredicate> predicatesForAttr)
      throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      NoSuchMethodException, SecurityException {
    if (predicatesForAttr.isEmpty() || Arrays.toString(psd.histogram).equals("null")) {
      return dest;
    }
    List<String> newHist = new ArrayList<>();
    for (int i = 0; i < psd.histogram.length; i++) {
      if (areSatisfied(psd.histogram[i], predicatesForAttr)) {
        newHist.add(psd.histogram[i]);
      }
    }
    dest.histogram = Arrays.copyOf(newHist.toArray(), newHist.size(), String[].class);
    return dest;
  }

  @SuppressWarnings("unused")
  private PgStatisticData calculateNewMostCommonValsFreqs(
      PgStatisticData psd, PgStatisticData dest,
      Set<AtomicPredicate> predicatesForAttr)
      throws SQLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      NoSuchMethodException, SecurityException {
    if (predicatesForAttr.isEmpty() || Arrays.toString(psd.mostCommonValues).equals("null")) {
      return dest;
    }
    List<String> newMcv = new ArrayList<>();
    List<Double> newMcf = new ArrayList<>();
    for (int i = 0; i < psd.mostCommonValues.length; i++) {
      if (areSatisfied(psd.mostCommonValues[i], predicatesForAttr)) {
        newMcv.add(psd.mostCommonValues[i]);
        newMcf.add(psd.mostCommonFreqs[i]);
      }
    }
    dest.mostCommonValues = Arrays.copyOf(newMcv.toArray(), newMcv.size(), String[].class);
    dest.mostCommonFreqs = Arrays.copyOf(newMcf.toArray(), newMcf.size(), Double[].class);
    return dest;
  }

	/*public PgStatisticData calculateNewStadistinct(PgStatisticData psd, PgStatisticData dest, Set<AtomicPredicate> predicatesForPartition, long tuplesInPartition) throws SQLException, ClassNotFoundException {
    double dest_stadistinct = 0.0;
		
		//String actualAttributeType = getActualAttributeType(psd.attributename);
				
		// a very fast recursive implementation of a select distinct query
		//String stadistinctSQL;
		//if(!Constants.NAIVE_COUNT_DISTINCT_ATTRS.contains(psd.attributename.toLowerCase())) {
		//	stadistinctSQL = getSelectDistinctRecursive(this.schemaname, this.relname, psd.attributename, actualAttributeType, predicatesForPartition);
		//} else {
		//	stadistinctSQL = getSelectDistinctNaive(this.schemaname, this.relname, psd.attributename, actualAttributeType, predicatesForPartition);
		//}
		//ResultSet rs = PostgresDBService.executeQuery(stadistinctSQL);
		//rs.next();
		//dest_stadistinct = rs.getDouble(1);
		//rs.close();
		
		if (psd.stadistinct < 0) {
			// we use the precalculated tuplesInPartition here !!!
			if(tuplesInPartition > 0)
				dest_stadistinct = - (dest_stadistinct / (double) tuplesInPartition);	
			else 
				dest_stadistinct = 0.0;
		}
		else if (psd.stadistinct > 0) {			
			// do nothing, stadistinct is already calculated above
		}
		else {
			dest_stadistinct = psd.stadistinct;
		}
		dest.stadistinct = dest_stadistinct;
		return dest;
	}*/

  public PgStatisticData calculateNewStadistinct(PgStatisticData psd, PgStatisticData dest,
      long psdAttributeCountDistinct, long predicatesCountAll) throws SQLException, ClassNotFoundException {
    double dest_stadistinct = 0.0;
    //String predicateConjunction = "";
    //if (predicatesForPartition != null) {
    //	StringBuilder sb = new StringBuilder();
    //	for(AtomicPredicate p : predicatesForPartition) {
    //		sb.append(p.toTextPredicateRaw());
    //		sb.append(" AND ");
    //	}
    //	predicateConjunction = sb.toString();
    //	predicateConjunction = predicateConjunction.substring(0, predicateConjunction.length() - 5);
    //}
    if (psd.stadistinct < 0) {
      if (predicatesCountAll == 0) {
        dest_stadistinct = 0.0;
      } else {
        dest_stadistinct = -(double) psdAttributeCountDistinct / (double) predicatesCountAll;
      }
      //String stadisticntSQL = "SELECT (CASE WHEN COUNT(*) = 0 THEN 0 ELSE (COUNT(DISTINCT " + psd.attributename + ")::real / COUNT(*)::real) END)"
      //						+ " FROM "
      //						+ String.format("%s.%s", this.schemaname, this.relname)
      //						+ " WHERE " + predicateConjunction;
      //ResultSet rs = PostgresDBService.executeQuery(stadisticntSQL);
      //rs.next();
      //dest_stadistinct = rs.getDouble(1);
      //dest_stadistinct = -dest_stadistinct;
      //rs.close();
    } else if (psd.stadistinct > 0) {
      dest_stadistinct = psdAttributeCountDistinct;
      // a very fast recursive implementation of a select distinct query
      //String stadistinctSQL = getSelectDistinctRecursive(this.schemaname, this.relname, psd.attributename, getActualAttributeType(psd.attributename), predicatesForPartition);
      //ResultSet rs = PostgresDBService.executeQuery(stadistinctSQL);
      //rs.next();
      //dest_stadistinct = rs.getDouble(1);
      //rs.close();
    } else {
      dest_stadistinct = psd.stadistinct;
    }
    dest.stadistinct = dest_stadistinct;
    return dest;
  }

  private boolean areSatisfied(String value, Set<AtomicPredicate> p)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException,
      SecurityException {
    for (AtomicPredicate ap : p) {
      Object newVal;
      if (!(ap.value instanceof String)) {
        newVal = ap.value.getClass().getMethod("valueOf", String.class).invoke(null, value);  // static invoke
      } else {
        newVal = value;
      }
      if (ap.operator == Operator.EQ && compare(newVal, ap.value) != 0) return false;
      if (ap.operator == Operator.NEQ && compare(newVal, ap.value) == 0) return false;
      if (ap.operator == Operator.LT && compare(newVal, ap.value) >= 0) return false;
      if (ap.operator == Operator.GT && compare(newVal, ap.value) <= 0) return false;
      if (ap.operator == Operator.LTE && compare(newVal, ap.value) > 0) return false;
      if (ap.operator == Operator.GTE && compare(newVal, ap.value) < 0) return false;
    }
    return true;
  }

  @SuppressWarnings("unused")
  private Double calculateTotalSelectivity(Set<AtomicPredicate> predicates)
      throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      NoSuchMethodException, SecurityException {
    if (this.pgStatistic.isEmpty()) {
      return null;
    }

    double totalSelectivity = 1.0;

    predicate_loop:
    for (AtomicPredicate p : predicates) {
      PgStatisticData attrStatistics = null;
      for (PgStatisticData psd : this.pgStatistic) {
        if (psd.attributename.equals(p.attribute)) {
          attrStatistics = psd;
          break;
        }
      }
      if (contains(attrStatistics.attributetype, DataType.NUMERIC)) {

        if (p.operator == Operator.EQ) {
          if (!Arrays.toString(attrStatistics.mostCommonValues).equals("null")) {
            boolean mcvContains = false;
            int valPos = -1;
            for (int i = 0; i < attrStatistics.mostCommonValues.length; i++) {
              Object valRhs =
                  p.value.getClass().getConstructor(String.class).newInstance(attrStatistics.mostCommonValues[i]);

              if (compare(p.value, valRhs) == 0) {
                mcvContains = true;
                valPos = i;
                break;
              }
            }
            if (mcvContains) {
              totalSelectivity *= attrStatistics.mostCommonFreqs[valPos];
            } else {
              double sumfreq = 0.0;
              double/*long*/ distinct;
              for (int i = 0; i < attrStatistics.mostCommonFreqs.length; i++)
                sumfreq += attrStatistics.mostCommonFreqs[i];
              if (attrStatistics.stadistinct < 0.0) {
                distinct = /*(long)*/ (pgClass.reltuples * Math
                    .abs(attrStatistics.stadistinct));
              } else {
                distinct = /*(long)*/ attrStatistics.stadistinct;
              }
              totalSelectivity *= ((1 - sumfreq)
                  / (double) (distinct - attrStatistics.mostCommonValues.length));
            }
          } else { // column is unique
            totalSelectivity *= (1.0 / pgClass.reltuples);
          }
        } else if (p.operator == Operator.LT
            || p.operator == Operator.GT
            || p.operator == Operator.LTE
            || p.operator == Operator.GTE) {

          double histogramSelectivity = 0.0;
          double sumRelevantFreqs = 0.0;
          double totalHISTFraction = 0.0;
          double totalMCVFraction = 0.0;  // <=> sumcommon od ineqsel() od source code na PostgreSQL
          if (!Arrays.toString(attrStatistics.histogram).equals(
              "null")) {
            int bucketPos = -1;
            double bucketFrac = 0.0;
            int bucketCount = attrStatistics.histogram.length - 1;
            for (int i = 0; i < attrStatistics.histogram.length - 1; i++) {
              Object histVal = p.value.getClass()
                  .getDeclaredMethod("valueOf", String.class)
                  .invoke(null, attrStatistics.histogram[i]);
              Object histValNext = p.value.getClass()
                  .getDeclaredMethod("valueOf", String.class)
                  .invoke(null, attrStatistics.histogram[i + 1]);
              if (compare(p.value, histVal) >= 0 && compare(p.value, histValNext) < 0) {
                bucketPos = i;
                bucketFrac =
                    (double) (p.value.getClass().getDeclaredMethod("doubleValue").invoke(minus(p.value, histVal))) /
                        (double) (p.value.getClass()
                            .getDeclaredMethod("doubleValue")
                            .invoke(minus(histValNext, histVal)));
                break;
              }
            }

            if (bucketPos != -1) {
              if (p.operator == Operator.LT || p.operator == Operator.LTE) {
                histogramSelectivity = ((bucketFrac + bucketPos)
                    / (double) bucketCount);
              } else {
                // promeneto kako vo source code na postgresql (ineq_histogram_selectivity pri krajot na kodot)
                histogramSelectivity = 1.0 - ((bucketFrac + bucketPos) / (double) bucketCount);
                //histogramSelectivity = (((1.0 - bucketFrac) + (bucketCount - bucketPos))
                //		/ (double)bucketCount);
              }
            }
          }
          if (!Arrays.toString(attrStatistics.mostCommonValues).equals("null")) {

            double sumFreq = 0.0;

            for (int i = 0; i < attrStatistics.mostCommonValues.length; i++) {
              Object valRhs = p.value.getClass()
                  .getDeclaredMethod("valueOf", String.class)
                  .invoke(null, attrStatistics.mostCommonValues[i]);
              totalMCVFraction += attrStatistics.mostCommonFreqs[i];
              if (p.operator == Operator.LT) {
                if (compare(valRhs, p.value) < 0 /*valRhs < val*/) {
                  sumFreq += attrStatistics.mostCommonFreqs[i];
                }
              } else if (p.operator == Operator.GT) {
                if (compare(valRhs, p.value) > 0/*valRhs > val*/) {
                  sumFreq += attrStatistics.mostCommonFreqs[i];
                }
              } else if (p.operator == Operator.LTE) {
                if (compare(valRhs, p.value) <= 0/*valRhs > val*/) {
                  sumFreq += attrStatistics.mostCommonFreqs[i];
                }
              } else if (p.operator == Operator.GTE) {
                if (compare(valRhs, p.value) >= 0/*valRhs > val*/) {
                  sumFreq += attrStatistics.mostCommonFreqs[i];
                }
              }
            }
            sumRelevantFreqs = sumFreq;
          }

          // if totalMCVFraction of the values are covered with MCV, then the others are covered with the Histogram (when the Histogram exists)
          totalHISTFraction = 1.0 - totalMCVFraction;

          // Bind the histogram selectivity to the default boundary values 0.0001 and 0.9999 (ineq_hist_selectivity)
          if (histogramSelectivity < 0.0001) {
            histogramSelectivity = 0.0001;
          }
          if (histogramSelectivity > 0.9999) {
            histogramSelectivity = 0.9999;
          }

          // Simple code added to match the code in ineqsel() from the PostgreSQL source code
          double mcv_selec, hist_selec, selec;
          mcv_selec = sumRelevantFreqs;
          hist_selec = histogramSelectivity;
          selec = 1.0 - totalMCVFraction;  // totalMCVFraction <=> sumcommon od ineqsel()
          if (hist_selec >= 0.0) {
            selec *= hist_selec;
          } else {
            // If no histogram but there are values not accounted for by MCV arbitrarily assume half of them will match.
            selec *= 0.5;
          }
          selec += mcv_selec;
          totalSelectivity *= selec;

          //totalSelectivity *= (sumRelevantFreqs + histogramSelectivity * totalHISTFraction);
        } else {
          // if the operator is NEQ, then invert the predicate, calculate its selectivity and subtract it from 1.0;
          AtomicPredicate pInv = AtomicPredicate.inverse(p);

          if (!Arrays.toString(attrStatistics.mostCommonValues)
              .equals("null")) {
            boolean mcvContains = false;
            int valPos = -1;
            for (int i = 0; i < attrStatistics.mostCommonValues.length; i++) {
              Object valRhs = pInv.value.getClass()
                  .getDeclaredMethod("valueOf", String.class)
                  .invoke(null, attrStatistics.mostCommonValues[i]);
              if (compare(pInv.value, valRhs) == 0/*val == valRhs*/) {
                mcvContains = true;
                valPos = i;
                break;
              }
            }
            if (mcvContains) {
              totalSelectivity *= (1.0 - attrStatistics.mostCommonFreqs[valPos]);
            } else {
              double sumfreq = 0.0;
              long distinct;
              for (int i = 0; i < attrStatistics.mostCommonFreqs.length; i++)
                sumfreq += attrStatistics.mostCommonFreqs[i];
              if (attrStatistics.stadistinct < 0.0) {
                distinct = (long) (pgClass.reltuples * Math
                    .abs(attrStatistics.stadistinct));
              } else {
                distinct = (long) attrStatistics.stadistinct;
              }
              totalSelectivity *= (1.0 - (1 - sumfreq)
                  / (distinct - attrStatistics.mostCommonValues.length));
            }
          } else { // column is unique
            totalSelectivity *= (1.0 - 1.0 / pgClass.reltuples);
          }
        }
      } else if (contains(attrStatistics.attributetype, DataType.TEXT)) {
        if (p.operator == Operator.EQ || p.operator == Operator.NEQ) {
          AtomicPredicate pp;
          //ne e potrebno pp
          if (p.operator == Operator.EQ) {
            pp = p;
          } else {
            pp = AtomicPredicate.inverse(p);
          }
          if (!Arrays.toString(attrStatistics.mostCommonValues).equals("null")) {
            String val = (String) pp.value;
            double sumFreqs = 0.0;
            for (int i = 0; i < attrStatistics.mostCommonValues.length; i++) {
              sumFreqs += attrStatistics.mostCommonFreqs[i];
              String mcvi = attrStatistics.mostCommonValues[i];
              if (mcvi.charAt(0) == '"' && mcvi.charAt(mcvi.length() - 1) == '"') {
                mcvi = mcvi.substring(0, mcvi.length() - 1);
              }
              if (mcvi.equals(val)) {
                if (p.operator == Operator.EQ) {
                  totalSelectivity *= attrStatistics.mostCommonFreqs[i];
                } else {
                  totalSelectivity *= (1.0 - attrStatistics.mostCommonFreqs[i]);
                }
                // selectivity for this predicate is mcf[i]
                continue predicate_loop;
              }
            }
            //System.out.println("SELECTIVITY FOR " + predicates + " = " + totalSelectivity);
            long distinct;
            if (attrStatistics.stadistinct < 0.0) {
              distinct = (long) (Math.abs(attrStatistics.stadistinct) * pgClass.reltuples);
            } else {
              distinct = (long) attrStatistics.stadistinct;
            }
            double res = (1.0 - sumFreqs) / (distinct - attrStatistics.mostCommonValues.length);
            //System.out.println("DISTINCT - MCVLENGTH = " + (distinct - attrStatistics.mostCommonValues.length));
            if (p.operator == Operator.EQ) {
              totalSelectivity *= res;
            } else {
              totalSelectivity *= (1.0 - res);
            }
            //System.out.println("SELECTIVITY FOR " + predicates + " = " + totalSelectivity);
          } else {
            if (p.operator == Operator.EQ) {
              totalSelectivity *= (1.0 / pgClass.reltuples);
            } else {
              totalSelectivity *= (1.0 - 1.0 / pgClass.reltuples);
            }
          }
          //System.out.println("SELECTIVITY FOR " + predicates + " = " + totalSelectivity);
        } else {
          double sumRelevantFreqs = 0.0;
          double sumAllFreqs = 0.0;
          double Hs = 0.0, Hf = 0.0;
          String val = (String) p.value;
          //System.out.println("SELECTIVITY FOR " + predicates + " = " + totalSelectivity);
          if (!Arrays.toString(attrStatistics.mostCommonValues).equals("null")) {
            for (int i = 0; i < attrStatistics.mostCommonValues.length; i++) {
              sumAllFreqs += attrStatistics.mostCommonFreqs[i];
              String mcvi = attrStatistics.mostCommonValues[i];
              if (mcvi.charAt(0) == '"' && mcvi.charAt(mcvi.length() - 1) == '"') {
                mcvi = mcvi.substring(0, mcvi.length() - 1);
              }
              if (p.operator == Operator.LT) {
                if (mcvi.compareTo(val) < 0) {
                  sumRelevantFreqs += attrStatistics.mostCommonFreqs[i];
                }
              } else if (p.operator == Operator.GT) {
                if (mcvi.compareTo(val) > 0) {
                  sumRelevantFreqs += attrStatistics.mostCommonFreqs[i];
                }
              } else if (p.operator == Operator.LTE) {
                if (mcvi.compareTo(val) <= 0) {
                  sumRelevantFreqs += attrStatistics.mostCommonFreqs[i];
                }
              } else if (p.operator == Operator.GTE) {
                if (mcvi.compareTo(val) >= 0) {
                  sumRelevantFreqs += attrStatistics.mostCommonFreqs[i];
                }
              }
            }
            Hf = 1.0 - sumAllFreqs;    //other values not contained in mcv
          }
          //System.out.println("SELECTIVITY FOR " + predicates + " = " + totalSelectivity);
          if (!Arrays.toString(attrStatistics.histogram).equals("null")) {
            int l = 0, z = attrStatistics.histogram.length;
            for (int i = 0; i < attrStatistics.histogram.length; i++) {
              String histi = attrStatistics.histogram[i];
              if (histi.charAt(0) == '"' && histi.charAt(histi.length() - 1) == '"') {
                histi = histi.substring(0, histi.length() - 1);
              }
              if (p.operator == Operator.LT) {
                if (histi.compareTo(val) < 0) l++;
              } else if (p.operator == Operator.GT) {
                if (histi.compareTo(val) > 0) l++;
              } else if (p.operator == Operator.LTE) {
                if (histi.compareTo(val) <= 0) l++;
              } else if (p.operator == Operator.GTE) {
                if (histi.compareTo(val) >= 0) l++;
              }
            }
            Hs = Hf / (z * l);
          }
          totalSelectivity *= (sumRelevantFreqs + Hs * Hf);
        }
      }
    }
    return totalSelectivity;
  }

  private boolean contains(String key, String[] arr) {
    for (int i = 0; i < arr.length; i++)
      if (arr[i].equals(key)) {
        return true;
      }
    return false;
  }

  private Object minus(Object lhs, Object rhs) {
    if (lhs instanceof Double && rhs instanceof Double) {
      return ((Double) lhs) - ((Double) rhs);
    } else if (lhs instanceof Short && rhs instanceof Short) {
      return ((Short) lhs) - ((Short) rhs);
    } else if (lhs instanceof Integer && rhs instanceof Integer) {
      return ((Integer) lhs) - ((Integer) rhs);
    } else if (lhs instanceof Long && rhs instanceof Long) {
      return ((Long) lhs) - ((Long) rhs);
    }

    return null;
  }

  private void createFalseFileNode(String path, long blocks) {
    if (blocks == 0) return;
    int exitVal = 0;
    try {
      Runtime rt = Runtime.getRuntime();
      Process proc;
      String[] cmd = {"/bin/bash", "-c",
          "echo \"arlinada1!\" | sudo -S " + Constants.SH_FALSE_FILENODE + " " + path + " " + String.valueOf(blocks),
          "chown", "postgres:postgres", Constants.SH_FALSE_FILENODE};
      proc = rt.exec(cmd);

      // any error message?
      StreamGobbler errorGobbler = new StreamGobbler(
          proc.getErrorStream(), "ERROR");
      // any output?
      StreamGobbler outputGobbler = new StreamGobbler(
          proc.getInputStream(), "OUTPUT");
      // kick them off
      errorGobbler.start();
      outputGobbler.start();
      // any error???
      exitVal = proc.waitFor();
    } catch (Throwable t) {
      System.err.println("Exit value of ruby script process: " + exitVal);
      t.printStackTrace();
      return;
    }
  }

  private int calculatePerfectlyAccurateRelpages(SimpleTable partition, int partitionCardinality,
      DBProperties dbProperties)
      throws SQLException, ClassNotFoundException {
    if (this.schemaname == null || this.schemaname.isEmpty()) {
      this.schemaname = "public";
    }

    String relpagesSQL = String.format(
        "SELECT ceil(%d::real / (reltuples::real / relpages::real)::real)::int FROM pg_class WHERE oid='%s.%s'::regclass::oid;",
        partitionCardinality, this.schemaname, this.relname);
    ResultSet relpagesRs = PostgresDBService.executeQuery(relpagesSQL, dbProperties);
    relpagesRs.next();
    int relpages = relpagesRs.getInt(1);
    relpagesRs.close();
    return relpages;
  }

  private String arrayToString(String[] arr, String delimiter) {
    if (arr.length > 0) {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < arr.length - 1; i++) {
        if (arr[i].length() == 0) {
          continue;
        }
        if (!arr[i].contains(delimiter)) {
          sb.append(arr[i]);
        } else {
          sb.append("\"").append(arr[i]).append("\"");
        }
        sb.append(delimiter);
      }
      if (!arr[arr.length - 1].contains(delimiter)) {
        sb.append(arr[arr.length - 1]);
      } else {
        sb.append("\"").append(arr[arr.length - 1]).append("\"");
      }

      return sb.toString();
    }
    return "";
  }
	
	/*private String getSelectDistinctRecursive(String nonPartitionedSchema, String nonPartitionedTable, String attname, String actualAttributeType, Set<AtomicPredicate> predicatesForPartition) {
		String predicateConjunction = AtomicPredicateUtils.generatePredicateConjunction(predicatesForPartition);
		
		// aggregate functions such as MIN, MAX, ... do not operate over BIT data types and their derivatives ==> we have to cast them first
		String bitCast = null;
		if(actualAttributeType.toLowerCase().contains("bit")) {
			bitCast = actualAttributeType.toLowerCase().replace("bit", "::varchar");
		}
		
		String sql = String.format(
				"SELECT COUNT(*) FROM ( "
				+ "WITH RECURSIVE t(n) AS ( "
					+ "SELECT MIN(%s%s) FROM %s.%s WHERE %s "
					+ "UNION "
					+ "SELECT(SELECT %s%s FROM %s.%s WHERE %s%s > n " + (predicatesForPartition != null ? " AND " : "") + "%s ORDER BY %s LIMIT 1) "
					+ "FROM t WHERE n IS NOT NULL "
				+ ") "
				+ "SELECT n as n FROM t "
				+ ") x "
				+ "WHERE x.n IS NOT NULL;",
				attname,
				bitCast != null ? bitCast : "",
				nonPartitionedSchema, nonPartitionedTable,
				predicatesForPartition != null ? predicateConjunction : "",
				attname,
				bitCast != null ? bitCast : "",
				nonPartitionedSchema, nonPartitionedTable,
				attname,
				bitCast != null ? bitCast : "",
				predicatesForPartition != null ? predicateConjunction : "",
				attname);
		return sql;
	}*/
	
	/*public String getSelectDistinctNaive(String nonPartitionedSchema, String nonPartitionedTable, String attname, String actualAttributeType, Set<AtomicPredicate> predicatesForPartition) {
		String predicateConjunction = AtomicPredicateUtils.generatePredicateConjunction(predicatesForPartition);
		
		// aggregate functions such as MIN, MAX, ... do not operate over BIT data types and their derivatives ==> we have to cast them first
		String bitCast = null;
		if(actualAttributeType.toLowerCase().contains("bit")) {
			bitCast = actualAttributeType.toLowerCase().replace("bit", "::varchar");
		}
		
		String sql = String.format(
				"SELECT COUNT(DISTINCT %s%s) FROM %s.%s " 
				+(predicatesForPartition != null ? "WHERE %s" : ""),
				attname,
				bitCast != null ? bitCast : "",
				nonPartitionedSchema, nonPartitionedTable,
				predicatesForPartition != null ? predicateConjunction : "");
		return sql;
	}*/
}
