package org.partitioning;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.DBProperties;
import org.bitmapindex.ColumnarBitmapIndex;
import org.bitmapindex.ColumnarBitmapIndex.ColumnarBitmapIndexIterator;
import org.bitmapindex.ColumnarBitmapIndex.ColumnarIndexElement;
import org.multidimhistogram.Pair;
import org.roaringbitmap.RoaringBitmap;

public final class PredicateBitmaps {

  //final static Map<AtomicPredicate, RoaringBitmap> predicateBitmaps = new HashMap<AtomicPredicate, RoaringBitmap>();
  final static Map<AtomicPredicate, ColumnarBitmapIndex> bitmapIndexes =
      new HashMap<AtomicPredicate, ColumnarBitmapIndex>();

  // predicates: all predicates that were extracted from the workload
  public static void generatePredicateBitArrays(AtomicPredicate[] predicates, DBProperties dbProperties)
      throws ClassNotFoundException, SQLException {
    String sql = "select rownum - 1 from (select row_number() over(order by %s) as rownum, %s from %s) x where %s";

    for (AtomicPredicate p : predicates) {
      SimpleTable t = p.extractTable();
      AtomicPredicate pInverse = AtomicPredicate.inverse(p);
      ColumnarBitmapIndex index = new ColumnarBitmapIndex(t);
      ColumnarBitmapIndex indexInverse = new ColumnarBitmapIndex(t);
      RoaringBitmap r = new RoaringBitmap();
      RoaringBitmap rInverse = new RoaringBitmap();

      String query = String.format(sql, getPrimaryKeyAsString(t), p.attribute, t.toTextTable(), p.toTextPredicateRaw());
      ResultSet rs = PostgresDBService.executeQuery(query, dbProperties);
      while (rs.next()) {
        r.add(rs.getInt(1));
      }
      rs.close();

      query = String.format(sql, getPrimaryKeyAsString(t), pInverse.attribute, t.toTextTable(),
          pInverse.toTextPredicateRaw());
      ResultSet rsInverse = PostgresDBService.executeQuery(query, dbProperties);
      while (rsInverse.next()) {
        rInverse.add(rsInverse.getInt(1));
      }
      rsInverse.close();

      index.addElement(p.attribute, r);
      indexInverse.addElement(p.attribute, rInverse);

      bitmapIndexes.put(p, index);
      bitmapIndexes.put(pInverse, indexInverse);
    }

		/*for(AtomicPredicate p : predicates) {
       SimpleTable t = new SimpleTable("public", p.table);
			 if(p.schema != null) {
				 if(!p.schema.isEmpty())
					 t.schemaname = p.schema;
			 }
			 RoaringBitmap bitmap = new RoaringBitmap();
			 String sql = "select rownum - 1 from (select row_number() over(order by " + getPrimaryKeyAsString(t) + ") as rownum, " + p.attribute + " from " + t + ") x where " + p.toTextPredicateRaw();
			 ResultSet rs = PostgresDBService.executeQuery(sql);
			 int bitIndex;
			 while(rs.next()) {
				 bitIndex = Integer.valueOf(rs.getString(1));
				 bitmap.add(bitIndex);
			 }
			 rs.close();
			 predicateBitmaps.put(p, bitmap);
			 
			 // do the same for the inverted predicate
			 AtomicPredicate pInverse = AtomicPredicate.inverse(p);
			 RoaringBitmap bitmapInverse = new RoaringBitmap();
			 sql = "select rownum - 1 from (select row_number() over(order by " + getPrimaryKeyAsString(t) + ") as rownum, " + pInverse.attribute + " from " + t + ") x where " + pInverse.toTextPredicateRaw();
			 ResultSet rsInverse = PostgresDBService.executeQuery(sql);
			 while(rsInverse.next()) {
				 bitIndex = Integer.valueOf(rsInverse.getString(1));
				 bitmapInverse.add(bitIndex);
			 }
			 rs.close();
			 predicateBitmaps.put(pInverse, bitmapInverse);
		}*/

  }

  // predicates: predicates over the same table
  public static int selectCountAll(List<AtomicPredicate> predicates) {
    ColumnarBitmapIndex[] indexes = new ColumnarBitmapIndex[predicates.size()];
    int pos = 0;
    for (AtomicPredicate p : predicates)
      indexes[pos++] = bitmapIndexes.get(p);
    ColumnarBitmapIndex intersection = ColumnarBitmapIndex.and(indexes);
    ColumnarBitmapIndexIterator it = intersection.iterator();
    Pair<String, ColumnarIndexElement> pair = it.next();
    return pair.value.getCardinality();

		/*SimpleTable t = new SimpleTable("public", predicates.get(0).table);
		if(predicates.get(0).schema != null) {
			if(!predicates.get(0).schema.isEmpty())
				t.schemaname = predicates.get(0).schema;
		}
		if(predicates.size() == 1) {
			return predicateBitmaps.get(predicates.get(0)).getCardinality();
		}
		else {
			RoaringBitmap[] bitmaps = new RoaringBitmap[predicates.size()];
			int pos = 0;
			for(AtomicPredicate p : predicates) {
				bitmaps[pos++] = predicateBitmaps.get(p);
			}
			RoaringBitmap intersection = FastAggregation.and(bitmaps);
			return intersection.getCardinality();
		}*/
  }

  private static String getPrimaryKeyAsString(SimpleTable table) {
    return String.format(
        "(select array_to_string(array(select a.attname from pg_index i join pg_attribute a on a.attrelid = i.indrelid and a.attnum = any(i.indkey) "
            + "where i.indrelid = '%s'::regclass "
            + "and i.indisprimary), ','))",
        table.toTextTable());
  }
}
