package org.partitioning;

public final class Interval implements Comparable<Interval>{	
	
	Object a;
	Object b;
	
	boolean aClosed;
	boolean bClosed;
	
	public Interval() {
		
	}
	
	public Interval(Interval other) {
		this.a = other.a;
		this.b = other.b;
		this.aClosed = other.aClosed;
		this.bClosed = other.bClosed;
	}
	
	@Override
	public int compareTo(Interval o) {
		if(a.getClass().equals(o.a.getClass())) {	
			if(compare(a, o.a) == 0) {
				if(b.getClass().equals(o.b.getClass())) return compare(b, o.b);
				else return (b instanceof Double) ? 1 : -1;
			}
			else return compare(a, o.a);
		}
		else return (a instanceof Double) ? -1 : 1;
	}	
	
	public static Object max(Object a1, Object a2) {
		return (compare(a1, a2) >= 0) ? a1 : a2;
	}
	
	public static Object min(Object a1, Object a2) {
		return (compare(a1, a2) <= 0) ? a1 : a2;
	}
	
	@Override
	public String toString() {
		String res;
		if(aClosed) res = "[";
		else res = "(";
		res = (res + a + ", " + b);
		if(bClosed) res = res + "]";
		else res = res + ")";
		return res;
	}
	
	public Interval intersect(Interval o) {
		Interval i1, i2;
		if(this.compareTo(o) <= 0) {i1 = this; i2 = o;}
		else {i1 = o; i2 = this;}
		
		if(i1.bClosed && i2.aClosed) {
			if(compare(i2.a, i1.b) > 0) return null;
		} 
		else {
			if(compare(i2.a, i1.b) >= 0) return null;
		}
		
		Interval intersection = new Interval();
		intersection.a = i2.a;
		intersection.b = min(i1.b, i2.b);
		
		if(compare(i1.a, i2.a) == 0) {
			if(!i1.aClosed || !i2.aClosed) intersection.aClosed = false;
			else intersection.aClosed = true;
		}
		else {
			intersection.aClosed = i2.aClosed;
		}
		if(compare(i1.b, i2.b) == 0) {
			if(!i1.bClosed || !i2.bClosed) intersection.bClosed = false;
			else intersection.bClosed = true;
		} else {
			if(compare(i1.b, i2.b) < 0) intersection.bClosed = i1.bClosed;
			else intersection.bClosed = i2.bClosed;
		}

		return intersection;
	}
	
	public static int compare(Object lhs, Object rhs) {
		try{
		if(lhs.getClass().equals(rhs.getClass())) {
			if(lhs instanceof Integer && rhs instanceof Integer)
				return ((Integer)lhs).compareTo((Integer)rhs);
			else if(lhs instanceof Long && rhs instanceof Long)
				return ((Long)lhs).compareTo((Long)rhs);
			else if(lhs instanceof Short && rhs instanceof Short)
				return ((Short)lhs).compareTo((Short)rhs);
			else if(lhs instanceof Double && rhs instanceof Double) 
				return ((Double)lhs).compareTo((Double)rhs);
			else if(lhs instanceof String && rhs instanceof String)
				return ((String)lhs).compareTo((String)rhs);
		} else {
			if(lhs instanceof Double) {
				if(((Double)lhs) == Double.NEGATIVE_INFINITY) return -1;
				else return 1;
			} else {
				if(((Double)rhs) == Double.NEGATIVE_INFINITY) return 1;
				else return -1;
			}
		}
		}
		catch(ClassCastException e) {
			System.out.println("Classes--------");
			e.printStackTrace();
			System.out.println("lhs: " + lhs.getClass().getName());
			System.out.println(lhs);
			System.out.println("rhs: " + rhs.getClass().getName());
			System.out.println(rhs);
			System.out.println("---------------");
		}
		return -2;
	}
	
}
