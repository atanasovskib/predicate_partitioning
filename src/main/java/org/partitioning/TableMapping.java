package org.partitioning;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public final class TableMapping {

  public static final Map<SimpleTable, HashSet<String>> tableAttributes;

  static {
    Map<SimpleTable, HashSet<String>> tableMap = new HashMap<>();
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader("ssb.ddl"));
      String line;
      boolean intable = false;
      SimpleTable s = null;
      HashSet<String> h = null;
      while ((line = br.readLine()) != null) {
        line = line.trim();
        String[] w = line.toLowerCase().split("\\s+");
        if (w[0].equals("create") && w[1].equals("table")) {
          if (!intable) {
            intable = true;
            if (!w[2].contains(".")) {
              s = new SimpleTable("public", w[2]);
              h = new HashSet<>();
            }
          }
        } else if (intable && !w[0].equals("primary") && !w[0].equals("key")) {
          if (!w[0].startsWith(")")) {
            h.add(w[0].trim());
          }
          if (w[0].startsWith(")") || w[w.length - 1].contains(");")) {
            intable = false;
            tableMap.put(s, h);
          }
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    tableAttributes = Collections.unmodifiableMap(tableMap);
  }
}
