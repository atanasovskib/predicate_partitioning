package org.partitioning;


public class ExplainResult {
	
	public double executionCost;
	public int rowsEstimation;
	
	public ExplainResult()
	{
		this.executionCost = 0.0;
		this.rowsEstimation = 0;
	}
	
	public ExplainResult(double executionCost, int rowsEstimation) {
		this.executionCost = executionCost;
		this.rowsEstimation = rowsEstimation;
	}
}
