package org.partitioning;

public enum AttributeType {
	
	NUMERIC, TEXT, DATE;
	
}
