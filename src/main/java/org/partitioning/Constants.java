package org.partitioning;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public final class Constants {
	
	public static final String NONPARTITIONED_SCHEMA = "public";
	public static long TOTAL_Q_TIME = 0L;
	
	public static final String PARSER_WHERE_CLAUSE = "whereClause";
	public static final String PARSER_FROM_CLAUSE = "fromClause";
	public static final String PARSER_RANGEVAR = "RANGEVAR";
	public static final String PARSER_SELECT = "SELECT";
	public static final String PARSER_SCHEMANAME = "schemaname";
	public static final String PARSER_RELNAME = "relname";
	public static final String PARSER_ALIAS_LC = "alias";
	public static final String PARSER_ALIAS_UC = "ALIAS";
	public static final String PARSER_ALISANAME = "aliasname";
	public static final String PARSER_AEXPR = "AEXPR";
	public static final String PARSER_AEXPR_IN = "AEXPR IN";
	public static final String PARSER_A_CONST = "A_CONST";
	public static final String PARSER_COLUMNREF = "COLUMNREF";
	public static final String PARSER_REXPR = "rexpr";
	public static final String PARSER_LEXPR = "lexpr";
	public static final String PARSER_FIELDS = "fields";
	public static final String PARSER_NAME = "name";
	public static final String PARSER_VAL = "val";
	
	public static final int PREDICATE_OP_REGULAR = 0;
	public static final int PREDICATE_OP_INVERSE = 1;
	
	public static final String WORKLOAD_PATH = "workload.sql";
	
	public static final String DB_DIR = "/media/nino/SonyExternalHDD/postgresql/9.3/data_dir/base/12066/";
	public static final String SH_FALSE_FILENODE = "scripts/false-filenode.sh";
	
	public static String WORKLOAD;
	static {
		Scanner s = null;
		
		try {
			s = new Scanner(new File(WORKLOAD_PATH));
			WORKLOAD = s.useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			System.err.println("Unable to read workload. Aborting.");
			e.printStackTrace();
			System.exit(1);
		} finally {
			s.close();
		}
	}
	
	public static final Set<String> NAIVE_COUNT_DISTINCT_ATTRS = new HashSet<String>();
	static {
		NAIVE_COUNT_DISTINCT_ATTRS.add("lo_orderkey");
		NAIVE_COUNT_DISTINCT_ATTRS.add("lo_extendedprice");
		NAIVE_COUNT_DISTINCT_ATTRS.add("lo_revenue");
		NAIVE_COUNT_DISTINCT_ATTRS.add("lo_ordtotalprice");
	}
}
