package org.partitioning;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

class StreamGobbler extends Thread
{
    InputStream is;
    String type;
    StringBuilder output;
    
    StreamGobbler(InputStream is, String type)
    {
        this.is = is;
        this.type = type;
        this.output = new StringBuilder();
    }
    
    public String getOutput(){
    	return this.output.toString();
    }

    @Override
	public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;
            while ( (line = br.readLine()) != null) {
                output.append(line);
                output.append(System.getProperty("line.separator"));
            }
            
       } catch (IOException ioe)
              {
                ioe.printStackTrace();  
              }
    }
}