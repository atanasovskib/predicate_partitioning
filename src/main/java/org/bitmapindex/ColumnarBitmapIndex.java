package org.bitmapindex;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.multidimhistogram.Pair;
import org.partitioning.SimpleTable;
import org.roaringbitmap.FastAggregation;
import org.roaringbitmap.RoaringBitmap;

public class ColumnarBitmapIndex extends BitmapIndex
    implements Iterable<Pair<String, ColumnarBitmapIndex.ColumnarIndexElement>> {

  private Map<String, ColumnarIndexElement> elements;

  public ColumnarBitmapIndex() {
    this.table = null;
    this.elements = new HashMap<>();
    this.sortedAttributes = null;
  }

  public ColumnarBitmapIndex(SimpleTable table) {
    this.table = table;
    this.elements = new HashMap<>();
    this.sortedAttributes = new ArrayList<>();
  }

  public static RoaringBitmap and(RoaringBitmap... bitmaps) {
    return FastAggregation.and(bitmaps);
  }

  public static int andCardinality(RoaringBitmap... bitmaps) {
    return FastAggregation.and(bitmaps).getCardinality();
  }

  public static RoaringBitmap or(RoaringBitmap... bitmaps) {
    return FastAggregation.horizontal_or(bitmaps);
  }

  public static int orCardinality(RoaringBitmap... bitmaps) {
    return FastAggregation.horizontal_or(bitmaps).getCardinality();
  }

  public static ColumnarBitmapIndex and(ColumnarBitmapIndex... bitmapIndexes) {
    Objects.requireNonNull(bitmapIndexes);
    if (bitmapIndexes[0] == null) {
      System.out.println("null");
    }

    ColumnarBitmapIndex result = new ColumnarBitmapIndex(bitmapIndexes[0].table);
    ColumnarBitmapIndexIterator[] iterators = new ColumnarBitmapIndexIterator[bitmapIndexes.length];
    int pos = 0;
    String attribute;
    Pair<String, ColumnarIndexElement> p;
    for (ColumnarBitmapIndex cbi : bitmapIndexes)
      iterators[pos++] = cbi.iterator();
    while (iterators[0].hasNext()) {
      RoaringBitmap[] bitmaps = new RoaringBitmap[bitmapIndexes.length];
      p = iterators[0].next();
      attribute = p.key;
      bitmaps[0] = p.value.bitmap;
      for (int i = 1; i < iterators.length; ++i) {
        bitmaps[i] = iterators[i].next().value.bitmap;
      }
      result.addElement(attribute, FastAggregation.and(bitmaps));
    }
    return result;
  }

  public static ColumnarBitmapIndex or(ColumnarBitmapIndex... bitmapIndexes) {
    ColumnarBitmapIndex result = new ColumnarBitmapIndex(bitmapIndexes[0].table);
    ColumnarBitmapIndexIterator[] iterators = new ColumnarBitmapIndexIterator[bitmapIndexes.length];
    int pos = 0;
    String attribute;
    Pair<String, ColumnarIndexElement> p;
    for (ColumnarBitmapIndex cbi : bitmapIndexes)
      iterators[pos++] = cbi.iterator();
    while (iterators[0].hasNext()) {
      RoaringBitmap[] bitmaps = new RoaringBitmap[bitmapIndexes.length];
      p = iterators[0].next();
      attribute = p.key;
      bitmaps[0] = p.value.bitmap;
      for (int i = 1; i < iterators.length; ++i) {
        bitmaps[i] = iterators[i].next().value.bitmap;
      }
      result.addElement(attribute, FastAggregation.or(bitmaps));
    }
    return result;
  }

  public static ColumnarBitmapIndex read(ObjectInput input) throws IOException, ClassNotFoundException {
    ColumnarBitmapIndex newIndex = new ColumnarBitmapIndex();
    newIndex.readExternal(input);
    return newIndex;
  }

  @Override
  public void writeExternal(ObjectOutput output) throws IOException {
    super.writeExternal(output);
    output.writeBoolean(this.elements == null);
    if (this.elements == null) {
      return;
    }

    output.writeObject(this.elements);
  }

  @Override public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException {
    boolean isTableNull = input.readBoolean();
    if (!isTableNull) {
      this.table = (SimpleTable) input.readObject();
    }

    boolean areAttrNull = input.readBoolean();
    if (!areAttrNull) {
      this.sortedAttributes = (List<String>) input.readObject();
    }

    boolean areElementsNull = input.readBoolean();
    if (areElementsNull) {
      return;
    }

    this.elements = (Map<String, ColumnarIndexElement>) input.readObject();
  }

  public ColumnarBitmapIndexIterator iterator() {
    return new ColumnarBitmapIndexIterator();
  }

  public void addElement(String attribute, RoaringBitmap bitmap) {
    if (!this.elements.containsKey(attribute)) {
      this.sortedAttributes.add(attribute);
      sortAttributes();
    }
    this.elements.put(attribute, new ColumnarIndexElement(bitmap));
  }

  @Override
  public ColumnarIndexElement getElement(String attribute) {
    return this.elements.get(attribute);
  }

  public RoaringBitmap getBitmap(String attribute) {
    return this.elements.get(attribute).bitmap;
  }

  public ColumnarBitmapIndex and(ColumnarBitmapIndex other) {
    ColumnarBitmapIndex result = new ColumnarBitmapIndex(other.table);
    ColumnarBitmapIndexIterator thisIterator = this.iterator();
    ColumnarBitmapIndexIterator otherIterator = other.iterator();
    while (thisIterator.hasNext()) {
      Pair<String, ColumnarIndexElement> p1 = thisIterator.next();
      Pair<String, ColumnarIndexElement> p2 = otherIterator.next();
      result.addElement(p1.key, p1.value.and(p2.value));
    }
    return result;
  }

  public ColumnarBitmapIndex or(ColumnarBitmapIndex other) {
    ColumnarBitmapIndex result = new ColumnarBitmapIndex(other.table);
    ColumnarBitmapIndexIterator thisIterator = this.iterator();
    ColumnarBitmapIndexIterator otherIterator = other.iterator();
    while (thisIterator.hasNext()) {
      Pair<String, ColumnarIndexElement> p1 = thisIterator.next();
      Pair<String, ColumnarIndexElement> p2 = otherIterator.next();
      result.addElement(p1.key, p1.value.or(p2.value));
    }
    return result;
  }

  public static final class ColumnarIndexElement extends IndexElement {

    private RoaringBitmap bitmap;

    public ColumnarIndexElement() {
      this.bitmap = new RoaringBitmap();
    }

    ColumnarIndexElement(RoaringBitmap bitmap) {
      this.bitmap = bitmap;
    }

    public int getCardinality() {
      return this.bitmap.getCardinality();
    }

    public RoaringBitmap and(ColumnarIndexElement other) {
      return RoaringBitmap.and(this.bitmap, other.bitmap);
    }

    public RoaringBitmap andNot(ColumnarIndexElement other) {
      return RoaringBitmap.andNot(this.bitmap, other.bitmap);
    }

    public RoaringBitmap or(ColumnarIndexElement other) {
      return RoaringBitmap.or(this.bitmap, other.bitmap);
    }

    public void add(int index) {
      this.bitmap.add(index);
    }

    public void writeExternal(ObjectOutput output) throws IOException {
      output.writeBoolean(this.bitmap == null);
      if (this.bitmap != null) {
        output.writeObject(this.bitmap);
      }
    }

    public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException {
      boolean isBitmapNull = input.readBoolean();
      if (!isBitmapNull) {
        this.bitmap = (RoaringBitmap) input.readObject();
      }
    }
  }

  public class ColumnarBitmapIndexIterator implements Iterator<Pair<String, ColumnarIndexElement>> {

    int currentIndex = 0;

    @Override
    public boolean hasNext() {
      return (currentIndex < sortedAttributes.size());
    }

    @Override
    public Pair<String, ColumnarIndexElement> next() {
      Pair<String, ColumnarIndexElement> ret = new Pair<>(
          sortedAttributes.get(currentIndex),
          elements.get(sortedAttributes.get(currentIndex)));
      currentIndex++;
      return ret;
    }

    @Override
    public void remove() {
      // not supported
      throw new UnsupportedOperationException("Remove is not supported");
    }
  }
}
