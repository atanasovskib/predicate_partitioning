package org.bitmapindex;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import org.multidimhistogram.Pair;
import org.partitioning.SimpleTable;
import org.roaringbitmap.RoaringBitmap;

public class ValuedBitmapIndex extends BitmapIndex
    implements Iterable<Pair<String, ValuedBitmapIndex.ValuedIndexElement>> {

  private HashMap<String, ValuedIndexElement> elements;

  public ValuedBitmapIndex() {
    this.table = null;
    this.elements = new HashMap<>();
  }

  public ValuedBitmapIndex(SimpleTable table) {
    this.table = table;
  }

  public ValuedBitmapIndexIterator iterator() {
    return new ValuedBitmapIndexIterator();
  }

  public void addElement(String attribute, String value, RoaringBitmap bitmap) {
    if (!this.elements.containsKey(attribute)) {
      this.sortedAttributes.add(attribute);
      sortAttributes();
      ValuedIndexElement e = new ValuedIndexElement();
      e.addBitmap(value, bitmap);
      this.elements.put(attribute, e);
    } else {
      this.elements.get(attribute).addBitmap(value, bitmap);
    }
  }

  @Override
  public ValuedIndexElement getElement(String attribute) {
    return this.elements.get(attribute);
  }

  public RoaringBitmap getBitmap(String attribute, String value) {
    if (this.elements.containsKey(attribute)) {
      return this.elements.get(attribute).getBitmap(value);
    }

    return null;
  }

  @Override
  public void writeExternal(ObjectOutput output) throws IOException {
    super.writeExternal(output);
    output.writeBoolean(this.elements == null);
    if (this.elements == null) {
      return;
    }

    output.writeObject(this.elements);
  }

  @Override public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException {
    boolean isTableNull = input.readBoolean();
    if (!isTableNull) {
      this.table = (SimpleTable) input.readObject();
    }

    boolean areAttrNull = input.readBoolean();
    if (!areAttrNull) {
      this.sortedAttributes = (List<String>) input.readObject();
    }

    boolean areElementsNull = input.readBoolean();
    if (areElementsNull) {
      return;
    }

    this.elements = (HashMap<String, ValuedIndexElement>) input.readObject();
  }

  protected static final class ValuedIndexElement extends BitmapIndex.IndexElement {

    // AVL namesto TreeMap (ovozmozuva brz range search)
    // AVL e pobrzo za lookup (visinata e ~1.44logN vs ~2logN za red-black tree)

    protected TreeMap<String, RoaringBitmap> bitmaps;

    public ValuedIndexElement() {
      this.bitmaps = new TreeMap<>();
    }

    public ValuedIndexElement(TreeMap<String, RoaringBitmap> bitmaps) {
      this.bitmaps = bitmaps;
    }

    public static ValuedIndexElement read(ObjectInput in) throws IOException, ClassNotFoundException {
      ValuedIndexElement newIndex = new ValuedIndexElement();
      newIndex.readExternal(in);
      return newIndex;
    }

    public void readExternal(ObjectInput input) throws IOException, ClassNotFoundException {
      boolean areBitmapsNull = input.readBoolean();
      if (areBitmapsNull) {
        return;
      }

      int numBitmaps = input.readInt();
      for (int i = 0; i < numBitmaps; i++) {
        boolean isKeyNull = input.readBoolean();
        String key = null;
        if (!isKeyNull) {
          key = input.readUTF();
        }

        boolean isValNull = input.readBoolean();
        RoaringBitmap val = null;
        if (!isValNull) {
          val = (RoaringBitmap) input.readObject();
        }

        this.bitmaps.put(key, val);
      }
    }

    public void addBitmap(String key, RoaringBitmap bitmap) {
      this.bitmaps.put(key, bitmap);
    }

    public RoaringBitmap getBitmap(String key) {
      return this.bitmaps.get(key);
    }

    public int getCardinality(String key) {
      return this.bitmaps.get(key).getCardinality();
    }

    public RoaringBitmap and(ValuedIndexElement other, String key) {
      return RoaringBitmap.and(this.bitmaps.get(key), other.bitmaps.get(key));
    }

    public RoaringBitmap andNot(ValuedIndexElement other, String key) {
      return RoaringBitmap.andNot(this.bitmaps.get(key), other.bitmaps.get(key));
    }

    public RoaringBitmap or(ValuedIndexElement other, String key) {
      return RoaringBitmap.or(this.bitmaps.get(key), other.bitmaps.get(key));
    }

    public void add(String key, int index) {
      if (this.bitmaps.containsKey(key)) {
        this.bitmaps.get(key).add(index);
      } else {
        RoaringBitmap b = new RoaringBitmap();
        b.add(index);
        this.bitmaps.put(key, b);
      }
    }

    public void writeExternal(ObjectOutput output) throws IOException {
      output.writeBoolean(this.bitmaps == null);
      if (this.bitmaps == null) {
        return;
      }

      output.writeInt(this.bitmaps.size());
      for (String key : this.bitmaps.keySet()) {
        output.writeBoolean(key == null);
        if (key != null) {
          output.writeUTF(key);
        }

        RoaringBitmap val = this.bitmaps.get(key);
        output.writeBoolean(val == null);
        if (val != null) {
          output.writeObject(val);
        }
      }
    }
  }

  public class ValuedBitmapIndexIterator implements Iterator<Pair<String, ValuedIndexElement>> {

    int currentIndex = 0;

    @Override
    public boolean hasNext() {
      return (currentIndex < sortedAttributes.size());
    }

    @Override
    public Pair<String, ValuedIndexElement> next() {
      Pair<String, ValuedIndexElement> ret = new Pair<>(
          sortedAttributes.get(currentIndex),
          elements.get(sortedAttributes.get(currentIndex)));
      currentIndex++;
      return ret;
    }

    @Override
    public void remove() {
      // not supported
      throw new UnsupportedOperationException("Remove is not supported.");
    }
  }
}
