package org.bitmapindex;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectOutput;
import java.util.Collections;
import java.util.List;
import org.partitioning.SimpleTable;
import org.roaringbitmap.RoaringBitmap;

public abstract class BitmapIndex implements Externalizable{

  protected SimpleTable table;
  protected List<String> sortedAttributes;

  public abstract Object getElement(String attribute);

  public void addElement(String attribute) {
  }

  protected void sortAttributes() {
    Collections.sort(this.sortedAttributes);
  }

  public void writeExternal(ObjectOutput output) throws IOException {
    output.writeBoolean(table == null);
    if (table != null) {
      table.writeExternal(output);
    }

    output.writeBoolean(sortedAttributes == null);
    if (sortedAttributes == null) {
      return;
    }

    output.writeObject(this.sortedAttributes);
  }

  protected static abstract class IndexElement implements Externalizable {

    public int getCardinality() {
      return 0;
    }

    public RoaringBitmap and() {
      return null;
    }

    public RoaringBitmap andNot() {
      return null;
    }

    public RoaringBitmap or() {
      return null;
    }

    public void add(int index) {
    }
  }
}
