package org;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBProperties {
  private static final Logger log = LoggerFactory.getLogger(DBProperties.class);
  private final Properties props;

  private DBProperties(String dbPropertiesFile) throws IOException {
    Objects.requireNonNull(dbPropertiesFile);
    this.props = new Properties();
    FileInputStream in = new FileInputStream(dbPropertiesFile);
    this.props.load(in);
    in.close();
    log.debug("opened db properties file {}", dbPropertiesFile);
    log.debug("Loaded properties:");
    List<String> keys = Arrays.asList(Keys.DB_NAME, Keys.DB_SERVER, Keys.DB_PORT, Keys.DB_USERNAME, Keys.DB_PASS);
    for (String key : keys) {
      String value = props.getProperty(key);
      if (value == null) {
        log.error("Property {} not set in file {}", key, dbPropertiesFile);
        throw new IllegalStateException("Must set a value for the property: "
            + key
            + "; Add an new line "
            + key
            + "=desired_value to the file "
            + dbPropertiesFile);
      } else {
        log.info("Property {} has value {}", key, value);
      }
    }
  }

  public static DBProperties load(String dbPropertiesFile) {
    try {
      return new DBProperties(dbPropertiesFile);
    } catch (IOException e) {
      throw new RuntimeException("Could not initialize DB properties", e);
    }
  }

  public String getServer() {
    return this.props.getProperty(Keys.DB_SERVER);
  }

  public String getDatabaseName() {
    return this.props.getProperty(Keys.DB_NAME);
  }

  public String getUsername() {
    return this.props.getProperty(Keys.DB_USERNAME);
  }

  public String getPassword() {
    return this.props.getProperty(Keys.DB_PASS);
  }

  public String getPort() {
    return this.props.getProperty(Keys.DB_PORT);
  }

  public String getStarShemaTBLFolder() {
    return this.props.getProperty(Keys.SSB_TBL_FOLDER);
  }

  private class Keys {
    static final String DB_NAME = "database_name";
    static final String DB_USERNAME = "database_user";
    static final String DB_PASS = "database_pass";
    static final String DB_SERVER = "database_server";
    static final String DB_PORT = "database_port";
    static final String SSB_TBL_FOLDER = "ssb_tbl_folder";
  }
}
