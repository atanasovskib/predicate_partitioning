package org;

public class ProjectConstants {
  public static final String defaultDbPropertiesFileName = "db.properties";
  public static final String serializedPredicatesFileName = "predicates.serialized";
  public static final String serializedTableMetaDataFileName = "table_metadata.serialized";
  public static final String serializedBitmapIndexHolderFileName = "bitmap_index_holder.serialized";
  public static final String serializedMultiDimensionalHistogramsFileName = "multidimensional_histograms.serialized";
}
