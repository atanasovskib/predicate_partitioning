package org.partitioning;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import junit.framework.Assert;
import org.junit.Test;

public class SimpleTableTest {
  @Test
  public void readWriteExternal() throws Exception {
    SimpleTable table = new SimpleTable(null, "tablename");
    test(table);
  }

  private void test(SimpleTable table) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectOutput output = new ObjectOutputStream(baos);

    table.writeExternal(output);
    output.close();
    byte[] bytes = baos.toByteArray();

    ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
    ObjectInput input = new ObjectInputStream(bais);
    SimpleTable deserialized = SimpleTable.readExternal(input);

    Assert.assertEquals(table, deserialized);
  }

  @Test
  public void readWriteExternal2() throws Exception {
    SimpleTable table = new SimpleTable("schema", "tablename");
    test(table);
  }

  @Test
  public void readWriteExternal3() throws Exception {
    SimpleTable table = new SimpleTable("schema", null);
    test(table);
  }

  @Test
  public void readWriteExternal4() throws Exception {
    SimpleTable table = new SimpleTable(null, null);
    test(table);
  }
}