package org.partitioning;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import junit.framework.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AtomicPredicateTest {
  private static final Logger log = LoggerFactory.getLogger(AtomicPredicateTest.class);

  @Test
  public void readWriteExternal() throws Exception {
    String[] workload = Constants.WORKLOAD.split(";");
    Set<AtomicPredicate> uniquePredicates = new HashSet<>();
    for (String query : workload) {
      WorkloadQuery q = new WorkloadQuery(query);
      q.extractPredicates();
      List<AtomicPredicate> q_predicates = q.getPredicates();
      for (AtomicPredicate p : q_predicates) {
        uniquePredicates.add(p);
      }
    }

    log.debug("predicates extracted and added to set");
    WorkloadPredicates.predicates = new AtomicPredicate[uniquePredicates.size()];
    int i = 0;
    for (AtomicPredicate p : uniquePredicates) {
      WorkloadPredicates.predicates[i++] = p;
    }

    AtomicPredicate[] allPredicates = WorkloadPredicates.predicates;
    int predicateNumber = 1;
    for (AtomicPredicate predicate : allPredicates) {
      log.debug("Predicate number: {}", predicateNumber++);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutput output = new ObjectOutputStream(baos);
      predicate.writeExternal(output);
      output.close();
      byte[] bytes = baos.toByteArray();

      ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
      ObjectInput input = new ObjectInputStream(bais);
      AtomicPredicate deserializedPredicate = AtomicPredicate.readExternal(input);

      Assert.assertEquals(predicate, deserializedPredicate);
    }
  }
}