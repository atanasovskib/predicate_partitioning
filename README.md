# Predicate Partitioning - Implementation
This is a Gradle project,
you can use it any IDE that has Gradle integration

The dependencies of the project are all on the Central Maven Repository except the Jenes library that it has localy in the lib folder

After importing the project in an IDE of your choice and all the dependencies are resolved you must set up the environment

1. Install ruby and pg_query
This Ruby extension uses the actual PostgreSQL server source to parse SQL queries and return the internal PostgreSQL parsetree.
(source https://github.com/lfittl/pg_query)

  On most linux systems (ubuntu, debian, fedora etc) ruby is found in the repository of the package manager (apt-get for debian based)
  but there have been issues with the ruby installed in those repos and the pg_query gem (ruby plugin)

  Uninstall all previously installed versions of ruby
  Install RVM with Ruby from www.rvm.io/rvm/install (tested with the stable version)
    after the installation it will tell you to execute the command source /user_home_folder/.rvm/scripts/rvm
    pay attention to the console output

    RVM installation requires
  Install pg_query
    Dependencies: flex and bison
    both available from standard repositories
    execute: apt-get install bison flex

    pg_query is available as a ruby gem in the default ruby repositories just run
      gem install pg_query

  Possible issues with RVM
   RVM may fail to install if there are 404 errors when executing `apt-get update` remove the failing repositories from your sources.list

2. Generate data for the SSB benchmark using SSB-DBGen
  checkout this repo https://github.com/greenlion/ssb-dbgen

  dependencies: make, gcc, stuff like that
  `apt-get install build-essential` should take care of that

  open the MAKEFILE in the ssb-dbgen repo
=======
The setup has been tested on a ubuntu/debian operating system, the following instructions will assume that you are using a Debian derivative
# How to set up the environment

The project uses pg_query, a ruby extension. This Ruby extension uses the actual PostgreSQL server source to parse SQL queries and return the internal PostgreSQL parsetree.
The source code can be found on the GitHub repo [pg\_query](https://github.com/lfittl/pg_query)

* Installing RVM with Ruby
  * Unistall all previously installed versions of Ruby `apt-get remove ruby`
  * Visit (www.rvm.io/rvm/install) and follow the instructions. Install the stable version
  * After the instalation finishes it will tell you to execute the command `source ~/.rvm/scripts/rvm`
  * RVM executes an `apt-get update` command, if there are 404 errors from some repositories from your sources list, it will fail *pay atention to the console output*
  * create a link to the ruby executable in /bin/bash `ln -s ~/.rvm/rubies/ruby-2.3.0/bin/ruby /bin/ruby`
  * or add the ruby executable to the path
  * create a link for gem in /bin/bash `ln -s ~/.rvm/rubies/ruby-2.3.0/bin/gem /bin/gem`
  * notice the ruby version, adjust to your version acordingly
  
* Installing pg_query
  * pg_query is available as ruby gem in the public ruby repository, just run `gem install pg_query`
  * If pg_query failed to install then you need to install flex and bison `apt-get install flex bison`

# Generating data 

After you have set up ruby and pg_query, you have to generate data for the SSB benchmark.
We are using the SSB-DBGen tool found on the [ssb-dbgen](https://github.com/atanasovskib/ssb-dbgen) repository on GitHub (a for of (https://github.com/greenlion/ssb-dbgen))

* Make sure you have a C compiler and make `apt-get install build-essential`
* Clone the ssb-dbgen repo `git clone git@github.com:greenlion/ssb-dbgen.git` if you have a set up ssh access to GitHub, or `git clone https://github.com/greenlion/ssb-dbgen.git` to authenticate with user/pass
* Navigate to the newly created ssb-dbgen folder and open `makefile` 
* You will see at the start of the file a few variables being defined, CC is set to gcc, DATABASE is set to DB2, we are going to leave that because we are using PostgreSQL, set MACHINE to LINUX and WORKLOAD should remain SSBM
* Compile the source. Be sure that you are in the ssb-dbgen folder and execute `make`
* If only warnings appear and no errors you should be good
* Time to generate the data. The project only works for scale factor 1
  * Generate customers: `./dbgen -s 1 -T c`
  * Generate parts: `./dbgen -s 1 -T p`
  * Generate suppliers: `./dbgen -s 1 -T s`
  * Generate dates: `./dbgen -s 1 -T d`
  * Genrate fact table line orders: `./dbgen -s 1 -T l`

* Or generate all the tables at once with `./dbgetn -s 1 -T a`

# PosgreSQL server
* Have a PostgreSQL server available, LOCALLY on the computer you are running the project on
* Create an database on that server

# Inserting the data in the PostgreSQL database

The SSB-DBGen tool generates files with the .tbl extension in the folder where the dbgen executable is.
Those files have rows like 

`1|lace spring|MFGR#1|MFGR#11|MFGR#1121|goldenrod|PROMO BURNISHED COPPER|7|JUMBO PKG|`

The script that we use to move the data from the files to the database expects that there isn't a | character at the
end of each line. That's why before inserting them, another script needs to be run from the folder where the files were generated.

The ssb-dbgen fork found here [https://github.com/atanasovskib/ssb-dbgen] has a bash script named `removeTrailingChars.sh` run it before doing anything else.

`bash removeTrailingChars.sh`

Now you can start the Java project in your favourite IDE.

* Find the `GenerateStarSchema.java` file. It has a main function that takes 1 argument, the location of a properties file.
* If an argument is not specifed a default file is used. The file is called db.properties and can be found in the root folder 
of the project
* Open the db.properties file and change the values in it to reflect your set up (or create a new file with the property keys inside)

```
database_name=database_name_that_will_hold_the_data
database_user=database_username_used_for_connection
database_pass=password_for_the_user
database_server=localhost
database_port=database_server_listening_port
ssb_tbl_folder=path_to_the_folder_that_holds_the_generated_tbl_files
```

* Run the GenerateStarSchema.java file

# Run the algorithm
>>>>>>> da2f42a964c0012eb91471f574827f0d5e1b2c4f
